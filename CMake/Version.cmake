find_package(Git)

if(GIT_FOUND AND GIT_EXECUTABLE AND EXISTS ${PROJECT_SOURCE_DIR}/.git)
	execute_process(COMMAND ${GIT_EXECUTABLE} describe --dirty --long --tags --always --match=v*
		WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
		OUTPUT_VARIABLE VERSION_STRING)
	string(REGEX REPLACE "[\n\r]" "" VERSION_STRING "${VERSION_STRING}")
	string(REGEX REPLACE "^v" "" VERSION_STRING "${VERSION_STRING}")
	string(REGEX REPLACE "-([0-9]+)-" ".r\\1." VERSION_STRING "${VERSION_STRING}")
endif()

if(${VERSION_STRING})
	message("Building ${PROJECT_NAME} Git version ${VERSION_STRING}")
else()
	set(VERSION_STRING "${PROJECT_VERSION}")
	message("Building ${PROJECT_NAME} version: ${VERSION_STRING}")
endif()

configure_file(
	${PROJECT_SOURCE_DIR}/CMake/version.hpp.in
	${PROJECT_BINARY_DIR}/version.hpp
	)
install(FILES ${PROJECT_BINARY_DIR}/version.hpp
	DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/algorithms-library
	)
