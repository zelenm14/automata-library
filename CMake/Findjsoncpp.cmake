# - Find jsoncpp
#
# Find jsoncpp includes and library
#
# This module defines the following variables:
#  jsoncpp_FOUND - True if libjsoncpp has been found.
#  jsoncpp_INCLUDE_DIR - location to look for headers,
#  jsoncpp_LIBRARIES - the libraries to link against libjsoncpp

find_path(jsoncpp_INCLUDE_DIR json/features.h json/json_features.h
    PATH_SUFFIXES include jsoncpp include/jsoncpp)
find_library(jsoncpp_LIBRARY jsoncpp)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(jsoncpp DEFAULT_MSG
    jsoncpp_INCLUDE_DIR jsoncpp_LIBRARY)

if(NOT jsoncpp_FOUND)
    message(FATAL_ERROR "libjsoncpp missing" )
endif(NOT jsoncpp_FOUND)

set(jsoncpp_LIBRARIES ${jsoncpp_LIBRARY})
mark_as_advanced(
    jsoncpp_INCLUDE_DIR
    jsoncpp_LIBRARIES
)
