/*
 * AlgorithmRegistryInfo.cpp
 *
 *  Created on: 11. 7. 2017
 *	  Author: Jan Travnicek
 */

#include "AlgorithmRegistryInfo.hpp"
#include "Registry.h"

namespace abstraction {

ext::pair < std::string, abstraction::TypeQualifiers::TypeQualifierSet > AlgorithmFullInfo::getNormalizedResult ( ) const {
	if ( ! abstraction::Registry::hasNormalize ( m_result.first ) )
		return m_result;

	std::shared_ptr < abstraction::OperationAbstraction > normalized = abstraction::Registry::getNormalizeAbstraction ( m_result.first );

	return ext::make_pair ( normalized->getReturnType ( ), normalized->getReturnTypeQualifiers ( ) );
}

} /* namespace abstraction */
