/*
 * ValuePrinterRegistry.hpp
 *
 *  Created on: 11. 7. 2017
 *	  Author: Jan Travnicek
 */

#ifndef _VALUE_PRINTER_REGISTRY_HPP_
#define _VALUE_PRINTER_REGISTRY_HPP_

#include <alib/memory>
#include <alib/string>
#include <alib/map>
#include <alib/typeinfo>

#include <abstraction/OperationAbstraction.hpp>
#include "BaseRegistryEntry.hpp"

namespace abstraction {

class ValuePrinterRegistry {
public:
	class Entry : public BaseRegistryEntry {
	};

private:
	template < class Param >
	class EntryImpl : public Entry {
	public:
		EntryImpl ( ) = default;

		std::shared_ptr < abstraction::OperationAbstraction > getAbstraction ( ) const override;
	};

	static ext::map < std::string, std::unique_ptr < Entry > > & getEntries ( );

public:
	static void unregisterValuePrinter ( const std::string & param );

	template < class ParamType >
	static void unregisterValuePrinter ( ) {
		std::string param = ext::to_string < ParamType > ( );
		unregisterValuePrinter ( param );
	}

	static void registerValuePrinter ( std::string param, std::unique_ptr < Entry > entry );

	template < class ParamType >
	static void registerValuePrinter ( std::string param ) {
		registerValuePrinter ( std::move ( param ), std::unique_ptr < Entry > ( new EntryImpl < ParamType > ( ) ) );
	}

	template < class ParamType >
	static void registerValuePrinter ( ) {
		std::string param = ext::to_string < ParamType > ( );
		registerValuePrinter < ParamType > ( std::move ( param ) );
	}

	static std::shared_ptr < abstraction::OperationAbstraction > getAbstraction ( const std::string & param );
};

} /* namespace abstraction */

#include <abstraction/ValuePrinterAbstraction.hpp>

namespace abstraction {

template < class Param >
std::shared_ptr < abstraction::OperationAbstraction > ValuePrinterRegistry::EntryImpl < Param >::getAbstraction ( ) const {
	return std::make_shared < abstraction::ValuePrinterAbstraction < const Param & > > ( );
}

template < >
std::shared_ptr < abstraction::OperationAbstraction > ValuePrinterRegistry::EntryImpl < void >::getAbstraction ( ) const;

} /* namespace abstraction */

#endif /* _VALUE_PRINTER_REGISTRY_HPP_ */
