#ifndef _OPERATOR_REGISTRATION_HPP_
#define _OPERATOR_REGISTRATION_HPP_

#include <alib/registration>

#include <registry/OperatorRegistry.hpp>

namespace registration {

template < abstraction::Operators::BinaryOperators Type, class FirstParamType, class SecondParamType >
class BinaryOperatorRegister : public ext::Register < void > {
public:
	template < class ReturnType >
	BinaryOperatorRegister ( ReturnType ( * callback ) ( FirstParamType, SecondParamType ) ) : ext::Register < void > ( [ = ] ( ) {
				abstraction::OperatorRegistry::registerBinary < ReturnType, FirstParamType, SecondParamType > ( Type, callback );
			}, [ ] ( ) {
				abstraction::OperatorRegistry::unregisterBinary < FirstParamType, SecondParamType > ( Type );
			} ) {
	}

	BinaryOperatorRegister ( ) : ext::Register < void > ( [ ] ( ) {
				abstraction::OperatorRegistry::registerBinary < Type, FirstParamType, SecondParamType > ( );
			}, [ ] ( ) {
				abstraction::OperatorRegistry::unregisterBinary < FirstParamType, SecondParamType > ( Type );
			} ) {
	}

};

template < abstraction::Operators::PrefixOperators Type, class ParamType >
class PrefixOperatorRegister : public ext::Register < void > {
public:
	template < class ReturnType >
	PrefixOperatorRegister ( ReturnType ( * callback ) ( ParamType ) ) : ext::Register < void > ( [ = ] ( ) {
				abstraction::OperatorRegistry::registerPrefix < ReturnType, ParamType > ( Type, callback );
			}, [ ] ( ) {
				abstraction::OperatorRegistry::unregisterBinary < ParamType > ( Type );
			} ) {
	}

	PrefixOperatorRegister ( ) : ext::Register < void > ( [ ] ( ) {
				abstraction::OperatorRegistry::registerPrefix < Type, ParamType > ( );
			}, [ ] ( ) {
				abstraction::OperatorRegistry::unregisterPrefix < ParamType > ( Type );
			} ) {
	}

};

template < abstraction::Operators::PostfixOperators Type, class ParamType >
class PostfixOperatorRegister : public ext::Register < void > {
public:
	template < class ReturnType >
	PostfixOperatorRegister ( ReturnType ( * callback ) ( ParamType ) ) : ext::Register < void > ( [ = ] ( ) {
				abstraction::OperatorRegistry::registerPostfix < ReturnType, ParamType > ( Type, callback );
			}, [ ] ( ) {
				abstraction::OperatorRegistry::unregisterBinary < ParamType > ( Type );
			} ) {
	}

	PostfixOperatorRegister ( ) : ext::Register < void > ( [ ] ( ) {
				abstraction::OperatorRegistry::registerPostfix < Type, ParamType > ( );
			}, [ ] ( ) {
				abstraction::OperatorRegistry::unregisterPostfix < ParamType > ( Type );
			} ) {
	}

};

} /* namespace registration */

#endif // _OPERATOR_REGISTRATION_HPP_
