#ifndef _COMPONENT_REGISTRATION_HPP_
#define _COMPONENT_REGISTRATION_HPP_

namespace registration {

template < class ObjectType >
class ComponentRegister {
public:
	ComponentRegister ( ) {
		ObjectType::registerComponent ( );
	}

	~ComponentRegister ( ) {
		ObjectType::unregisterComponent ( );
	}

};

} /* namespace registration */

#endif /* _COMPONENT_REGISTRATION_HPP_ */
