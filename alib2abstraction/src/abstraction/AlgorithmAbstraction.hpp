/*
 * AlgorithmAbstraction.hpp
 *
 *  Created on: 11. 7. 2017
 *	  Author: Jan Travnicek
 */

#ifndef _ALGORITHM_ABSTRACTION_HPP_
#define _ALGORITHM_ABSTRACTION_HPP_

#include <abstraction/NaryOperationAbstraction.hpp>
#include <abstraction/ValueOperationAbstraction.hpp>

namespace abstraction {

template < class ReturnType, class ... ParamTypes >
class AlgorithmAbstraction : virtual public NaryOperationAbstraction < ParamTypes ... >, virtual public ValueOperationAbstraction < ReturnType > {
	std::function < ReturnType ( ParamTypes ... ) > m_callback;

public:
	AlgorithmAbstraction ( std::function < ReturnType ( ParamTypes ... ) > callback ) : m_callback ( std::move ( callback ) ) {
	}

	std::shared_ptr < abstraction::Value > run ( ) override {
		return this->template run_helper < ParamTypes ... > ( m_callback, this->getParams ( ) );
	}

};

} /* namespace abstraction */

#endif /* _ALGORITHM_ABSTRACTION_HPP_ */
