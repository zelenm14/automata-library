/*
 * OperationAbstraction.cpp
 *
 *  Created on: 10. 7. 2017
 *	  Author: Jan Travnicek
 */

#include "OperationAbstraction.hpp"

namespace abstraction {

std::string OperationAbstraction::getParamType ( size_t index ) const {
	if ( isParamTypeUniversal ( index ) )
		return "auto";

	return ext::to_string ( getParamTypeIndex ( index ) );
}

std::string OperationAbstraction::getReturnType ( ) const {
	if ( isReturnTypeUniversal ( ) )
		return "auto";

	return ext::to_string ( getReturnTypeIndex ( ) );
}

std::shared_ptr < abstraction::OperationAbstraction > OperationAbstraction::getProxyAbstraction ( ) {
	return shared_from_this();
}

} /* namespace abstraction */
