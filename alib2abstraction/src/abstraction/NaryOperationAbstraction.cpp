/*
 * NaryOperationAbstraction.hpp
 *
 *  Created on: 20. 8. 2017
 *	  Author: Jan Travnicek
 */

#include "NaryOperationAbstraction.hpp"

template class abstraction::NaryOperationAbstractionImpl < 0 >;
template class abstraction::NaryOperationAbstractionImpl < 1 >;
template class abstraction::NaryOperationAbstractionImpl < 2 >;
template class abstraction::NaryOperationAbstractionImpl < 3 >;
