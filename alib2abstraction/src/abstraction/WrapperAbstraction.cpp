/*
 * WrapperAbstraction.hpp
 *
 *  Created on: 20. 8. 2017
 *	  Author: Jan Travnicek
 */

#include "WrapperAbstraction.hpp"

template class abstraction::WrapperAbstractionImpl < 1 >;
template class abstraction::WrapperAbstractionImpl < 2 >;
template class abstraction::WrapperAbstractionImpl < 3 >;
