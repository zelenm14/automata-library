#include "TypeQualifiers.hpp"

namespace abstraction {

std::ostream & operator << ( std::ostream & os, TypeQualifiers::TypeQualifierSet arg ) {
	os << "[ ";
	bool first = true;
	if ( TypeQualifiers::isConst ( arg ) )
			os << ( first ? "" : ", " ) << "Constant", first = false;
	if ( TypeQualifiers::isLvalueRef ( arg ) )
			os << ( first ? "" : ", " ) << "LValue reference", first = false;
	if ( TypeQualifiers::isRvalueRef ( arg ) )
			os << ( first ? "" : ", " ) << "RValue reference", first = false;
	os << " ]";
	return os;
}

} /* namespace abstraction */
