#ifndef _CAST_HELPER_H_
#define _CAST_HELPER_H_

#include <string>
#include <memory>
#include <abstraction/Value.hpp>
#include <abstraction/TemporariesHolder.h>

namespace abstraction {

class CastHelper {
public:
	static std::shared_ptr < abstraction::Value > eval ( abstraction::TemporariesHolder & environment, const std::shared_ptr < abstraction::Value > & param, const std::string & type );

};

} /* namespace abstraction */

#endif /* _CAST_PARAM_H_ */
