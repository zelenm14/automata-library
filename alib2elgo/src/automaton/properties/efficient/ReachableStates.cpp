/*
 * ReachableStates.cpp
 *
 *  Created on: 23. 3. 2014
 *	  Author: Tomas Pecka
 */

#include "ReachableStates.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ReachableStatesEpsilonNFA = registration::AbstractRegister < automaton::properties::efficient::ReachableStates, ext::set < DefaultStateType >, const automaton::EpsilonNFA < > & > ( automaton::properties::efficient::ReachableStates::reachableStates );
auto ReachableStatesNFA = registration::AbstractRegister < automaton::properties::efficient::ReachableStates, ext::set < DefaultStateType >, const automaton::NFA < > & > ( automaton::properties::efficient::ReachableStates::reachableStates );
auto ReachableStatesCompactNFA = registration::AbstractRegister < automaton::properties::efficient::ReachableStates, ext::set < DefaultStateType >, const automaton::CompactNFA < > & > ( automaton::properties::efficient::ReachableStates::reachableStates );
auto ReachableStatesExtendedNFA = registration::AbstractRegister < automaton::properties::efficient::ReachableStates, ext::set < DefaultStateType >, const automaton::ExtendedNFA < > & > ( automaton::properties::efficient::ReachableStates::reachableStates );
auto ReachableStatesMultiInitialStateNFA = registration::AbstractRegister < automaton::properties::efficient::ReachableStates, ext::set < DefaultStateType >, const automaton::MultiInitialStateNFA < > & > ( automaton::properties::efficient::ReachableStates::reachableStates );
auto ReachableStatesDFA = registration::AbstractRegister < automaton::properties::efficient::ReachableStates, ext::set < DefaultStateType >, const automaton::DFA < > & > ( automaton::properties::efficient::ReachableStates::reachableStates );

} /* namespace */
