/*
 * AllEpsilonClosure.cpp
 *
 *  Created on: 12. 4. 2015
 *	  Author: Jan Travnicek
 */

#include "AllEpsilonClosure.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto AllEpsilonClosureEpsilonNFA = registration::AbstractRegister < automaton::properties::efficient::AllEpsilonClosure, ext::map < DefaultStateType, ext::set < DefaultStateType > >, const automaton::EpsilonNFA < > & > ( automaton::properties::efficient::AllEpsilonClosure::allEpsilonClosure );
auto AllEpsilonClosureMultiInitialStateNFA = registration::AbstractRegister < automaton::properties::efficient::AllEpsilonClosure, ext::map < DefaultStateType, ext::set < DefaultStateType > >, const automaton::MultiInitialStateNFA < > & > ( automaton::properties::efficient::AllEpsilonClosure::allEpsilonClosure );
auto AllEpsilonClosureNFA = registration::AbstractRegister < automaton::properties::efficient::AllEpsilonClosure, ext::map < DefaultStateType, ext::set<DefaultStateType > >, const automaton::NFA < > & > ( automaton::properties::efficient::AllEpsilonClosure::allEpsilonClosure );
auto AllEpsilonClosureDFA = registration::AbstractRegister < automaton::properties::efficient::AllEpsilonClosure, ext::map < DefaultStateType, ext::set<DefaultStateType > >, const automaton::DFA < > & > ( automaton::properties::efficient::AllEpsilonClosure::allEpsilonClosure );
auto AllEpsilonClosureExtendedNFA = registration::AbstractRegister < automaton::properties::efficient::AllEpsilonClosure, ext::map < DefaultStateType, ext::set<DefaultStateType > >, const automaton::ExtendedNFA < > & > ( automaton::properties::efficient::AllEpsilonClosure::allEpsilonClosure );
auto AllEpsilonClosureCompactNFA = registration::AbstractRegister < automaton::properties::efficient::AllEpsilonClosure, ext::map < DefaultStateType, ext::set<DefaultStateType > >, const automaton::CompactNFA < > & > ( automaton::properties::efficient::AllEpsilonClosure::allEpsilonClosure );

} /* namespace */
