/*
 * UsefulStates.cpp
 *
 *  Created on: 23. 3. 2014
 *	  Author: Tomas Pecka
 */

#include "UsefulStates.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto UsefulStatesEpsilonNFA = registration::AbstractRegister < automaton::properties::efficient::UsefulStates, ext::set < DefaultStateType >, const automaton::EpsilonNFA < > & > ( automaton::properties::efficient::UsefulStates::usefulStates );
auto UsefulStatesNFA = registration::AbstractRegister < automaton::properties::efficient::UsefulStates, ext::set < DefaultStateType >, const automaton::NFA < > & > ( automaton::properties::efficient::UsefulStates::usefulStates );
auto UsefulStatesCompactNFA = registration::AbstractRegister < automaton::properties::efficient::UsefulStates, ext::set < DefaultStateType >, const automaton::CompactNFA < > & > ( automaton::properties::efficient::UsefulStates::usefulStates );
auto UsefulStatesExtendedNFA = registration::AbstractRegister < automaton::properties::efficient::UsefulStates, ext::set < DefaultStateType >, const automaton::ExtendedNFA < > & > ( automaton::properties::efficient::UsefulStates::usefulStates );
auto UsefulStatesMultiInitialStateNFA = registration::AbstractRegister < automaton::properties::efficient::UsefulStates, ext::set < DefaultStateType >, const automaton::MultiInitialStateNFA < > & > ( automaton::properties::efficient::UsefulStates::usefulStates );
auto UsefulStatesDFA = registration::AbstractRegister < automaton::properties::efficient::UsefulStates, ext::set < DefaultStateType >, const automaton::DFA < > & > ( automaton::properties::efficient::UsefulStates::usefulStates );

} /* namespace */
