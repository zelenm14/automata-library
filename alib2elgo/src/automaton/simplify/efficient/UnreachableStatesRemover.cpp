/*
 * UnreachableStatesRemover.cpp
 *
 *  Created on: 23. 3. 2014
 *	  Author: Tomas Pecka
 */

#include "UnreachableStatesRemover.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto UnreachableStatesRemoverEpsilonNFA = registration::AbstractRegister < automaton::simplify::efficient::UnreachableStatesRemover, automaton::EpsilonNFA < >, const automaton::EpsilonNFA < > & > ( automaton::simplify::efficient::UnreachableStatesRemover::remove );
auto UnreachableStatesRemoverNFA = registration::AbstractRegister < automaton::simplify::efficient::UnreachableStatesRemover, automaton::NFA < > , const automaton::NFA < > & > ( automaton::simplify::efficient::UnreachableStatesRemover::remove );
auto UnreachableStatesRemoverCompactNFA = registration::AbstractRegister < automaton::simplify::efficient::UnreachableStatesRemover, automaton::CompactNFA < >, const automaton::CompactNFA < > & > ( automaton::simplify::efficient::UnreachableStatesRemover::remove );
auto UnreachableStatesRemoverExtendedNFA = registration::AbstractRegister < automaton::simplify::efficient::UnreachableStatesRemover, automaton::ExtendedNFA < >, const automaton::ExtendedNFA < > & > ( automaton::simplify::efficient::UnreachableStatesRemover::remove );
auto UnreachableStatesRemoverDFA = registration::AbstractRegister < automaton::simplify::efficient::UnreachableStatesRemover, automaton::DFA < >, const automaton::DFA < > & > ( automaton::simplify::efficient::UnreachableStatesRemover::remove );
auto UnreachableStatesRemoverMultiInitialStateNFA = registration::AbstractRegister < automaton::simplify::efficient::UnreachableStatesRemover, automaton::MultiInitialStateNFA < >, const automaton::MultiInitialStateNFA < > & > ( automaton::simplify::efficient::UnreachableStatesRemover::remove );

} /* namespace */
