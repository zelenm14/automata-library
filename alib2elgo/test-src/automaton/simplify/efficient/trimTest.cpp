#include <catch2/catch.hpp>

#include <list>

#include "automaton/simplify/Trim.h"
#include "automaton/FSM/DFA.h"


TEST_CASE ( "Trim automaton", "[unit][automaton][elgo]" ) {
	automaton::DFA < > automaton(DefaultStateType(1));

	automaton.addState(DefaultStateType(1));
	automaton.addState(DefaultStateType(2));
	automaton.addState(DefaultStateType(3));
	automaton.addInputSymbol(DefaultSymbolType("a"));
	automaton.addInputSymbol(DefaultSymbolType("b"));

	automaton.addTransition(DefaultStateType(1), DefaultSymbolType("a"), DefaultStateType(2));
	automaton.addTransition(DefaultStateType(2), DefaultSymbolType("b"), DefaultStateType(1));
	automaton.addTransition(DefaultStateType(3), DefaultSymbolType("b"), DefaultStateType(1));

	automaton.addFinalState(DefaultStateType(1));

	automaton::DFA < > trimed = automaton::simplify::Trim::trim(automaton);

	CHECK ( trimed.getStates().size() == 2 );
}
