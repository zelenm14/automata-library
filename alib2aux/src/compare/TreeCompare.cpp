/*
 * TreeCompare.cpp
 *
 *  Created on: Feb 9, 2019
 *      Author: Tomas Pecka
 */

#include "TreeCompare.h"
#include <registration/AlgoRegistration.hpp>
#include <common/DefaultSymbolType.h>

namespace {

auto TreeCompareRankedTree = registration::AbstractRegister < compare::TreeCompare, bool, const tree::RankedTree < DefaultSymbolType> &, const tree::RankedTree < DefaultSymbolType > & > ( compare::TreeCompare::compare );

} /* namespace */
