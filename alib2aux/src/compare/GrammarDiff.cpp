/*
 * GrammarDiff.cpp
 *
 *  Created on: Apr 1, 2013
 *      Author: honza
 */

#include "GrammarDiff.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto GrammarDiffLeftLG = registration::AbstractRegister < compare::GrammarDiff, std::string, const grammar::LeftLG < > &, const grammar::LeftLG < > & > ( compare::GrammarDiff::diff );
auto GrammarDiffLeftRG = registration::AbstractRegister < compare::GrammarDiff, std::string, const grammar::LeftRG < > &, const grammar::LeftRG < > & > ( compare::GrammarDiff::diff );
auto GrammarDiffRightLG = registration::AbstractRegister < compare::GrammarDiff, std::string, const grammar::RightLG < > &, const grammar::RightLG < > & > ( compare::GrammarDiff::diff );
auto GrammarDiffRightRG = registration::AbstractRegister < compare::GrammarDiff, std::string, const grammar::RightRG < > &, const grammar::RightRG < > & > ( compare::GrammarDiff::diff );

auto GrammarDiffLG = registration::AbstractRegister < compare::GrammarDiff, std::string, const grammar::LG < > &, const grammar::LG < > & > ( compare::GrammarDiff::diff );
auto GrammarDiffCFG = registration::AbstractRegister < compare::GrammarDiff, std::string, const grammar::CFG < > &, const grammar::CFG < > & > ( compare::GrammarDiff::diff );
auto GrammarDiffEpsilonFreeCFG = registration::AbstractRegister < compare::GrammarDiff, std::string, const grammar::EpsilonFreeCFG < > &, const grammar::EpsilonFreeCFG < > & > ( compare::GrammarDiff::diff );
auto GrammarDiffCNF = registration::AbstractRegister < compare::GrammarDiff, std::string, const grammar::CNF < > &, const grammar::CNF < > & > ( compare::GrammarDiff::diff );
auto GrammarDiffGNF = registration::AbstractRegister < compare::GrammarDiff, std::string, const grammar::GNF < > &, const grammar::GNF < > & > ( compare::GrammarDiff::diff );
auto GrammarDiffCSG = registration::AbstractRegister < compare::GrammarDiff, std::string, const grammar::CSG < > &, const grammar::CSG < > & > ( compare::GrammarDiff::diff );

auto GrammarDiffNonContractingGrammar = registration::AbstractRegister < compare::GrammarDiff, std::string, const grammar::NonContractingGrammar < > &, const grammar::NonContractingGrammar < > & > ( compare::GrammarDiff::diff );
auto GrammarDiffContextPreservingUnrestrictedGrammar = registration::AbstractRegister < compare::GrammarDiff, std::string, const grammar::ContextPreservingUnrestrictedGrammar < > &, const grammar::ContextPreservingUnrestrictedGrammar < > & > ( compare::GrammarDiff::diff );

auto GrammarDiffUnrestrictedGrammar = registration::AbstractRegister < compare::GrammarDiff, std::string, const grammar::UnrestrictedGrammar < > &, const grammar::UnrestrictedGrammar < > & > ( compare::GrammarDiff::diff );

} /* namespace */
