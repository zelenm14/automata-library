/*
 * SetCompare.h
 *
 *  Created on: Jan 14, 2019
 *      Author: Tomas Pecka
 */

#ifndef SET_COMPARE_H_
#define SET_COMPARE_H_

#include <alib/set>

namespace compare {

class SetCompare {
public:
	template < class T >
	static bool compare ( const ext::set < T > & a, const ext::set < T > & b ) {
		return a == b;
	}
};

} /* namespace compare */

#endif /* SET_COMPARE_H_ */
