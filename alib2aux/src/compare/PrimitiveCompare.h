/*
 * PrimitiveCompare.h
 *
 *  Created on: Feb 4, 2019
 *      Author: Tomas Pecka
 */

#ifndef PRIMITIVE_COMPARE_H_
#define PRIMITIVE_COMPARE_H_

#include <alib/set>

namespace compare {

class PrimitiveCompare {
public:
	template < class T >
	static bool compare ( const T & a, const T & b ) {
		return a == b;
	}
};

} /* namespace compare */

#endif /* PRIMITIVE_COMPARE_H_ */
