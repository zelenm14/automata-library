/*
 * StringCompare.h
 *
 *  Created on: Apr 4, 2017
 *      Author: Tomas Pecka
 */

#ifndef STRING_COMPARE_H_
#define STRING_COMPARE_H_

#include "string/CyclicString.h"
#include "string/LinearString.h"

namespace compare {

class StringCompare {
public:
	template < class SymbolType >
	static bool compare ( const string::LinearString < SymbolType > & a, const string::LinearString < SymbolType > & b );

	template < class SymbolType >
	static bool compare ( const string::CyclicString < SymbolType > & a, const string::CyclicString < SymbolType > & b );
};

template < class SymbolType >
bool StringCompare::compare ( const string::LinearString < SymbolType > & a, const string::LinearString < SymbolType > & b ) {
	return		a.getAlphabet ( ) == b.getAlphabet ( ) &&
			a.getContent ( ) == b.getContent ( ) ;
}

template < class SymbolType >
bool StringCompare::compare ( const string::CyclicString < SymbolType > &, const string::CyclicString < SymbolType > & ) {
	throw "NYI";
}

} /* namespace compare */

#endif /* STRING_COMPARE_H_ */
