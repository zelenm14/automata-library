/*
 * SetSizeStat.cpp
 *
 *  Created on: 20. 9. 2014
 *	  Author: Jan Travnicek
 */

#include "SizeStat.h"

#include <object/Object.h>

#include <registration/AlgoRegistration.hpp>

namespace {

auto SizeStatUnsignedsSet = registration::AbstractRegister < stats::SizeStat, unsigned, const ext::set < unsigned > & > ( stats::SizeStat::stat );
auto SizeStatObjectsSet = registration::AbstractRegister < stats::SizeStat, unsigned, const ext::set < object::Object > & > ( stats::SizeStat::stat );

}
