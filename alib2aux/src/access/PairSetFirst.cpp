/*
 * PairSetFirst.cpp
 *
 *  Created on: Nov 24, 2017
 *      Author: Jan Travnicek
 */

#include <object/Object.h>
#include "PairSetFirst.h"

#include <registration/AlgoRegistration.hpp>

namespace {

auto pairSetFirstObjectObject = registration::AbstractRegister < dataAccess::PairSetFirst, ext::set < object::Object >, const ext::set < ext::pair < object::Object, object::Object > > & > ( dataAccess::PairSetFirst::access );

} /* namespace */
