/*
 * TikZConverter.cpp
 *
 *  Created on: Dec 1, 2015
 *      Author: Jan Travnicek
 */

#include "TikZConverter.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto TikZConverterEpsilonNFA = registration::AbstractRegister<convert::TikZConverter, std::string, const automaton::EpsilonNFA<> &>( convert::TikZConverter::convert );
auto TikZConverterMultiInitialStateNFA = registration::AbstractRegister<convert::TikZConverter, std::string, const automaton::MultiInitialStateNFA<> &>( convert::TikZConverter::convert );
auto TikZConverterNFA = registration::AbstractRegister<convert::TikZConverter, std::string, const automaton::NFA<> &>( convert::TikZConverter::convert );
auto TikZConverterDFA = registration::AbstractRegister<convert::TikZConverter, std::string, const automaton::DFA<> &>( convert::TikZConverter::convert );
auto TikZConverterExtendedNFA = registration::AbstractRegister<convert::TikZConverter, std::string, const automaton::ExtendedNFA<> &>( convert::TikZConverter::convert );
auto TikZConverterCompactNFA = registration::AbstractRegister<convert::TikZConverter, std::string, const automaton::CompactNFA<> &>( convert::TikZConverter::convert );
auto TikZConverterNFTA = registration::AbstractRegister<convert::TikZConverter, std::string, const automaton::NFTA<> &>( convert::TikZConverter::convert );
auto TikZConverterDFTA = registration::AbstractRegister<convert::TikZConverter, std::string, const automaton::DFTA<> &>( convert::TikZConverter::convert );
auto TikZConverterDPDA = registration::AbstractRegister<convert::TikZConverter, std::string, const automaton::DPDA<> &>( convert::TikZConverter::convert );
auto TikZConverterSinglePopDPDA = registration::AbstractRegister<convert::TikZConverter, std::string, const automaton::SinglePopDPDA<> &>( convert::TikZConverter::convert );
auto TikZConverterInputDrivenDPDA = registration::AbstractRegister<convert::TikZConverter, std::string, const automaton::InputDrivenDPDA<> &>( convert::TikZConverter::convert );
auto TikZConverterInputDrivenNPDA = registration::AbstractRegister<convert::TikZConverter, std::string, const automaton::InputDrivenNPDA<> &>( convert::TikZConverter::convert );
auto TikZConverterVisiblyPushdownDPDA = registration::AbstractRegister<convert::TikZConverter, std::string, const automaton::VisiblyPushdownDPDA<> &>( convert::TikZConverter::convert );
auto TikZConverterVisiblyPushdownNPDA = registration::AbstractRegister<convert::TikZConverter, std::string, const automaton::VisiblyPushdownNPDA<> &>( convert::TikZConverter::convert );
auto TikZConverterRealTimeHeightDeterministicDPDA = registration::AbstractRegister<convert::TikZConverter, std::string, const automaton::RealTimeHeightDeterministicDPDA<> &>( convert::TikZConverter::convert );
auto TikZConverterRealTimeHeightDeterministicNPDA = registration::AbstractRegister<convert::TikZConverter, std::string, const automaton::RealTimeHeightDeterministicNPDA<> &>( convert::TikZConverter::convert );
auto TikZConverterNPDA = registration::AbstractRegister<convert::TikZConverter, std::string, const automaton::NPDA<> &>( convert::TikZConverter::convert );
auto TikZConverterSinglePopNPDA = registration::AbstractRegister<convert::TikZConverter, std::string, const automaton::SinglePopNPDA<> &>( convert::TikZConverter::convert );
auto TikZConverterOneTapeDTM = registration::AbstractRegister<convert::TikZConverter, std::string, const automaton::OneTapeDTM<> &>( convert::TikZConverter::convert );
auto TikZConverterSquareGrid4 = registration::AbstractRegister<convert::TikZConverter, std::string, const grid::SquareGrid4<> &>( convert::TikZConverter::convert );
auto TikZConverterSquareGrid8 = registration::AbstractRegister<convert::TikZConverter, std::string, const grid::SquareGrid8<> &>( convert::TikZConverter::convert );
auto TikZConverterWeightedSquareGrid4 = registration::AbstractRegister<convert::TikZConverter, std::string, const grid::WeightedSquareGrid4<> &>( convert::TikZConverter::convert );
auto TikZConverterWeightedSquareGrid8 = registration::AbstractRegister<convert::TikZConverter, std::string, const grid::WeightedSquareGrid8<> &>( convert::TikZConverter::convert );

auto TikzRTE = registration::AbstractRegister < convert::TikZConverter, std::string, const rte::FormalRTE < > & > ( convert::TikZConverter::convert, "rte" ).setDocumentation ( "Converts a regular tree expression to Latex (tikz-forest) representation." );

} /* namespace */
