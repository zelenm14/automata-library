/*
 * DotConverterTree.h
 *
 *  Created on: 15. 3. 2019
 *	  Author: Tomas Pecka
 */

#ifndef _DOT_CONVERTER_TREE_H_
#define _DOT_CONVERTER_TREE_H_

#include <alphabet/string/RankedSymbol.h>

#include <tree/ranked/RankedTree.h>

namespace convert {

class DotConverterTree {
	template < class SymbolType >
	static unsigned convertRecursive ( const ext::tree < SymbolType > & node, std::ostream & oss, unsigned & nodeIdCounter );

public:
	template < class SymbolType >
	static void convertInternal ( std::ostream & oss, const tree::RankedTree < SymbolType > & tree );
};

template < class SymbolType >
void DotConverterTree::convertInternal ( std::ostream & oss, const tree::RankedTree < SymbolType > & tree ) {
	unsigned nodeIdCounter = 0;
	convertRecursive ( tree.getContent ( ), oss, nodeIdCounter );
}

template < class SymbolType >
unsigned DotConverterTree::convertRecursive ( const ext::tree < SymbolType > & node, std::ostream & oss, unsigned & nodeIdCounter ) {
	unsigned id = nodeIdCounter ++;

	oss << id << "[label=\"" + factory::StringDataFactory::toString ( node.getData ( ) ) + "\", shape=plaintext];" << std::endl;

	for ( const ext::tree < SymbolType > & child : node.getChildren ( ) ) {
		size_t childId = convertRecursive ( child, oss, nodeIdCounter );
		oss << id << " -> " << childId << ";" << std::endl;
	}

	return id;
}

} /* namespace convert */

#endif /* _DOT_CONVERTER_TREE_H_ */
