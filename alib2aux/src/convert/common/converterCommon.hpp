/*
 * ConverterCommon.hpp
 *
 *  Created on: Nov 24, 2016
 *      Author: Jan Travnicek
 */

#ifndef CONVERTER_COMMON_HPP_
#define CONVERTER_COMMON_HPP_

namespace convert {

inline auto replaceInplace = [] ( std::string & str, const std::string & what, const std::string & with ) {
	size_t index = 0;

	while ( ( index = str.find ( what, index ) ) != std::string::npos ) {
		str.replace ( index, what.length ( ), with );
		index += with.length ( );
	}
};

inline auto replace = [] ( std::string str, const std::string & what, const std::string & with ) {
	replaceInplace ( str, what, with );
	return str;
};

} /* namespace convert */

#endif /* CONVERTER_COMMON_HPP_ */
