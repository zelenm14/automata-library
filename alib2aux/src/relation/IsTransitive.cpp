/*
* IsTransitive.cpp
 *
 *  Created on: 2. 10. 2019
 *	  Author: Jan Travnicek
 */

#include "IsTransitive.h"
#include <registration/AlgoRegistration.hpp>

#include <object/Object.h>

namespace {

auto IsTransitive = registration::AbstractRegister < relation::IsTransitive, bool, const ext::set < ext::pair < object::Object, object::Object > > & > ( relation::IsTransitive::isTransitive, "relation" ).setDocumentation (
"Checks whether a relation is transitive.\n\
\n\
@param relation the tested relation\n\
\n\
@return true if the relation is transitive, false otherwise");

} /* namespace */
