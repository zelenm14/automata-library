/*
 * Random.h
 *
 *  Created on: 18. 3. 2019
 *	  Author: Jan Travnicek
 */

#include "Random.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto RandomInt = registration::AbstractRegister < debug::Random < int >, int > ( debug::Random < int >::random );
auto RandomUnsigned = registration::AbstractRegister < debug::Random < unsigned >, unsigned > ( debug::Random < unsigned >::random );

} /* namespace */
