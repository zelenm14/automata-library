/*
 * Author: Radovan Cerveny
 */
#include "StealthTypes.hpp"

namespace measurements {

std::string to_string ( const stealth_string & ss ) {
	return ss.c_str ( );
}

std::string to_string ( stealth_string & ss ) {
	return ss.c_str ( );
}

stealth_string stealthStringFromString ( const std::string & str ) {
	return stealth_string ( str.c_str ( ) );
}

}
