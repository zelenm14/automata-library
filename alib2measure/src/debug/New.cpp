/*
 * Author: Radovan Cerveny
 */

#include <cstdlib>
#include <new>
#include "../measurements/MeasurementNew.hpp"

#if 0
	void * operator new( std::size_t n ) {
		return operator new( n, true );
	}

	void operator delete( void * ptr ) noexcept {
		operator delete( ptr, true );
	}

	void operator delete( void * ptr, std::size_t ) noexcept {
		operator delete( ptr, true );
	}
#endif
