// EdgeTypes.hpp
//
//     Created on: 17. 12. 2017
//         Author: Jan Uhlik
//    Modified by:
//
// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.
// Git repository: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library

#ifndef ALIB2_EDGETYPES_HPP
#define ALIB2_EDGETYPES_HPP

#include "Edge.hpp"
#include "weighted/WeightedEdge.hpp"
#include "capacity/CapacityEdge.hpp"

#endif //ALIB2_EDGETYPES_HPP
