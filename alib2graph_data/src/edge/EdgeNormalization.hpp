// EdgeNormalization.hpp
//
//     Created on: 04. 03. 2018
//         Author: Jan Uhlik
//    Modified by:
//
// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.
// Git repository: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library

#ifndef ALIB2_EDGENORMALIZATION_HPP
#define ALIB2_EDGENORMALIZATION_HPP

#include <core/normalize.hpp>
#include <common/Normalize.hpp>

#include "EdgeClasses.hpp"

namespace core {

/**
 * Helper for normalisation of types specified by templates used as internal datatypes of symbols and states.
 *
 * \returns new instance of the graph with default template parameters or unmodified instance if the template parameters were already the default ones
 */

template<typename TNode>
struct normalize<edge::Edge<TNode>, typename std::enable_if<!std::is_same<
    edge::Edge<TNode>, edge::Edge<> >::value>::type> {
  static edge::Edge<> eval(edge::Edge<TNode> &&value) {
    DefaultNodeType first = common::Normalize::normalizeNode(value.first);
    DefaultNodeType second = common::Normalize::normalizeNode(value.second);
    return edge::Edge(std::move(first), std::move(second));
  }
};

// ---------------------------------------------------------------------------------------------------------------------

/**
 * Helper for normalisation of types specified by templates used as internal datatypes of symbols and states.
 *
 * \returns new instance of the graph with default template parameters or unmodified instance if the template parameters were already the default ones
 */

template<typename TNode>
struct normalize<edge::WeightedEdge<TNode>, typename std::enable_if<!std::is_same<
    edge::WeightedEdge<TNode>, edge::WeightedEdge<> >::value>::type> {
  static edge::WeightedEdge<> eval(edge::WeightedEdge<TNode> &&value) {
    return common::Normalize::normalizeWeightedEdge(value);
  }
};

// ---------------------------------------------------------------------------------------------------------------------

/**
 * Helper for normalisation of types specified by templates used as internal datatypes of symbols and states.
 *
 * \returns new instance of the graph with default template parameters or unmodified instance if the template parameters were already the default ones
 */

template<typename TNode>
struct normalize<edge::CapacityEdge<TNode>, typename std::enable_if<!std::is_same<
    edge::CapacityEdge<TNode>, edge::CapacityEdge<> >::value>::type> {
  static edge::WeightedEdge<> eval(edge::WeightedEdge<TNode> &&value) {
    return common::Normalize::normalizeCapacityEdge(value);
  }
};

// ---------------------------------------------------------------------------------------------------------------------

}

#endif //ALIB2_EDGENORMALIZATION_HPP
