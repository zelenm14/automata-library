// EdgeBase.hpp
//
//     Created on: 04. 03. 2018
//         Author: Jan Uhlik
//    Modified by:
//
// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.
// Git repository: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library

#ifndef ALIB2_EDGEBASE_HPP
#define ALIB2_EDGEBASE_HPP

#include <alib/compare>

namespace edge {

/**
 * Represents edge in graph.
 */
class EdgeBase {
public:
	virtual ~EdgeBase ( ) noexcept = default;

// ---------------------------------------------------------------------------------------------------------------------

 public:
// ---------------------------------------------------------------------------------------------------------------------
	friend std::ostream & operator << ( std::ostream & os, const EdgeBase & instance ) {
		instance >> os;
		return os;
	}

	virtual void operator >> ( std::ostream & os ) const = 0;

	virtual operator std::string ( ) const = 0;
};

} // namespace edge

#endif //ALIB2_EDGEBASE_HPP
