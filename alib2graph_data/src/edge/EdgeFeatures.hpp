// EdgeFeatures.hpp
//
//     Created on: 01. 03. 2018
//         Author: Jan Uhlik
//    Modified by:
//
// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.
// Git repository: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library

#ifndef ALIB2_EDGEFEATURES_HPP
#define ALIB2_EDGEFEATURES_HPP

#include <common/default_type/DefaultNodeType.hpp>
#include <common/default_type/DefaultWeightType.hpp>
#include <common/default_type/DefaultCapacityType.hpp>

namespace edge {

template<typename TNode = DefaultNodeType>
class Edge;

template<typename TNode = DefaultNodeType, typename TWeight = DefaultWeightType>
class WeightedEdge;

template<typename TNode = DefaultNodeType, typename TWeight = DefaultCapacityType>
class CapacityEdge;

} // namespace edge

#endif //ALIB2_EDGEFEATURES_HPP
