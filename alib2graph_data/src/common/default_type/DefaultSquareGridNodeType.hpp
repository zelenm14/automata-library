// DefaultSquareGridNodeType.hpp
//
//     Created on: 04. 03. 2018
//         Author: Jan Uhlik
//    Modified by:
//
// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.
// Git repository: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library

#ifndef ALIB2_DEFAULTSQUAREGRIDNODETYPE_HPP
#define ALIB2_DEFAULTSQUAREGRIDNODETYPE_HPP

#include <alib/pair>
#include "DefaultCoordinateType.hpp"

typedef ext::pair<DefaultCoordinateType, DefaultCoordinateType> DefaultSquareGridNodeType;

#endif //ALIB2_DEFAULTSQUAREGRIDNODETYPE_HPP
