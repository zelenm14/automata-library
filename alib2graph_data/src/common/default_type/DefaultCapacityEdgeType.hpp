// DefaultCapacityEdgeType.hpp
//
//     Created on: 29. 03. 2018
//         Author: Jan Uhlik
//    Modified by:
//
// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.
// Git repository: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library

#ifndef ALIB2_DEFAULTCAPACITYEDGETYPE_HPP
#define ALIB2_DEFAULTCAPACITYEDGETYPE_HPP

#include <common/default_type/DefaultNodeType.hpp>
#include <common/default_type/DefaultCapacityType.hpp>
#include <edge/capacity/CapacityEdge.hpp>

typedef edge::CapacityEdge<DefaultNodeType, DefaultCapacityType> DefaultCapacityEdgeType;

#endif //ALIB2_DEFAULTCAPACITYEDGETYPE_HPP
