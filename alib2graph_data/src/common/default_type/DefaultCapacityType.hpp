// DefaultCapacityType.h
//
//     Created on: 28. 03. 2018
//         Author: Jan Uhlik
//    Modified by:
//
// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.
// Git repository: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library

#ifndef ALIB2_DEFAULTCAPACITYTYPE_H
#define ALIB2_DEFAULTCAPACITYTYPE_H

typedef unsigned long DefaultCapacityType;

#endif //ALIB2_DEFAULTCAPACITYTYPE_H
