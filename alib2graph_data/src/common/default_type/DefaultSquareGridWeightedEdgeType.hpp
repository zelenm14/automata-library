// DefaultSquareGridWeightedEdgeType.hpp
//
//     Created on: 04. 03. 2018
//         Author: Jan Uhlik
//    Modified by:
//
// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.
// Git repository: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library

#ifndef ALIB2_DEFAULTSQUAREGRIDWEIGHTEDEDGETYPE_HPP
#define ALIB2_DEFAULTSQUAREGRIDWEIGHTEDEDGETYPE_HPP

#include <alib/pair>
#include <edge/weighted/WeightedEdge.hpp>
#include "DefaultSquareGridNodeType.hpp"
#include "DefaultWeightType.hpp"

typedef edge::WeightedEdge<DefaultSquareGridNodeType, DefaultWeightType>
    DefaultSquareGridWeightedEdgeType;

#endif //ALIB2_DEFAULTSQUAREGRIDWEIGHTEDEDGETYPE_HPP
