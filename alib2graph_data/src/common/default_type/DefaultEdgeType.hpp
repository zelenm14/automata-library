/*
 * DefaultEdgeType.h
 *
 *  Created on: Jan 12, 2017
 *      Author: Jan Travnicek
 */

#ifndef DEFAULT_EDGE_TYPE_H_
#define DEFAULT_EDGE_TYPE_H_

#include <alib/pair>
#include <object/Object.h>
#include "DefaultNodeType.hpp"

typedef ext::pair<DefaultNodeType, DefaultNodeType> DefaultEdgeType;

#endif /* DEFAULT_EDGE_TYPE_H_ */
