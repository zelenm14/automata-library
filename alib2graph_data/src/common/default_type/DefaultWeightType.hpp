// DefaultWeightType.hpp
//
//     Created on: 01. 03. 2018
//         Author: Jan Uhlik
//    Modified by:
//
// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.
// Git repository: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library

#ifndef ALIB2_DEFAULTWEIGHTTYPE_HPP
#define ALIB2_DEFAULTWEIGHTTYPE_HPP

typedef double DefaultWeightType;

#endif //ALIB2_DEFAULTWEIGHTTYPE_HPP
