/*
 * DefaultNodeType.h
 *
 *  Created on: Jan 12, 2017
 *      Author: Jan Travnicek
 */

#ifndef DEFAULT_NODE_TYPE_H_
#define DEFAULT_NODE_TYPE_H_

#include <object/Object.h>

typedef object::Object DefaultNodeType;

#endif /* DEFAULT_NODE_TYPE_H_ */
