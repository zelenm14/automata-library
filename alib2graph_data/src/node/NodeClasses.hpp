// NodeTypes.hpp
//
//     Created on: 18. 12. 2017
//         Author: Jan Uhlik
//    Modified by:
//
// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.
// Git repository: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library

#ifndef ALIB2_NODETYPES_HPP
#define ALIB2_NODETYPES_HPP

#include "Node.hpp"

#endif //ALIB2_NODETYPES_HPP
