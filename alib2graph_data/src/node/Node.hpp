// Node.hpp
//
//     Created on: 18. 12. 2017
//         Author: Jan Uhlik
//    Modified by:
//
// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.
// Git repository: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library

#ifndef ALIB2_NODE_HPP
#define ALIB2_NODE_HPP

#include <ostream>
#include <object/Object.h>

#include "NodeBase.hpp"

namespace node {

class Node : public NodeBase {
// ---------------------------------------------------------------------------------------------------------------------

 private:
  int m_id;

// =====================================================================================================================
// Constructor, Destructor, Operators

 public:
  explicit Node();
  explicit Node(int id);

// ---------------------------------------------------------------------------------------------------------------------

  auto operator <=> ( const Node &rhs) const { return m_id <=> rhs.m_id; }
  bool operator ==( const Node &rhs) const { return m_id == rhs.m_id; }

// ---------------------------------------------------------------------------------------------------------------------
// =====================================================================================================================
// NodeBase interface

 public:
  void operator>>(std::ostream &ostream) const override;

  explicit operator std::string() const override;

// =====================================================================================================================
 public:

  std::string name() const;

// ---------------------------------------------------------------------------------------------------------------------

  int getId() const;

// ---------------------------------------------------------------------------------------------------------------------

};

// =====================================================================================================================

} // namespace node

// =====================================================================================================================

namespace std {

template<>
struct hash<node::Node> {
  std::size_t operator()(const node::Node &k) const {
    return hash<int>()(k.getId());
  }
};
}

// =====================================================================================================================

#endif //ALIB2_NODE_HPP
