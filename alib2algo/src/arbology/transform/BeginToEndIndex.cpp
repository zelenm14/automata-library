/*
 * BeginToEndIndex.cpp
 *
 *  Created on: 5. 11. 2014
 *      Author: Jan Travnicek
 */

#include "BeginToEndIndex.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto BeginToEndIndexPrefixRankedBarTreePrefixRankedBarTree = registration::AbstractRegister < arbology::transform::BeginToEndIndex, ext::set < unsigned >, const tree::PrefixRankedBarTree < > &, const ext::set < unsigned > & > ( arbology::transform::BeginToEndIndex::transform );
auto BeginToEndIndexPrefixRankedBarTreePrefixRankedBarPattern = registration::AbstractRegister < arbology::transform::BeginToEndIndex, ext::set < unsigned >, const tree::PrefixRankedTree < > &, const ext::set < unsigned > & > ( arbology::transform::BeginToEndIndex::transform );

} /* namespace */
