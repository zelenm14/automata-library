/*
 * ExactSubtreeMatch.cpp
 *
 *  Created on: 9. 2. 2014
 *      Author: Jan Travnicek
 */

#include "ExactSubtreeMatch.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ExactSubtreeMatchUnrankedTree = registration::AbstractRegister < arbology::exact::ExactSubtreeMatch, ext::set < unsigned >, const tree::UnrankedTree < > &, const tree::UnrankedTree < > & > ( arbology::exact::ExactSubtreeMatch::match );
auto ExactSubtreeMatchRankedTree = registration::AbstractRegister < arbology::exact::ExactSubtreeMatch, ext::set < unsigned >, const tree::RankedTree < > &, const tree::RankedTree < > & > ( arbology::exact::ExactSubtreeMatch::match );
auto ExactSubtreeMatchPrefixRankedTree = registration::AbstractRegister < arbology::exact::ExactSubtreeMatch, ext::set < unsigned >, const tree::PrefixRankedTree < > &, const tree::PrefixRankedTree < > & > ( arbology::exact::ExactSubtreeMatch::match );
auto ExactSubtreeMatchPrefixRankedBarTree = registration::AbstractRegister < arbology::exact::ExactSubtreeMatch, ext::set < unsigned >, const tree::PrefixRankedBarTree < > &, const tree::PrefixRankedBarTree < > & > ( arbology::exact::ExactSubtreeMatch::match );

} /* namespace */
