/*
 * KnuthMorrisPratt.cpp
 *
 *  Created on: 5. 11. 2014
 *      Author: Jan Travnicek
 */

#include "KnuthMorrisPratt.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto KnuthMorrisPrattPrefixRankedBarTreePrefixRankedBarTree = registration::AbstractRegister < arbology::exact::KnuthMorrisPratt, ext::set < unsigned >, const tree::PrefixRankedBarTree < > &, const tree::PrefixRankedBarTree < > & > ( arbology::exact::KnuthMorrisPratt::match );

auto KnuthMorrisPrattPrefixRankedBarTreePrefixRankedBarPattern = registration::AbstractRegister < arbology::exact::KnuthMorrisPratt, ext::set < unsigned >, const tree::PrefixRankedBarTree < > &, const tree::PrefixRankedBarPattern < > & > ( arbology::exact::KnuthMorrisPratt::match );

auto KnuthMorrisPrattPrefixRankedBarTreePrefixRankedBarNonlinearPattern = registration::AbstractRegister < arbology::exact::KnuthMorrisPratt, ext::set < unsigned >, const tree::PrefixRankedBarTree < > &, const tree::PrefixRankedBarNonlinearPattern < > & > ( arbology::exact::KnuthMorrisPratt::match );

auto KnuthMorrisPrattPrefixRankedTreePrefixRankedTree = registration::AbstractRegister < arbology::exact::KnuthMorrisPratt, ext::set < unsigned >, const tree::PrefixRankedTree < > &, const tree::PrefixRankedTree < > & > ( arbology::exact::KnuthMorrisPratt::match );

auto KnuthMorrisPrattPrefixRankedTreePrefixRankedPattern = registration::AbstractRegister < arbology::exact::KnuthMorrisPratt, ext::set < unsigned >, const tree::PrefixRankedTree < > &, const tree::PrefixRankedPattern < > & > ( arbology::exact::KnuthMorrisPratt::match );

auto KnuthMorrisPrattPrefixRankedTreePrefixRankedNonlinearPattern = registration::AbstractRegister < arbology::exact::KnuthMorrisPratt, ext::set < unsigned >, const tree::PrefixRankedTree < > &, const tree::PrefixRankedNonlinearPattern < > & > ( arbology::exact::KnuthMorrisPratt::match );

} /* namespace */
