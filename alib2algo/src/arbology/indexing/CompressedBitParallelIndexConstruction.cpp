/*
 * CompressedBitParallelIndexConstruction.cpp
 *
 *  Created on: 6. 2. 2017
 *      Author: Jan Travnicek
 */

#include "CompressedBitParallelIndexConstruction.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto compressedBitParallelIndexConstructionPrefixRankedTree = registration::AbstractRegister < arbology::indexing::CompressedBitParallelIndexConstruction, indexes::arbology::CompressedBitParallelTreeIndex < DefaultSymbolType >, const tree::PrefixRankedTree < > & > ( arbology::indexing::CompressedBitParallelIndexConstruction::construct );

auto compressedBitParallelIndexConstructionPrefixRankedBarTree = registration::AbstractRegister < arbology::indexing::CompressedBitParallelIndexConstruction, indexes::arbology::CompressedBitParallelTreeIndex < DefaultSymbolType >, const tree::PrefixRankedBarTree < > & > ( arbology::indexing::CompressedBitParallelIndexConstruction::construct );

} /* namespace */
