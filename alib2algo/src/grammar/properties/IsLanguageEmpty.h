/*
 * IsLanguageEmpty.h
 *
 *  Created on: 22. 3. 2014
 *	  Author: Tomas Pecka
 */

#ifndef IS_LANGUAGE_EMPTY_H_
#define IS_LANGUAGE_EMPTY_H_

#include <grammar/properties/ProductiveNonterminals.h>

namespace grammar {

namespace properties {

/**
 * Based on algorithm 3.6 from Melichar, chapter 3.3
 */
class IsLanguageEmpty {
public:
	/*
	 * Decides whether L( grammar ) = \0
	 *
	 * Severals steps implemented in method @see grammar::properties::ProductiveNonterminals::getProductiveNonTerminals();
	 *
	 * \tparam T the type of the tested grammar
	 *
	 * \param grammar the tested grammar
	 *
	 * \returns true if L(@p grammar) = \0
	 */
	template<class T>
	static bool isLanguageEmpty( const T & grammar );
};

template<class T>
bool IsLanguageEmpty::isLanguageEmpty( const T & grammar ) {
	return grammar::properties::ProductiveNonterminals::getProductiveNonterminals( grammar ).count( grammar.getInitialSymbol( ) );
}

} /* namespace properties */

} /* namespace grammar */

#endif /* IS_LANGUAGE_EMPTY_H_ */
