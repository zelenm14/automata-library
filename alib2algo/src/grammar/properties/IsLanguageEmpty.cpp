/*
 * IsLanguageEmpty.cpp
 *
 *  Created on: 22. 3. 2014
 *	  Author: Tomas Pecka
 */

#include <grammar/ContextFree/CFG.h>
#include <grammar/ContextFree/EpsilonFreeCFG.h>
#include <grammar/ContextFree/GNF.h>
#include <grammar/ContextFree/CNF.h>
#include <grammar/ContextFree/LG.h>
#include <grammar/Regular/LeftLG.h>
#include <grammar/Regular/LeftRG.h>
#include <grammar/Regular/RightLG.h>
#include <grammar/Regular/RightRG.h>

#include "IsLanguageEmpty.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto IsLanguageEmptyCFG = registration::AbstractRegister < grammar::properties::IsLanguageEmpty, bool, const grammar::CFG < > & > ( grammar::properties::IsLanguageEmpty::isLanguageEmpty, "grammar" ).setDocumentation (
"Decides whether L( grammar ) = \\0\n\
\n\
Severals steps implemented in @see grammar::properties::ProductiveNonterminals\n\
\n\
@param grammar the tested grammar\n\
@returns true if L(@p grammar) = \\0");

auto IsLanguageEmptyEpsilonFreeCFG = registration::AbstractRegister < grammar::properties::IsLanguageEmpty, bool, const grammar::EpsilonFreeCFG < > & > ( grammar::properties::IsLanguageEmpty::isLanguageEmpty, "grammar" ).setDocumentation (
"Decides whether L( grammar ) = \\0\n\
\n\
Severals steps implemented in @see grammar::properties::ProductiveNonterminals\n\
\n\
@param grammar the tested grammar\n\
@returns true if L(@p grammar) = \\0");

auto IsLanguageEmptyGNF = registration::AbstractRegister < grammar::properties::IsLanguageEmpty, bool, const grammar::GNF < > & > ( grammar::properties::IsLanguageEmpty::isLanguageEmpty, "grammar" ).setDocumentation (
"Decides whether L( grammar ) = \\0\n\
\n\
Severals steps implemented in @see grammar::properties::ProductiveNonterminals\n\
\n\
@param grammar the tested grammar\n\
@returns true if L(@p grammar) = \\0");

auto IsLanguageEmptyCNF = registration::AbstractRegister < grammar::properties::IsLanguageEmpty, bool, const grammar::CNF < > & > ( grammar::properties::IsLanguageEmpty::isLanguageEmpty, "grammar" ).setDocumentation (
"Decides whether L( grammar ) = \\0\n\
\n\
Severals steps implemented in @see grammar::properties::ProductiveNonterminals\n\
\n\
@param grammar the tested grammar\n\
@returns true if L(@p grammar) = \\0");

auto IsLanguageEmptyLG = registration::AbstractRegister < grammar::properties::IsLanguageEmpty, bool, const grammar::LG < > & > ( grammar::properties::IsLanguageEmpty::isLanguageEmpty, "grammar" ).setDocumentation (
"Decides whether L( grammar ) = \\0\n\
\n\
Severals steps implemented in @see grammar::properties::ProductiveNonterminals\n\
\n\
@param grammar the tested grammar\n\
@returns true if L(@p grammar) = \\0");

auto IsLanguageEmptyLeftLG = registration::AbstractRegister < grammar::properties::IsLanguageEmpty, bool, const grammar::LeftLG < > & > ( grammar::properties::IsLanguageEmpty::isLanguageEmpty, "grammar" ).setDocumentation (
"Decides whether L( grammar ) = \\0\n\
\n\
Severals steps implemented in @see grammar::properties::ProductiveNonterminals\n\
\n\
@param grammar the tested grammar\n\
@returns true if L(@p grammar) = \\0");

auto IsLanguageEmptyLeftRG = registration::AbstractRegister < grammar::properties::IsLanguageEmpty, bool, const grammar::LeftRG < > & > ( grammar::properties::IsLanguageEmpty::isLanguageEmpty, "grammar" ).setDocumentation (
"Decides whether L( grammar ) = \\0\n\
\n\
Severals steps implemented in @see grammar::properties::ProductiveNonterminals\n\
\n\
@param grammar the tested grammar\n\
@returns true if L(@p grammar) = \\0");

auto IsLanguageEmptyRightLG = registration::AbstractRegister < grammar::properties::IsLanguageEmpty, bool, const grammar::RightLG < > & > ( grammar::properties::IsLanguageEmpty::isLanguageEmpty, "grammar" ).setDocumentation (
"Decides whether L( grammar ) = \\0\n\
\n\
Severals steps implemented in @see grammar::properties::ProductiveNonterminals\n\
\n\
@param grammar the tested grammar\n\
@returns true if L(@p grammar) = \\0");

auto IsLanguageEmptyRightRG = registration::AbstractRegister < grammar::properties::IsLanguageEmpty, bool, const grammar::RightRG < > & > ( grammar::properties::IsLanguageEmpty::isLanguageEmpty, "grammar" ).setDocumentation (
"Decides whether L( grammar ) = \\0\n\
\n\
Severals steps implemented in @see grammar::properties::ProductiveNonterminals\n\
\n\
@param grammar the tested grammar\n\
@returns true if L(@p grammar) = \\0");

} /* namespace */

