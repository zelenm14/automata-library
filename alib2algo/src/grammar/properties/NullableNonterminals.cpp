/*
 * NullableNonterminals.cpp
 *
 *  Created on: 22. 3. 2014
 *	  Author: Tomas Pecka
 */

#include <grammar/ContextFree/CFG.h>
#include <grammar/ContextFree/EpsilonFreeCFG.h>
#include <grammar/ContextFree/GNF.h>
#include <grammar/ContextFree/CNF.h>
#include <grammar/ContextFree/LG.h>
#include <grammar/Regular/LeftLG.h>
#include <grammar/Regular/LeftRG.h>
#include <grammar/Regular/RightLG.h>
#include <grammar/Regular/RightRG.h>

#include "NullableNonterminals.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto NullableNonterminalsCFG = registration::AbstractRegister < grammar::properties::NullableNonterminals, ext::set < DefaultSymbolType >, const grammar::CFG < > & > ( grammar::properties::NullableNonterminals::getNullableNonterminals, "grammar" ).setDocumentation (
"Retrieve all nullable nonterminals from grammar\n\
Nullable nonterminal is such nonterminal A for which holds that A ->^* \\eps\n\
Source: Melichar, algorithm 2.4, step 1\n\
\n\
@param grammar the tested grammar\n\
@return set of nullable nonterminals from the @p grammar");

auto NullableNonterminalsEpsilonFreeCFG = registration::AbstractRegister < grammar::properties::NullableNonterminals, ext::set < DefaultSymbolType >, const grammar::EpsilonFreeCFG < > & > ( grammar::properties::NullableNonterminals::getNullableNonterminals, "grammar" ).setDocumentation (
"Retrieve all nullable nonterminals from grammar\n\
Nullable nonterminal is such nonterminal A for which holds that A ->^* \\eps\n\
Source: Melichar, algorithm 2.4, step 1\n\
\n\
@param grammar the tested grammar\n\
@return set of nullable nonterminals from the @p grammar");

auto NullableNonterminalsGNF = registration::AbstractRegister < grammar::properties::NullableNonterminals, ext::set < DefaultSymbolType >, const grammar::GNF < > & > ( grammar::properties::NullableNonterminals::getNullableNonterminals, "grammar" ).setDocumentation (
"Retrieve all nullable nonterminals from grammar\n\
Nullable nonterminal is such nonterminal A for which holds that A ->^* \\eps\n\
Source: Melichar, algorithm 2.4, step 1\n\
\n\
@param grammar the tested grammar\n\
@return set of nullable nonterminals from the @p grammar");

auto NullableNonterminalsCNF = registration::AbstractRegister < grammar::properties::NullableNonterminals, ext::set < DefaultSymbolType >, const grammar::CNF < > & > ( grammar::properties::NullableNonterminals::getNullableNonterminals, "grammar" ).setDocumentation (
"Retrieve all nullable nonterminals from grammar\n\
Nullable nonterminal is such nonterminal A for which holds that A ->^* \\eps\n\
Source: Melichar, algorithm 2.4, step 1\n\
\n\
@param grammar the tested grammar\n\
@return set of nullable nonterminals from the @p grammar");

auto NullableNonterminalsLG = registration::AbstractRegister < grammar::properties::NullableNonterminals, ext::set < DefaultSymbolType >, const grammar::LG < > & > ( grammar::properties::NullableNonterminals::getNullableNonterminals, "grammar" ).setDocumentation (
"Retrieve all nullable nonterminals from grammar\n\
Nullable nonterminal is such nonterminal A for which holds that A ->^* \\eps\n\
Source: Melichar, algorithm 2.4, step 1\n\
\n\
@param grammar the tested grammar\n\
@return set of nullable nonterminals from the @p grammar");

auto NullableNonterminalsLeftLG = registration::AbstractRegister < grammar::properties::NullableNonterminals, ext::set < DefaultSymbolType >, const grammar::LeftLG < > & > ( grammar::properties::NullableNonterminals::getNullableNonterminals, "grammar" ).setDocumentation (
"Retrieve all nullable nonterminals from grammar\n\
Nullable nonterminal is such nonterminal A for which holds that A ->^* \\eps\n\
Source: Melichar, algorithm 2.4, step 1\n\
\n\
@param grammar the tested grammar\n\
@return set of nullable nonterminals from the @p grammar");

auto NullableNonterminalsLeftRG = registration::AbstractRegister < grammar::properties::NullableNonterminals, ext::set < DefaultSymbolType >, const grammar::LeftRG < > & > ( grammar::properties::NullableNonterminals::getNullableNonterminals, "grammar" ).setDocumentation (
"Retrieve all nullable nonterminals from grammar\n\
Nullable nonterminal is such nonterminal A for which holds that A ->^* \\eps\n\
Source: Melichar, algorithm 2.4, step 1\n\
\n\
@param grammar the tested grammar\n\
@return set of nullable nonterminals from the @p grammar");

auto NullableNonterminalsRightLG = registration::AbstractRegister < grammar::properties::NullableNonterminals, ext::set < DefaultSymbolType >, const grammar::RightLG < > & > ( grammar::properties::NullableNonterminals::getNullableNonterminals, "grammar" ).setDocumentation (
"Retrieve all nullable nonterminals from grammar\n\
Nullable nonterminal is such nonterminal A for which holds that A ->^* \\eps\n\
Source: Melichar, algorithm 2.4, step 1\n\
\n\
@param grammar the tested grammar\n\
@return set of nullable nonterminals from the @p grammar");

auto NullableNonterminalsRightRG = registration::AbstractRegister < grammar::properties::NullableNonterminals, ext::set < DefaultSymbolType >, const grammar::RightRG < > & > ( grammar::properties::NullableNonterminals::getNullableNonterminals, "grammar" ).setDocumentation (
"Retrieve all nullable nonterminals from grammar\n\
Nullable nonterminal is such nonterminal A for which holds that A ->^* \\eps\n\
Source: Melichar, algorithm 2.4, step 1\n\
\n\
@param grammar the tested grammar\n\
@return set of nullable nonterminals from the @p grammar");

} /* namespace */
