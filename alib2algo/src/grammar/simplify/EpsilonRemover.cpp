/*
 * EpsilonRemover.cpp
 *
 *  Created on: 24. 11. 2014
 *	  Author: Jan Travnicek
 */

#include "EpsilonRemover.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto EpsilonRemoverCFG = registration::AbstractRegister < grammar::simplify::EpsilonRemover, grammar::EpsilonFreeCFG < >, const grammar::CFG < > & > ( grammar::simplify::EpsilonRemover::remove, "grammar" ).setDocumentation (
"Removes epsilon rules from the given grammar.\n\
\n\
@param grammar the modified grammar\n\
@return grammar without epsilon rules" );

auto EpsilonRemoverEpsilonFreeCFG = registration::AbstractRegister < grammar::simplify::EpsilonRemover, grammar::EpsilonFreeCFG < >, const grammar::EpsilonFreeCFG < > & > ( grammar::simplify::EpsilonRemover::remove, "grammar" ).setDocumentation (
"Removes epsilon rules from the given grammar.\n\
\n\
@param grammar the modified grammar\n\
@return grammar without epsilon rules" );

auto EpsilonRemoverCNF = registration::AbstractRegister < grammar::simplify::EpsilonRemover, grammar::CNF < >, const grammar::CNF < > & > ( grammar::simplify::EpsilonRemover::remove, "grammar" ).setDocumentation (
"Removes epsilon rules from the given grammar.\n\
\n\
@param grammar the modified grammar\n\
@return grammar without epsilon rules" );

auto EpsilonRemoverGNF = registration::AbstractRegister < grammar::simplify::EpsilonRemover, grammar::GNF < >, const grammar::GNF < > & > ( grammar::simplify::EpsilonRemover::remove, "grammar" ).setDocumentation (
"Removes epsilon rules from the given grammar.\n\
\n\
@param grammar the modified grammar\n\
@return grammar without epsilon rules" );

auto EpsilonRemoverLG = registration::AbstractRegister < grammar::simplify::EpsilonRemover, grammar::EpsilonFreeCFG < >, const grammar::LG < > & > ( grammar::simplify::EpsilonRemover::remove, "grammar" ).setDocumentation (
"Removes epsilon rules from the given grammar.\n\
\n\
@param grammar the modified grammar\n\
@return grammar without epsilon rules" );

auto EpsilonRemoverLeftLG = registration::AbstractRegister < grammar::simplify::EpsilonRemover, grammar::EpsilonFreeCFG < >, const grammar::LeftLG < > & > ( grammar::simplify::EpsilonRemover::remove, "grammar" ).setDocumentation (
"Removes epsilon rules from the given grammar.\n\
\n\
@param grammar the modified grammar\n\
@return grammar without epsilon rules" );

auto EpsilonRemoverLeftRG = registration::AbstractRegister < grammar::simplify::EpsilonRemover, grammar::LeftRG < >, const grammar::LeftRG < > & > ( grammar::simplify::EpsilonRemover::remove, "grammar" ).setDocumentation (
"Removes epsilon rules from the given grammar.\n\
\n\
@param grammar the modified grammar\n\
@return grammar without epsilon rules" );

auto EpsilonRemoverRightLG = registration::AbstractRegister < grammar::simplify::EpsilonRemover, grammar::EpsilonFreeCFG < >, const grammar::RightLG < > & > ( grammar::simplify::EpsilonRemover::remove, "grammar" ).setDocumentation (
"Removes epsilon rules from the given grammar.\n\
\n\
@param grammar the modified grammar\n\
@return grammar without epsilon rules" );

auto EpsilonRemoverRightRG = registration::AbstractRegister < grammar::simplify::EpsilonRemover, grammar::RightRG < >, const grammar::RightRG < > & > ( grammar::simplify::EpsilonRemover::remove, "grammar" ).setDocumentation (
"Removes epsilon rules from the given grammar.\n\
\n\
@param grammar the modified grammar\n\
@return grammar without epsilon rules" );

} /* namespace */
