/*
 * ToGNF.cpp
 *
 *  Created on: 24. 11. 2014
 *	  Author: Jan Travnicek
 */

#include "ToGNF.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ToGNFCFG = registration::AbstractRegister < grammar::simplify::ToGNF, grammar::GNF < DefaultSymbolType, ext::variant < DefaultSymbolType, DefaultSymbolType > >, const grammar::CFG < > & > ( grammar::simplify::ToGNF::convert, "grammar" ).setDocumentation (
"Implements transformation of a grammar into greibach normal form.\n\
\n\
@param grammar the transformed grammar\n\
@return an grammar in greibach normal form equivalent to the @p grammar" );

auto ToGNFEpsilonFreeCFG = registration::AbstractRegister < grammar::simplify::ToGNF, grammar::GNF < DefaultSymbolType, ext::variant < DefaultSymbolType, DefaultSymbolType > >, const grammar::EpsilonFreeCFG < > & > ( grammar::simplify::ToGNF::convert, "grammar" ).setDocumentation (
"Implements transformation of a grammar into greibach normal form.\n\
\n\
@param grammar the transformed grammar\n\
@return an grammar in greibach normal form equivalent to the @p grammar" );

auto ToGNFCNF = registration::AbstractRegister < grammar::simplify::ToGNF, grammar::GNF < DefaultSymbolType, ext::variant < DefaultSymbolType, DefaultSymbolType > >, const grammar::CNF < > & > ( grammar::simplify::ToGNF::convert, "grammar" ).setDocumentation (
"Implements transformation of a grammar into greibach normal form.\n\
\n\
@param grammar the transformed grammar\n\
@return an grammar in greibach normal form equivalent to the @p grammar" );

auto ToGNFGNF = registration::AbstractRegister < grammar::simplify::ToGNF, grammar::GNF < >, const grammar::GNF < > & > ( grammar::simplify::ToGNF::convert, "grammar" ).setDocumentation (
"Implements transformation of a grammar into greibach normal form.\n\
\n\
@param grammar the transformed grammar\n\
@return an grammar in greibach normal form equivalent to the @p grammar" );

auto ToGNFLG = registration::AbstractRegister < grammar::simplify::ToGNF, grammar::GNF < DefaultSymbolType, ext::variant < DefaultSymbolType, DefaultSymbolType > >, const grammar::LG < > & > ( grammar::simplify::ToGNF::convert, "grammar" ).setDocumentation (
"Implements transformation of a grammar into greibach normal form.\n\
\n\
@param grammar the transformed grammar\n\
@return an grammar in greibach normal form equivalent to the @p grammar" );

auto ToGNFLeftLG = registration::AbstractRegister < grammar::simplify::ToGNF, grammar::GNF < DefaultSymbolType, ext::variant < DefaultSymbolType, DefaultSymbolType > >, const grammar::LeftLG < > & > ( grammar::simplify::ToGNF::convert, "grammar" ).setDocumentation (
"Implements transformation of a grammar into greibach normal form.\n\
\n\
@param grammar the transformed grammar\n\
@return an grammar in greibach normal form equivalent to the @p grammar" );

auto ToGNFLeftRG = registration::AbstractRegister < grammar::simplify::ToGNF, grammar::RightRG < >, const grammar::LeftRG < > & > ( grammar::simplify::ToGNF::convert, "grammar" ).setDocumentation (
"Implements transformation of a grammar into greibach normal form.\n\
\n\
@param grammar the transformed grammar\n\
@return an grammar in greibach normal form equivalent to the @p grammar" );

auto ToGNFRightLG = registration::AbstractRegister < grammar::simplify::ToGNF, grammar::GNF < DefaultSymbolType, ext::variant < DefaultSymbolType, DefaultSymbolType > >, const grammar::RightLG < > & > ( grammar::simplify::ToGNF::convert, "grammar" ).setDocumentation (
"Implements transformation of a grammar into greibach normal form.\n\
\n\
@param grammar the transformed grammar\n\
@return an grammar in greibach normal form equivalent to the @p grammar" );

auto ToGNFRightRG = registration::AbstractRegister < grammar::simplify::ToGNF, grammar::RightRG < >, const grammar::RightRG < > & > ( grammar::simplify::ToGNF::convert, "grammar" ).setDocumentation (
"Implements transformation of a grammar into greibach normal form.\n\
\n\
@param grammar the transformed grammar\n\
@return an grammar in greibach normal form equivalent to the @p grammar" );

} /* namespace */
