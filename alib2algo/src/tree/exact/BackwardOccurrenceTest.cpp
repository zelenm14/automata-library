/*
 * BackwardOccurrenceTest.cpp
 *
 *  Created on: 31. 3. 2018
 *	  Author: Jan Travnicek
 */

#include "BackwardOccurrenceTest.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto BackwardOccurrenceTestPrefixRankedBarTree = registration::AbstractRegister < tree::exact::BackwardOccurrenceTest, ext::pair < int, int >, const tree::PrefixRankedBarTree < > &, const ext::vector < int > &, const tree::PrefixRankedBarTree < > &, size_t > ( tree::exact::BackwardOccurrenceTest::occurrence, abstraction::AlgorithmCategories::AlgorithmCategory::DEFAULT, "subject", "subjectSubtreeJumpTable", "pattern", "position" );

} /* namespace */
