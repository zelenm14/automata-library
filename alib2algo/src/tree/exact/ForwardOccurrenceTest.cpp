/*
 * ForwardOccurrenceTest.cpp
 *
 *  Created on: 31. 3. 2018
 *	  Author: Jan Travnicek
 */

#include "ForwardOccurrenceTest.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ForwardOccurrenceTestPrefixRankedTree = registration::AbstractRegister < tree::exact::ForwardOccurrenceTest, size_t, const tree::PrefixRankedTree < > &, const ext::vector < int > &, const tree::PrefixRankedTree < > &, size_t > ( tree::exact::ForwardOccurrenceTest::occurrence, abstraction::AlgorithmCategories::AlgorithmCategory::DEFAULT, "subject", "subjectSubtreeJumpTable", "pattern", "position" );

auto ForwardOccurrenceTestPrefixRankedBarTree = registration::AbstractRegister < tree::exact::ForwardOccurrenceTest, size_t, const tree::PrefixRankedBarTree < > &, const ext::vector < int > &, const tree::PrefixRankedBarTree < > &, size_t > ( tree::exact::ForwardOccurrenceTest::occurrence, abstraction::AlgorithmCategories::AlgorithmCategory::DEFAULT, "subject", "subjectSubtreeJumpTable", "pattern", "position" );

} /* namespace */
