/*
 * BorderArray.cpp
 *
 *  Created on: 4. 10. 2019
 *      Author: Tomas Pecka, Jan Travnicek
 */

#include "BorderArray.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto BorderArrayPrefixRankedBarNonlinearPattern = registration::AbstractRegister < tree::properties::BorderArray, ext::vector < size_t >, const tree::PrefixRankedBarNonlinearPattern < > & > ( tree::properties::BorderArray::construct );
auto BorderArrayPrefixRankedBarPattern          = registration::AbstractRegister < tree::properties::BorderArray, ext::vector < size_t >, const tree::PrefixRankedBarPattern < > & >          ( tree::properties::BorderArray::construct );
auto BorderArrayPrefixRankedNonlinearPattern    = registration::AbstractRegister < tree::properties::BorderArray, ext::vector < size_t >, const tree::PrefixRankedNonlinearPattern < > & >    ( tree::properties::BorderArray::construct );
auto BorderArrayPrefixRankedPattern             = registration::AbstractRegister < tree::properties::BorderArray, ext::vector < size_t >, const tree::PrefixRankedPattern < > & >             ( tree::properties::BorderArray::construct );

} /* namespace */
