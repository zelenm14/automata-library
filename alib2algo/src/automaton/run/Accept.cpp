/*
 * Accept.cpp
 *
 *  Created on: 9. 2. 2014
 *	  Author: Jan Travnicek
 */

#include "Accept.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto AcceptDFALinearString = registration::AbstractRegister < automaton::run::Accept, bool, const automaton::DFA < > &, const string::LinearString < > & > ( automaton::run::Accept::accept, "automaton", "string" ).setDocumentation (
"Automaton run implementation resulting in boolean true if the automaton accepted the input.\n\
\n\
@param automaton the runned automaton\n\
@param string the input of the automaton\n\
@return true if the automaton accepts given string" );

auto AcceptNFALinearString = registration::AbstractRegister < automaton::run::Accept, bool, const automaton::NFA < > &, const string::LinearString < > & > ( automaton::run::Accept::accept, "automaton", "string" ).setDocumentation (
"Automaton run implementation resulting in boolean true if the automaton accepted the input.\n\
\n\
@param automaton the runned automaton\n\
@param string the input of the automaton\n\
@return true if the automaton accepts given string" );

auto AcceptEpsilonNFALinearString = registration::AbstractRegister < automaton::run::Accept, bool, const automaton::EpsilonNFA < > &, const string::LinearString < > & > ( automaton::run::Accept::accept, "automaton", "string" ).setDocumentation (
"Automaton run implementation resulting in boolean true if the automaton accepted the input.\n\
\n\
@param automaton the runned automaton\n\
@param string the input of the automaton\n\
@return true if the automaton accepts given string" );

auto AcceptDFTARankedTree = registration::AbstractRegister < automaton::run::Accept, bool, const automaton::DFTA < > &, const tree::RankedTree < > & > ( automaton::run::Accept::accept, "automaton", "tree" ).setDocumentation (
"Automaton run implementation resulting in boolean true if the automaton accepted the input.\n\
\n\
@param automaton the runned automaton\n\
@param string the input of the automaton\n\
@return true if the automaton accepts given string" );

auto AcceptNFTARankedTree = registration::AbstractRegister < automaton::run::Accept, bool, const automaton::NFTA < > &, const tree::RankedTree < > & > ( automaton::run::Accept::accept, "automaton", "tree" ).setDocumentation (
"Automaton run implementation resulting in boolean true if the automaton accepted the input.\n\
\n\
@param automaton the runned automaton\n\
@param string the input of the automaton\n\
@return true if the automaton accepts given string" );

auto AcceptInputDrivenDPDALinearString = registration::AbstractRegister < automaton::run::Accept, bool, const automaton::InputDrivenDPDA < > &, const string::LinearString < > & > ( automaton::run::Accept::accept, "automaton", "string" ).setDocumentation (
"Automaton run implementation resulting in boolean true if the automaton accepted the input.\n\
\n\
@param automaton the runned automaton\n\
@param string the input of the automaton\n\
@return true if the automaton accepts given string" );

auto AcceptVisiblyPushdownDPDALinearString = registration::AbstractRegister < automaton::run::Accept, bool, const automaton::VisiblyPushdownDPDA < > &, const string::LinearString < > & > ( automaton::run::Accept::accept, "automaton", "string" ).setDocumentation (
"Automaton run implementation resulting in boolean true if the automaton accepted the input.\n\
\n\
@param automaton the runned automaton\n\
@param string the input of the automaton\n\
@return true if the automaton accepts given string" );

auto AcceptRealTimeHeightDeterministicDPDALinearString = registration::AbstractRegister < automaton::run::Accept, bool, const automaton::RealTimeHeightDeterministicDPDA < > &, const string::LinearString < > & > ( automaton::run::Accept::accept, "automaton", "string" ).setDocumentation (
"Automaton run implementation resulting in boolean true if the automaton accepted the input.\n\
\n\
@param automaton the runned automaton\n\
@param string the input of the automaton\n\
@return true if the automaton accepts given string" );

auto AcceptDPDALinearString = registration::AbstractRegister < automaton::run::Accept, bool, const automaton::DPDA < > &, const string::LinearString < > & > ( automaton::run::Accept::accept, "automaton", "string" ).setDocumentation (
"Automaton run implementation resulting in boolean true if the automaton accepted the input.\n\
\n\
@param automaton the runned automaton\n\
@param string the input of the automaton\n\
@return true if the automaton accepts given string" );

auto AcceptNPDTALinearString = registration::AbstractRegister < automaton::run::Accept, bool, const automaton::NPDTA < > &, const string::LinearString < > & > ( automaton::run::Accept::accept, "automaton", "string" ).setDocumentation (
"Automaton run implementation resulting in boolean true if the automaton accepted the input.\n\
\n\
@param automaton the runned automaton\n\
@param string the input of the automaton\n\
@return true if the automaton accepts given string" );

} /* namespace */
