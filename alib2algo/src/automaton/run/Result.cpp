/*
 * Result.cpp
 *
 *  Created on: 9. 2. 2014
 *	  Author: Jan Travnicek
 */

#include "Result.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ResultDFALinearString = registration::AbstractRegister < automaton::run::Result, DefaultStateType, const automaton::DFA < > &, const string::LinearString < > & > ( automaton::run::Result::result, "automaton", "string" ).setDocumentation (
"General automaton run implementation resulting in the reached state. Generic fail state is returned if the automaton's transition function was not defined for the input.\n\
\n\
@param automaton the runned automaton\n\
@param string the input of the automaton\n\
@return state where the run stopped" );

auto ResultDFTARankedTree = registration::AbstractRegister < automaton::run::Result, DefaultStateType, const automaton::DFTA < > &, const tree::RankedTree < > & > ( automaton::run::Result::result, "automaton", "tree" ).setDocumentation (
"General automaton run implementation resulting in the reached state. Generic fail state is returned if the automaton's transition function was not defined for the input.\n\
\n\
@param automaton the runned automaton\n\
@param string the input of the automaton\n\
@return state where the run stopped" );

auto ResultInputDrivenDPDALinearString = registration::AbstractRegister < automaton::run::Result, DefaultStateType, const automaton::InputDrivenDPDA < > &, const string::LinearString < > & > ( automaton::run::Result::result, "automaton", "string" ).setDocumentation (
"General automaton run implementation resulting in the reached state. Generic fail state is returned if the automaton's transition function was not defined for the input.\n\
\n\
@param automaton the runned automaton\n\
@param string the input of the automaton\n\
@return state where the run stopped" );

auto ResultVisiblyPushdownDPDALinearString = registration::AbstractRegister < automaton::run::Result, DefaultStateType, const automaton::VisiblyPushdownDPDA < > &, const string::LinearString < > & > ( automaton::run::Result::result, "automaton", "string" ).setDocumentation (
"General automaton run implementation resulting in the reached state. Generic fail state is returned if the automaton's transition function was not defined for the input.\n\
\n\
@param automaton the runned automaton\n\
@param string the input of the automaton\n\
@return state where the run stopped" );

auto ResultRealTimeHeightDeterministicDPDALinearString = registration::AbstractRegister < automaton::run::Result, DefaultStateType, const automaton::RealTimeHeightDeterministicDPDA < > &, const string::LinearString < > & > ( automaton::run::Result::result, "automaton", "string" ).setDocumentation (
"General automaton run implementation resulting in the reached state. Generic fail state is returned if the automaton's transition function was not defined for the input.\n\
\n\
@param automaton the runned automaton\n\
@param string the input of the automaton\n\
@return state where the run stopped" );

auto ResultDPDALinearString = registration::AbstractRegister < automaton::run::Result, DefaultStateType, const automaton::DPDA < > &, const string::LinearString < > & > ( automaton::run::Result::result, "automaton", "string" ).setDocumentation (
"General automaton run implementation resulting in the reached state. Generic fail state is returned if the automaton's transition function was not defined for the input.\n\
\n\
@param automaton the runned automaton\n\
@param string the input of the automaton\n\
@return state where the run stopped" );

auto ResultDFALinearString2 = registration::AbstractRegister < automaton::run::Result, DefaultStateType, const automaton::DFA < > &, const string::LinearString < > &, const DefaultStateType & > ( automaton::run::Result::result, "automaton", "string", "fail" ).setDocumentation (
"General automaton run implementation resulting in the reached state. Generic fail state is returned if the automaton's transition function was not defined for the input.\n\
\n\
@param automaton the runned automaton\n\
@param string the input of the automaton\n\
@param fail the state to use when transition function is not defined for the input\n\
@return state where the run stopped" );

auto ResultDFTARankedTree2 = registration::AbstractRegister < automaton::run::Result, DefaultStateType, const automaton::DFTA < > &, const tree::RankedTree < > &, const DefaultStateType & > ( automaton::run::Result::result, "automaton", "tree", "fail" ).setDocumentation (
"General automaton run implementation resulting in the reached state. Generic fail state is returned if the automaton's transition function was not defined for the input.\n\
\n\
@param automaton the runned automaton\n\
@param string the input of the automaton\n\
@param fail the state to use when transition function is not defined for the input\n\
@return state where the run stopped" );

auto ResultInputDrivenDPDALinearString2 = registration::AbstractRegister < automaton::run::Result, DefaultStateType, const automaton::InputDrivenDPDA < > &, const string::LinearString < > &, const DefaultStateType & > ( automaton::run::Result::result, "automaton", "string", "fail" ).setDocumentation (
"General automaton run implementation resulting in the reached state. Generic fail state is returned if the automaton's transition function was not defined for the input.\n\
\n\
@param automaton the runned automaton\n\
@param string the input of the automaton\n\
@param fail the state to use when transition function is not defined for the input\n\
@return state where the run stopped" );

auto ResultVisiblyPushdownDPDALinearString2 = registration::AbstractRegister < automaton::run::Result, DefaultStateType, const automaton::VisiblyPushdownDPDA < > &, const string::LinearString < > &, const DefaultStateType & > ( automaton::run::Result::result, "automaton", "string", "fail" ).setDocumentation (
"General automaton run implementation resulting in the reached state. Generic fail state is returned if the automaton's transition function was not defined for the input.\n\
\n\
@param automaton the runned automaton\n\
@param string the input of the automaton\n\
@param fail the state to use when transition function is not defined for the input\n\
@return state where the run stopped" );

auto ResultRealTimeHeightDeterministicDPDALinearString2 = registration::AbstractRegister < automaton::run::Result, DefaultStateType, const automaton::RealTimeHeightDeterministicDPDA < > &, const string::LinearString < > &, const DefaultStateType & > ( automaton::run::Result::result, "automaton", "string", "fail" ).setDocumentation (
"General automaton run implementation resulting in the reached state. Generic fail state is returned if the automaton's transition function was not defined for the input.\n\
\n\
@param automaton the runned automaton\n\
@param string the input of the automaton\n\
@param fail the state to use when transition function is not defined for the input\n\
@return state where the run stopped" );

auto ResultDPDALinearString2 = registration::AbstractRegister < automaton::run::Result, DefaultStateType, const automaton::DPDA < > &, const string::LinearString < > &, const DefaultStateType & > ( automaton::run::Result::result, "automaton", "string", "fail" ).setDocumentation (
"General automaton run implementation resulting in the reached state. Generic fail state is returned if the automaton's transition function was not defined for the input.\n\
\n\
@param automaton the runned automaton\n\
@param string the input of the automaton\n\
@param fail the state to use when transition function is not defined for the input\n\
@return state where the run stopped" );

} /* namespace */
