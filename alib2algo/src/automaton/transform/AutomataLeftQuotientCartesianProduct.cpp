/*
 * AutomataLeftQuotientCartesianProduct.cpp
 *
 *  Created on: 20. 11. 2014
 *	  Author: Tomas Pecka
 */

#include "AutomataLeftQuotientCartesianProduct.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto AutomataLeftQuotientCartesianProductNFA2 = registration::AbstractRegister < automaton::transform::AutomataLeftQuotientCartesianProduct, automaton::MultiInitialStateNFA < DefaultSymbolType, ext::pair < DefaultStateType, DefaultStateType > >, const automaton::NFA < > &, const automaton::NFA < > &, bool > ( automaton::transform::AutomataLeftQuotientCartesianProduct::quotient, "first", "second", "full" ).setDocumentation (
"Divides (computes quotient of) two languages represented by two finite automata.\n\
For finite automata A1, A2, we create a finite automaton A such that L(A) = { x : wx in L(A2) and w in L(A1) }.\n\
Full quotient requires that w is maximal and x does not have prefix from L(A1). Non-full quotient does not require that to be true and only some prefix is removed from words in L(A2).\n\
\n\
@param first First automaton (A1) representing language of the removed prefix\n\
@param second Second automaton (A2) representing language from which the prefix is removed\n\
@param full true if the resulting quotient should be full, false if not.\n\
@return nondeterministic FA with multiple initial states representing the division (computed quotient) of two languages" );

} /* namespace */
