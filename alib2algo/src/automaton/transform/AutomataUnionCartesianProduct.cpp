/*
 * AutomataUnionCartesianProduct.cpp
 *
 *  Created on: 20. 11. 2014
 *	  Author: Tomas Pecka
 */

#include "AutomataUnionCartesianProduct.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto AutomataUnionCartesianProductDFA = registration::AbstractRegister < automaton::transform::AutomataUnionCartesianProduct, automaton::DFA < DefaultSymbolType, ext::pair < DefaultStateType, DefaultStateType > >, const automaton::DFA < > &, const automaton::DFA < > & > ( automaton::transform::AutomataUnionCartesianProduct::unification, "first", "second" ).setDocumentation (
"Union of two automata.\n\
\n\
@param first First automaton (A1)\n\
@param second Second automaton (A2)\n\
@return (non)deterministic FA representing the union of two automata" );

auto AutomataUnionCartesianProductNFA = registration::AbstractRegister < automaton::transform::AutomataUnionCartesianProduct, automaton::NFA < DefaultSymbolType, ext::pair < DefaultStateType, DefaultStateType > >, const automaton::NFA < > &, const automaton::NFA < > & > ( automaton::transform::AutomataUnionCartesianProduct::unification, "first", "second" ).setDocumentation (
"Union of two automata.\n\
\n\
@param first First automaton (A1)\n\
@param second Second automaton (A2)\n\
@return (non)deterministic FA representing the union of two automata" );

} /* namespace */
