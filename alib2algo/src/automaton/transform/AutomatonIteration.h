/*
 * AutomatonIteration.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: 29. 11. 2014
 *	  Author: Tomas Pecka
 */

#ifndef AUTOMATON_ITERATION_H_
#define AUTOMATON_ITERATION_H_

#include <automaton/FSM/EpsilonNFA.h>

namespace automaton {

namespace transform {

/**
 * Iteration of a finite automaton.
 * For finite automaton A1, we create automaton A such that L(A) = L(A1)*
 * This method utilizes epsilon transitions in finite automata (Melichar: Jazyky a překlady, 2.82).
 */
class AutomatonIteration {
public:
	/**
	 * Iteration of a finite automaton.
	 * @tparam SymbolType Type for input symbols.
	 * @tparam StateType Type for states.
	 * @param automaton automaton to iterate
	 * @return nondeterministic FA representing the intersection of @p automaton
	 */
	template < class SymbolType, class StateType >
	static automaton::NFA < SymbolType, StateType > iteration(const automaton::DFA < SymbolType, StateType > & automaton);

	/**
	 * @overload
	 */
	template < class SymbolType, class StateType >
	static automaton::NFA < SymbolType, StateType > iteration(const automaton::NFA < SymbolType, StateType > & automaton);
};

template < class SymbolType, class StateType >
automaton::NFA < SymbolType, StateType > AutomatonIteration::iteration(const automaton::DFA < SymbolType, StateType > & automaton) {
	automaton::NFA < SymbolType, StateType > res(automaton);

	for(const auto& qf : res.getFinalStates())
		for(const auto& t : res.getTransitionsToState(qf))
			res.addTransition(t.first.first, t.first.second, res.getInitialState());

	StateType newInitialState = common::createUnique(automaton.getInitialState(), automaton.getStates());
	res.addState(newInitialState);
	res.setInitialState(newInitialState);
	res.addFinalState(newInitialState);

	for(const auto& t : automaton.getTransitionsFromState(automaton.getInitialState()))
		res.addTransition(newInitialState, t.first.second, t.second);

	return res;
}

template < class SymbolType, class StateType >
automaton::NFA < SymbolType, StateType > AutomatonIteration::iteration(const automaton::NFA < SymbolType, StateType > & automaton) {
	automaton::NFA < SymbolType, StateType > res(automaton);

	for(const auto& qf : res.getFinalStates())
		for(const auto& t : res.getTransitionsToState(qf))
			res.addTransition(t.first.first, t.first.second, res.getInitialState());

	StateType newInitialState = common::createUnique(automaton.getInitialState(), automaton.getStates());
	res.addState(newInitialState);
	res.setInitialState(newInitialState);
	res.addFinalState(newInitialState);

	for(const auto& t : automaton.getTransitionsFromState(automaton.getInitialState()))
		res.addTransition(newInitialState, t.first.second, t.second);

	return res;
}

} /* namespace transform */

} /* namespace automaton */

#endif /* AUTOMATON_ITERATION_H_ */
