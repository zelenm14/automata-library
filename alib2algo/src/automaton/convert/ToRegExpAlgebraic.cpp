/*
 * ToRegExpAlgebraic.cpp
 *
 *  Created on: 11. 2. 2014
 *	  Author: Tomas Pecka
 */

#include "ToRegExpAlgebraic.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ToRegExpAlgebraicEpsilonNFA = registration::AbstractRegister < automaton::convert::ToRegExpAlgebraic, regexp::UnboundedRegExp < >, const automaton::EpsilonNFA < > & > ( automaton::convert::ToRegExpAlgebraic::convert, "automaton" ).setDocumentation (
"Performs the actual conversion.\n\
\n\
@param automaton The automaton that is to be converted to the regular expression\n\
@return regular expression equivalent to the input @p automaton" );

auto ToRegExpAlgebraicMultiInitialStateNFA = registration::AbstractRegister < automaton::convert::ToRegExpAlgebraic, regexp::UnboundedRegExp < >, const automaton::MultiInitialStateNFA < > & > ( automaton::convert::ToRegExpAlgebraic::convert, "automaton" ).setDocumentation (
"Performs the actual conversion.\n\
\n\
@param automaton The automaton that is to be converted to the regular expression\n\
@return regular expression equivalent to the input @p automaton" );

auto ToRegExpAlgebraicNFA = registration::AbstractRegister < automaton::convert::ToRegExpAlgebraic, regexp::UnboundedRegExp < >, const automaton::NFA < > & > ( automaton::convert::ToRegExpAlgebraic::convert, "automaton" ).setDocumentation (
"Performs the actual conversion.\n\
\n\
@param automaton The automaton that is to be converted to the regular expression\n\
@return regular expression equivalent to the input @p automaton" );

auto ToRegExpAlgebraicDFA = registration::AbstractRegister < automaton::convert::ToRegExpAlgebraic, regexp::UnboundedRegExp < >, const automaton::DFA < > & > ( automaton::convert::ToRegExpAlgebraic::convert, "automaton" ).setDocumentation (
"Performs the actual conversion.\n\
\n\
@param automaton The automaton that is to be converted to the regular expression\n\
@return regular expression equivalent to the input @p automaton" );

} /* namespace */
