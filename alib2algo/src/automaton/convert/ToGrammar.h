/*
 * ToGrammar.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: 9. 2. 2014
 *      Author: Jan Travnicek
 */

#ifndef _AUTOMATON_TO_GRAMMAR_H__
#define _AUTOMATON_TO_GRAMMAR_H__

#include <grammar/Regular/RightRG.h>
#include <automaton/FSM/NFA.h>
#include <automaton/FSM/DFA.h>
#include "ToGrammarRightRG.h"

namespace automaton {

namespace convert {

/**
 * Conversion of finite automata to regular grammars.
 * This class serves as a "default wrapper" over the conversion of FA to RG. It delegates to the right regular grammar conversion.
 * @sa ToGrammarRightRG
 */
class ToGrammar {
public:
	/**
	 * Performs the conversion (@sa ToGrammarRightRG::convert).
	 *
	 * \tparam T the converted automaton
	 *
	 * \param automaton the automaton to convert
	 *
	 * \return right regular grammar equivalent to the input @p automaton
	 */
	template < class T >
	static ext::require < isDFA < T > || isNFA < T >, grammar::RightRG < typename T::SymbolType, typename T::StateType > > convert ( const T & automaton );

};

template < class T >
ext::require < isDFA < T > || isNFA < T >, grammar::RightRG < typename T::SymbolType, typename T::StateType > > ToGrammar::convert ( const T & automaton ) {
	return ToGrammarRightRG::convert ( automaton );
}


} /* namespace convert */

} /* namespace automaton */

#endif /* _AUTOMATON_TO_GRAMMAR_H__ */
