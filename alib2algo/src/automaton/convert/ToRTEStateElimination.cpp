/*
 * ToRTEStateElimination.cpp
 *
 *  Created on: 13. 3. 2019
 *	  Author: Tomas Pecka
 */

#include "ToRTEStateElimination.h"
#include <registration/AlgoRegistration.hpp>

namespace automaton {

namespace convert {

auto ToRTEStateEliminationDFTA = registration::AbstractRegister< ToRTEStateElimination, rte::FormalRTE< ext::variant< DefaultSymbolType, DefaultStateType > >, const automaton::DFTA<>& > ( ToRTEStateElimination::convert, "automaton" );
auto ToRTEStateEliminationNFTA = registration::AbstractRegister< ToRTEStateElimination, rte::FormalRTE< ext::variant< DefaultSymbolType, DefaultStateType > >, const automaton::NFTA<>& > ( ToRTEStateElimination::convert, "automaton" );

} /* namespace convert */

} /* namespace automaton */
