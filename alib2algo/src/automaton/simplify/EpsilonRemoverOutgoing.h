/*
 * EpsilonRemoverOutgoing.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: 16. 1. 2014
 *	  Author: Tomas Pecka
 */

#ifndef EPSILON_REMOVER_OUTGOING_H_
#define EPSILON_REMOVER_OUTGOING_H_

#include <automaton/FSM/EpsilonNFA.h>
#include <automaton/FSM/MultiInitialStateNFA.h>
#include <automaton/FSM/NFA.h>
#include <automaton/FSM/DFA.h>

#include <automaton/properties/EpsilonClosure.h>

#include <global/GlobalData.h>

namespace automaton {

namespace simplify {

/**
 * Removes epsilon transitions from an automaton.
 * This method returns multi-initial state automata (it is not the one teached at BI-AAG course).
 *
 * @sa automaton::simplify::EpsilonRemoverIncoming
 */
class EpsilonRemoverOutgoing {
public:
	/**
	 * Removes epsilon transitions from an automaton.
	 *
	 * @tparam SymbolType Type for input symbols.
	 * @tparam StateType Type for states.
	 * @param fsm automaton to remove epsilon transitions from
	 * @return an automaton (with multiple initial states) equivalent to @p fsm but without epsilon transitions
	 */
	template < class StateType, class SymbolType >
	static automaton::MultiInitialStateNFA < SymbolType, StateType > remove( const automaton::EpsilonNFA < SymbolType, StateType > & fsm );

	/**
	 * @overload
	 */
	template < class StateType, class SymbolType >
	static automaton::MultiInitialStateNFA < SymbolType, StateType > remove( const automaton::MultiInitialStateNFA < SymbolType, StateType > & fsm );

	/**
	 * @overload
	 */
	template < class StateType, class SymbolType >
	static automaton::NFA < SymbolType, StateType > remove( const automaton::NFA < SymbolType, StateType > & fsm );

	/**
	 * @overload
	 */
	template < class StateType, class SymbolType >
	static automaton::DFA < SymbolType, StateType > remove( const automaton::DFA < SymbolType, StateType > & fsm );
};

template < class StateType, class SymbolType >
automaton::DFA < SymbolType, StateType > EpsilonRemoverOutgoing::remove(const automaton::DFA < SymbolType, StateType > & fsm) {
	return fsm;
}

template < class StateType, class SymbolType >
automaton::MultiInitialStateNFA < SymbolType, StateType > EpsilonRemoverOutgoing::remove(const automaton::MultiInitialStateNFA < SymbolType, StateType > & fsm) {
	return fsm;
}

template < class StateType, class SymbolType >
automaton::NFA < SymbolType, StateType > EpsilonRemoverOutgoing::remove(const automaton::NFA < SymbolType, StateType > & fsm) {
	return fsm;
}

template < class StateType, class SymbolType >
automaton::MultiInitialStateNFA < SymbolType, StateType > EpsilonRemoverOutgoing::remove( const automaton::EpsilonNFA < SymbolType, StateType > & fsm ) {
	automaton::MultiInitialStateNFA < SymbolType, StateType > res;
	res.setStates( fsm.getStates() );
	res.setFinalStates( fsm.getFinalStates() );
	res.setInputAlphabet( fsm.getInputAlphabet() );

	/**
	 * Step 1 from Melichar 2.41
	 */
	for( const auto & middle : fsm.getStates( ) ) {
		const ext::set<StateType> middleClosure = automaton::properties::EpsilonClosure::epsilonClosure( fsm, middle );

		if( common::GlobalData::verbose ) {
			common::Streams::err << "E-clos(" << middle << ") -> " << middleClosure << std::endl;
		}

		for( const auto & symbol : fsm.getInputAlphabet() ) {
			for( const auto& transition : fsm.getTransitions( ) ) {
				if(transition.second != middle ) continue;
				if(transition.first.second != symbol) continue;

				for( const auto & to : middleClosure )
					res.addTransition(transition.first.first, symbol, to );
			}
		}
	}

	/**
	 * Step 2 from Melichar 2.41
	 */
	const ext::set<StateType> cl = automaton::properties::EpsilonClosure::epsilonClosure( fsm, fsm.getInitialState() );
	res.setInitialStates( cl );

	return res;
}

} /* namespace simplify */

} /* namespace automaton */

#endif /* EPSILON_REMOVER_OUTGOING_H_ */
