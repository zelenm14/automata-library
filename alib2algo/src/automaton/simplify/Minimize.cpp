/*
 * Minimize.cpp
 *
 *  Created on: Dec 9, 2013
 *	  Author: honza
 */

#include "Minimize.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto MinimizeNFA = registration::AbstractRegister < automaton::simplify::Minimize, automaton::DFA < >, const automaton::DFA < > & > ( automaton::simplify::Minimize::minimize, "dfa" ).setDocumentation (
"Minimizes deterministic finite autmaton.\n\
\n\
@param dfa deterministic finite automaton to minimize\n\
@return Minimal deterministic finite automaton equivalent to @p dfa");

auto MinimizeNFTA = registration::AbstractRegister < automaton::simplify::Minimize, automaton::DFTA < >, const automaton::DFTA < > & > ( automaton::simplify::Minimize::minimize, "dfta" ).setDocumentation (
"Minimizes deterministic finite tree autmaton.\n\
\n\
@param dfta deterministic finite tree automaton to minimize\n\
@return Minimal deterministic finite automaton equivalent to @p dfa" );

} /* namespace */
