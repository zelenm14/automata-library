/*
 * EpsilonRemoverOutgoing.cpp
 *
 *  Created on: 16. 1. 2014
 *	  Author: Tomas Pecka
 */

#include "EpsilonRemoverOutgoing.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto EpsilonRemoverOutgoingDFA = registration::AbstractRegister < automaton::simplify::EpsilonRemoverOutgoing, automaton::DFA < >, const automaton::DFA < > & > ( automaton::simplify::EpsilonRemoverOutgoing::remove, "fsm" ).setDocumentation (
"Removes epsilon transitions from an automaton.\n\
\n\
@param fsm automaton to remove epsilon transitions from\n\
@return an automaton (with multiple initial states) equivalent to @p fsm but without epsilon transitions" );

auto EpsilonRemoverOutgoingMultiInitialStateNFA = registration::AbstractRegister < automaton::simplify::EpsilonRemoverOutgoing, automaton::MultiInitialStateNFA < > , const automaton::MultiInitialStateNFA < > & > ( automaton::simplify::EpsilonRemoverOutgoing::remove, "fsm" ).setDocumentation (
"Removes epsilon transitions from an automaton.\n\
\n\
@param fsm automaton to remove epsilon transitions from\n\
@return an automaton (with multiple initial states) equivalent to @p fsm but without epsilon transitions" );

auto EpsilonRemoverOutgoingNFA = registration::AbstractRegister < automaton::simplify::EpsilonRemoverOutgoing, automaton::NFA < >, const automaton::NFA < > & > ( automaton::simplify::EpsilonRemoverOutgoing::remove, "fsm" ).setDocumentation (
"Removes epsilon transitions from an automaton.\n\
\n\
@param fsm automaton to remove epsilon transitions from\n\
@return an automaton (with multiple initial states) equivalent to @p fsm but without epsilon transitions" );

auto EpsilonRemoverOutgoingEpsilonNFA = registration::AbstractRegister < automaton::simplify::EpsilonRemoverOutgoing, automaton::MultiInitialStateNFA < >, const automaton::EpsilonNFA < > & > ( automaton::simplify::EpsilonRemoverOutgoing::remove, "fsm" ).setDocumentation (
"Removes epsilon transitions from an automaton.\n\
\n\
@param fsm automaton to remove epsilon transitions from\n\
@return an automaton (with multiple initial states) equivalent to @p fsm but without epsilon transitions" );

} /* namespace */
