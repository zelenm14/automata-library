/*
 * EpsilonClosure.cpp
 *
 *  Created on: 23. 3. 2014
 *	  Author: Tomas Pecka
 */

#include "EpsilonClosure.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto EpsilonClosureEpsilonNFA = registration::AbstractRegister < automaton::properties::EpsilonClosure, ext::set < DefaultStateType >, const automaton::EpsilonNFA < > &, const DefaultStateType & > ( automaton::properties::EpsilonClosure::epsilonClosure, "fsm", "state" ).setDocumentation (
"Computes epsilon closure for a state of a nondeterministic finite automaton with epsilon transitions.\n\
Implemented using breadth-first search.\n\
\n\
@param fsm nondeterministic finite automaton with epsilon transitions\n\
@param state state for which we want to compute the closure\n\
@return set of states representing the epsilon closures of a @p state of @p fsm\n\
@throws exception::CommonException if state is not in the set of @p fsm states" );

auto EpsilonClosureMultiInitialStateNFA = registration::AbstractRegister < automaton::properties::EpsilonClosure, ext::set < DefaultStateType >, const automaton::MultiInitialStateNFA < > &, const DefaultStateType & > ( automaton::properties::EpsilonClosure::epsilonClosure, "fsm", "state" ).setDocumentation (
"Computes epsilon closure for a state of a nondeterministic finite automaton with epsilon transitions.\n\
Implemented using breadth-first search.\n\
\n\
@param fsm nondeterministic finite automaton with epsilon transitions\n\
@param state state for which we want to compute the closure\n\
@return set of states representing the epsilon closures of a @p state of @p fsm\n\
@throws exception::CommonException if state is not in the set of @p fsm states" );

auto EpsilonClosureNFA = registration::AbstractRegister < automaton::properties::EpsilonClosure, ext::set < DefaultStateType >, const automaton::NFA < > &, const DefaultStateType & > ( automaton::properties::EpsilonClosure::epsilonClosure, "fsm", "state" ).setDocumentation (
"Computes epsilon closure for a state of a nondeterministic finite automaton with epsilon transitions.\n\
Implemented using breadth-first search.\n\
\n\
@param fsm nondeterministic finite automaton with epsilon transitions\n\
@param state state for which we want to compute the closure\n\
@return set of states representing the epsilon closures of a @p state of @p fsm\n\
@throws exception::CommonException if state is not in the set of @p fsm states" );

auto EpsilonClosureDFA = registration::AbstractRegister < automaton::properties::EpsilonClosure, ext::set < DefaultStateType >, const automaton::DFA < > &, const DefaultStateType & > ( automaton::properties::EpsilonClosure::epsilonClosure, "fsm", "state" ).setDocumentation (
"Computes epsilon closure for a state of a nondeterministic finite automaton with epsilon transitions.\n\
Implemented using breadth-first search.\n\
\n\
@param fsm nondeterministic finite automaton with epsilon transitions\n\
@param state state for which we want to compute the closure\n\
@return set of states representing the epsilon closures of a @p state of @p fsm\n\
@throws exception::CommonException if state is not in the set of @p fsm states" );

auto EpsilonClosureExtendedNFA = registration::AbstractRegister < automaton::properties::EpsilonClosure, ext::set < DefaultStateType >, const automaton::ExtendedNFA < > &, const DefaultStateType & > ( automaton::properties::EpsilonClosure::epsilonClosure, "fsm", "state" ).setDocumentation (
"Computes epsilon closure for a state of a nondeterministic finite automaton with epsilon transitions.\n\
Implemented using breadth-first search.\n\
\n\
@param fsm nondeterministic finite automaton with epsilon transitions\n\
@param state state for which we want to compute the closure\n\
@return set of states representing the epsilon closures of a @p state of @p fsm\n\
@throws exception::CommonException if state is not in the set of @p fsm states" );

auto EpsilonClosureCompactNFA = registration::AbstractRegister < automaton::properties::EpsilonClosure, ext::set < DefaultStateType >, const automaton::CompactNFA < > &, const DefaultStateType & > ( automaton::properties::EpsilonClosure::epsilonClosure, "fsm", "state" ).setDocumentation (
"Computes epsilon closure for a state of a nondeterministic finite automaton with epsilon transitions.\n\
Implemented using breadth-first search.\n\
\n\
@param fsm nondeterministic finite automaton with epsilon transitions\n\
@param state state for which we want to compute the closure\n\
@return set of states representing the epsilon closures of a @p state of @p fsm\n\
@throws exception::CommonException if state is not in the set of @p fsm states" );

} /* namespace */
