/*
 * ReachableStates.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be reachable,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: 23. 3. 2014
 *	  Author: Tomas Pecka
 */

#ifndef REACHABLE_STATES_H_
#define REACHABLE_STATES_H_

#include <alib/algorithm>
#include <alib/deque>
#include <alib/set>
#include <alib/map>

#include <automaton/FSM/ExtendedNFA.h>
#include <automaton/FSM/CompactNFA.h>
#include <automaton/FSM/EpsilonNFA.h>
#include <automaton/FSM/NFA.h>
#include <automaton/FSM/DFA.h>
#include <automaton/TA/DFTA.h>
#include <automaton/TA/NFTA.h>

namespace automaton {

namespace properties {

/**
 * Find all reachable states of an automaton
 * A state is reachable if there exists any sequence of transitions from the initial state to it.
 */
class ReachableStates {
public:
	/**
	 * Finds all reachable states of a finite automaton.
	 * Using closure implementation of the BFS algorithm.
	 *
	 * @tparam T type of finite automaton
	 * @param fsm automaton
	 * @return set of reachable states of @p fsm
	 */
	template < class T >
	static ext::require < isDFA < T > || isNFA < T > || isEpsilonNFA < T > || isCompactNFA < T > || isExtendedNFA < T >, ext::set < typename T::StateType > > reachableStates( const T & fsm );

	/**
	 * Finds all reachable states of a finite automaton with multiple initial states.
	 * Using closure implementation of the BFS algorithm.
	 *
	 * @tparam T type of finite automaton with multiple initial states
	 * @param fsm automaton
	 * @return set of reachable states of @p fsm
	 */
	template < class T >
	static ext::require < isMultiInitialStateNFA < T > || isMultiInitialStateEpsilonNFA < T >, ext::set < typename T::StateType > > reachableStates ( const T & fsm );

	/**
	 * Finds all reachable states of a finite tree automaton.
	 * Using closure implementation of the BFS algorithm.
	 *
	 * @tparam T type of finite tree automaton
	 * @param fta automaton
	 * @return set of reachable states of @p fta
	 */
	template < class T >
	static ext::require < isDFTA < T > || isNFTA < T >, ext::set < typename T::StateType > > reachableStates ( const T & fsm );
};

template < class T >
ext::require < isDFA < T > || isNFA < T > || isEpsilonNFA < T > || isCompactNFA < T > || isExtendedNFA < T >, ext::set < typename T::StateType > > ReachableStates::reachableStates( const T & fsm ) {
	using StateType = typename T::StateType;

	// 1a
	ext::deque<ext::set<StateType>> Qi;
	Qi.push_back( ext::set<StateType>( ) );
	Qi.at( 0 ).insert ( fsm.getInitialState( ) );

	int i = 0;

	// 1bc
	do {
		i = i + 1;

		Qi.push_back( Qi.at( i - 1 ) );

		for( const auto & p : Qi.at( i - 1 ) )
			for( const auto & transition : fsm.getTransitionsFromState( p ) )
				Qi.at( i ).insert( transition.second );

	} while ( Qi.at( i ) != Qi.at( i - 1 ) );

	return Qi.at( i );
}

template < class T >
ext::require < isMultiInitialStateNFA < T > || isMultiInitialStateEpsilonNFA < T >, ext::set < typename T::StateType > > ReachableStates::reachableStates( const T & fsm ) {
	using StateType = typename T::StateType;

	// 1a
	ext::deque<ext::set<StateType>> Qi;
	Qi.push_back( ext::set<StateType>( ) );
	Qi.at( 0 ) = fsm.getInitialStates( );

	int i = 0;

	// 1bc
	do {
		i = i + 1;

		Qi.push_back( Qi.at( i - 1 ) );

		for( const auto & p : Qi.at( i - 1 ) )
			for( const auto & transition : fsm.getTransitionsFromState( p ) )
				Qi.at( i ).insert ( transition.second );
	} while ( Qi.at( i ) != Qi.at( i - 1 ) );

	return Qi.at( i );
}

template < class T >
ext::require < isDFTA < T > || isNFTA < T >, ext::set < typename T::StateType > > ReachableStates::reachableStates( const T & fta ) {
	using StateType = typename T::StateType;

	// 1a
	ext::deque<ext::set<StateType>> Qi;
	Qi.push_back( ext::set<StateType>( ) );

	int i = 0;

	// 1bc
	do {
		i = i + 1;

		Qi.push_back( Qi.at( i - 1) );

		for( const auto & transition : fta.getTransitions ( ) )
			if ( std::all_of ( transition.first.second.begin ( ), transition.first.second.end ( ), [ & ] ( const StateType & state ) { return Qi.at ( i - 1 ).count ( state ); } ) )
				Qi.at( i ).insert ( transition.second );

	} while ( Qi.at( i ) != Qi.at( i - 1 ) );

	return Qi.at( i );
}

} /* namespace properties */

} /* namespace automaton */

#endif /* REACHABLE_STATES_H_ */
