/*
* DistinguishableStates.cpp
 *
 *  Created on: 27. 3. 2019
 *	  Author: Tomas Pecka
 */

#include "DistinguishableStates.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto DistinguishableStatesDFA = registration::AbstractRegister < automaton::properties::DistinguishableStates, ext::set < ext::pair < DefaultStateType, DefaultStateType > >, const automaton::DFA < > & > ( automaton::properties::DistinguishableStates::distinguishable, "fsm" ).setDocumentation (
"Computes the pairs of distinguishable states in given DFA.\n\
\n\
@param dfa finite automaton.\n\
@return set of pairs of distinguishable states" );

auto DistinguishableStatesDFTA = registration::AbstractRegister < automaton::properties::DistinguishableStates, ext::set < ext::pair < DefaultStateType, DefaultStateType > >, const automaton::DFTA < > & > ( automaton::properties::DistinguishableStates::distinguishable, "fta" ).setDocumentation (
"Computes the pairs of distinguishable states in given DFTA.\n\
\n\
@param dfta finite tree automaton.\n\
@return set of pairs of distinguishable states" );

auto DistinguishableStatesUnorderedDFTA = registration::AbstractRegister < automaton::properties::DistinguishableStates, ext::set < ext::pair < DefaultStateType, DefaultStateType > >, const automaton::UnorderedDFTA < > & > ( automaton::properties::DistinguishableStates::distinguishable, "fta" ).setDocumentation (
"Computes the pairs of distinguishable states in given UnorderedDFTA.\n\
\n\
@param unordered dfta finite tree automaton.\n\
@return set of pairs of distinguishable states" );

} /* namespace */
