/*
 * BackwardBisimulation.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: 27. 3. 2019
 *	  Author: Tomas Pecka
 */

#ifndef BACKWARD_BISIMULATION_H_
#define BACKWARD_BISIMULATION_H_

#include <alib/set>
#include <alib/map>

#include <automaton/TA/DFTA.h>
#include <automaton/TA/NFTA.h>

#include <automaton/TA/UnorderedDFTA.h>
#include <automaton/TA/UnorderedNFTA.h>

#include <automaton/FSM/DFA.h>
#include <automaton/FSM/NFA.h>

namespace automaton {

namespace properties {

/**
 * Find all backwardBisimulation pairs of states of DFA.
 * Implements table-filling algorithm, Hopcroft 2nd edition, 4.4.1
 */
class BackwardBisimulation {
	template < class StateType >
	static ext::set < ext::pair < StateType, StateType > > initial ( const ext::set < StateType > & states, const ext::set < StateType > & initials ) {
		ext::set < ext::pair < StateType, StateType > > init;

		for ( const StateType & a : states ) {
			for ( const StateType & b : states ) {
				if ( initials.count ( a ) == initials.count ( b ) ) {
					init.insert ( ext::make_pair ( a, b ) );
					init.insert ( ext::make_pair ( b, a ) );
				}
			}
		}

		return init;
	}

public:
	/**
	 * Computes a relation on states of the DFA satisfying the backward bisimulation definition.
	 *
	 * @param fta the examined automaton
	 *
	 * @return set of pairs of states of the @p fta that are the backward bisimulation.
	 */
	template < class SymbolType, class StateType >
	static ext::set < ext::pair < StateType, StateType > > backwardBisimulation ( const automaton::DFA < SymbolType, StateType > & fta );

	/**
	 * Computes a relation on states of the NFA satisfying the backward bisimulation definition.
	 *
	 * @param fta the examined automaton
	 *
	 * @return set of pairs of states of the @p fta that are the backward bisimulation.
	 */
	template < class SymbolType, class StateType >
	static ext::set < ext::pair < StateType, StateType > > backwardBisimulation ( const automaton::NFA < SymbolType, StateType > & fta );

	/**
	 * Computes a relation on states of the DFTA satisfying the backward bisimulation definition.
	 *
	 * @param fta the examined automaton
	 *
	 * @return set of pairs of states of the @p fta that are the backward bisimulation.
	 */
	template < class SymbolType, class StateType >
	static ext::set < ext::pair < StateType, StateType > > backwardBisimulation ( const automaton::DFTA < SymbolType, StateType > & fta );

	/**
	 * Computes a relation on states of the NFTA satisfying the backward bisimulation definition.
	 *
	 * @param fta the examined automaton
	 *
	 * @return set of pairs of states of the @p fta that are the backward bisimulation.
	 */
	template < class SymbolType, class StateType >
	static ext::set < ext::pair < StateType, StateType > > backwardBisimulation ( const automaton::NFTA < SymbolType, StateType > & fta );

	/**
	 * Computes a relation on states of the DFTA satisfying the backward bisimulation definition.
	 *
	 * @param fta the examined automaton
	 *
	 * @return set of pairs of states of the @p fta that are the backward bisimulation.
	 */
	template < class SymbolType, class StateType >
	static ext::set < ext::pair < StateType, StateType > > backwardBisimulation ( const automaton::UnorderedDFTA < SymbolType, StateType > & fta );

	/**
	 * Computes a relation on states of the NFTA satisfying the backward bisimulation definition.
	 *
	 * @param fta the examined automaton
	 *
	 * @return set of pairs of states of the @p fta that are the backward bisimulation.
	 */
	template < class SymbolType, class StateType >
	static ext::set < ext::pair < StateType, StateType > > backwardBisimulation ( const automaton::UnorderedNFTA < SymbolType, StateType > & fta );
};

template < class SymbolType, class StateType >
ext::set < ext::pair < StateType, StateType > > BackwardBisimulation::backwardBisimulation ( const automaton::DFA < SymbolType, StateType > & fta ) {
	return backwardBisimulation ( NFA < SymbolType, StateType > ( fta ) );
}

template < class SymbolType, class StateType >
ext::set < ext::pair < StateType, StateType > > BackwardBisimulation::backwardBisimulation ( const automaton::NFA < SymbolType, StateType > & fta ) {
	ext::set < ext::pair < StateType, StateType > > backwardBisimulation = initial ( fta.getStates ( ), { fta.getInitialState ( ) } );

	bool changed;
	do {
		changed = false;

		for ( const StateType & p : fta.getStates ( ) ) {
			for ( const StateType & q : fta.getStates ( ) ) {
				if ( ! backwardBisimulation.contains ( ext::make_pair ( p, q ) ) )
					continue;

				for ( const std::pair < const ext::pair < StateType, SymbolType >, StateType > & pTransition : fta.getTransitionsToState ( p ) ) {

					bool exists = false;
					for ( const std::pair < const ext::pair < StateType, SymbolType >, StateType > & qTransition : fta.getTransitionsToState ( q ) ) {
						if ( qTransition.first.second != pTransition.first.second )
							continue;

						if ( backwardBisimulation.contains ( ext::make_pair ( pTransition.first.first, qTransition.first.first ) ) ) {
							exists = true;
							break;
						}
					}

					if ( ! exists ) {
						backwardBisimulation.erase ( ext::make_pair ( p, q ) );
						backwardBisimulation.erase ( ext::make_pair ( q, p ) );
						changed = true;
						break;
					}
				}
			}
		}
	} while ( changed );

	return backwardBisimulation;
}

template < class SymbolType, class StateType >
ext::set < ext::pair < StateType, StateType > > BackwardBisimulation::backwardBisimulation ( const automaton::DFTA < SymbolType, StateType > & fta ) {
	return backwardBisimulation ( NFTA < SymbolType, StateType > ( fta ) );
}

template < class SymbolType, class StateType >
ext::set < ext::pair < StateType, StateType > > BackwardBisimulation::backwardBisimulation ( const automaton::NFTA < SymbolType, StateType > & fta ) {
	ext::set < ext::pair < StateType, StateType > > backwardBisimulation = initial ( fta.getStates ( ), { } );

	bool changed;
	do {
		changed = false;

		for ( const StateType & p : fta.getStates ( ) ) {
			for ( const StateType & q : fta.getStates ( ) ) {
				if ( ! backwardBisimulation.contains ( ext::make_pair ( p, q ) ) )
					continue;

				for ( const std::pair < const ext::pair < common::ranked_symbol < SymbolType >, ext::vector < StateType > >, StateType > & pTransition : fta.getTransitionsToState ( p ) ) {

					bool exists = false;
					for ( const std::pair < const ext::pair < common::ranked_symbol < SymbolType >, ext::vector < StateType > >, StateType > & qTransition : fta.getTransitionsToState ( q ) ) {
						if ( qTransition.first.first != pTransition.first.first )
							continue;

						size_t inRelation = 0;
						for ( size_t i = 0; i < pTransition.first.second.size ( ); ++ i ) {
							if ( backwardBisimulation.contains ( ext::make_pair ( pTransition.first.second [ i ], qTransition.first.second [ i ] ) ) )
								++ inRelation;
						}

						if ( inRelation == pTransition.first.second.size ( ) ) {
							exists = true;
							break;
						}
					}

					if ( ! exists ) {
						backwardBisimulation.erase ( ext::make_pair ( p, q ) );
						backwardBisimulation.erase ( ext::make_pair ( q, p ) );
						changed = true;
						break;
					}
				}
			}
		}
	} while ( changed );

	return backwardBisimulation;
}

template < class SymbolType, class StateType >
ext::set < ext::pair < StateType, StateType > > BackwardBisimulation::backwardBisimulation ( const automaton::UnorderedDFTA < SymbolType, StateType > & fta ) {
	return backwardBisimulation ( UnorderedNFTA < SymbolType, StateType > ( fta ) );
}

template < class SymbolType, class StateType >
ext::set < ext::pair < StateType, StateType > > BackwardBisimulation::backwardBisimulation ( const automaton::UnorderedNFTA < SymbolType, StateType > & fta ) {
	ext::set < ext::pair < StateType, StateType > > backwardBisimulation = initial ( fta.getStates ( ), { } );

	bool changed;
	do {
		changed = false;

		for ( const StateType & p : fta.getStates ( ) ) {
			for ( const StateType & q : fta.getStates ( ) ) {
				if ( ! backwardBisimulation.contains ( ext::make_pair ( p, q ) ) )
					continue;

				for ( const std::pair < const ext::pair < common::ranked_symbol < SymbolType >, ext::multiset < StateType > >, StateType > & pTransition : fta.getTransitionsToState ( p ) ) {

					ext::multiset < StateType > pSource;
					for ( const StateType & state : pTransition.first.second ) {
						auto lower = backwardBisimulation.lower_bound ( ext::slice_comp ( state ) );
						pSource.insert ( lower->second );
					}

					bool exists = false;
					for ( const std::pair < const ext::pair < common::ranked_symbol < SymbolType >, ext::multiset < StateType > >, StateType > & qTransition : fta.getTransitionsToState ( q ) ) {
						if ( qTransition.first.first != pTransition.first.first )
							continue;

						ext::multiset < StateType > qSource;
						for ( const StateType & state : qTransition.first.second ) {
							auto lower = backwardBisimulation.lower_bound ( ext::slice_comp ( state ) );
							qSource.insert ( lower->second );
						}

						if ( pSource == qSource ) {
							exists = true;
							break;
						}
					}

					if ( ! exists ) {
						backwardBisimulation.erase ( ext::make_pair ( p, q ) );
						backwardBisimulation.erase ( ext::make_pair ( q, p ) );
						changed = true;
						break;
					}
				}
			}
		}
	} while ( changed );

	return backwardBisimulation;
}

} /* namespace properties */

} /* namespace automaton */

#endif /* BACKWARD_BISIMULATION_H_ */
