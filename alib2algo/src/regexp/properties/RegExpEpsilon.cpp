/*
 * RegExpEpsilon.cpp
 *
 *  Created on: 19. 1. 2014
 *	  Author: Tomas Pecka
 */

#include "RegExpEpsilon.h"
#include <regexp/formal/FormalRegExpElements.h>
#include <regexp/unbounded/UnboundedRegExpElements.h>
#include <registration/AlgoRegistration.hpp>

namespace {

auto RegExpEpsilonFormalRegExp = registration::AbstractRegister < regexp::properties::RegExpEpsilon, bool, const regexp::FormalRegExp < > & > ( regexp::properties::RegExpEpsilon::languageContainsEpsilon, "regexp" ).setDocumentation (
"Determines whether regular expression describes a language containing epsilon (\\eps \\in h(regexp)).\n\
\n\
@param regexp the regexp to test\n\
@return true of the language described by the regular expression contains epsilon" );

auto RegExpEpsilonUnboundedRegExp = registration::AbstractRegister < regexp::properties::RegExpEpsilon, bool, const regexp::UnboundedRegExp < > & > ( regexp::properties::RegExpEpsilon::languageContainsEpsilon, "regexp" ).setDocumentation (
"Determines whether regular expression describes a language containing epsilon (\\eps \\in h(regexp)).\n\
\n\
@param regexp the regexp to test\n\
@return true of the language described by the regular expression contains epsilon" );

} /* namespace */
