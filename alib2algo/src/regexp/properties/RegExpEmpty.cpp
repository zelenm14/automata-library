/*
 * RegExpEmpty.cpp
 *
 *  Created on: 19. 1. 2014
 *	  Author: Tomas Pecka
 */

#include "RegExpEmpty.h"
#include <regexp/formal/FormalRegExpElements.h>
#include <regexp/unbounded/UnboundedRegExpElements.h>
#include <registration/AlgoRegistration.hpp>

namespace {

auto RegExpEmptyFormalRegExp = registration::AbstractRegister < regexp::properties::RegExpEmpty, bool, const regexp::FormalRegExp < > & > ( regexp::properties::RegExpEmpty::languageIsEmpty, "regexp" ).setDocumentation (
"Determines whether regular expression is describes an empty language (regexp == \\0)\n\
\n\
@param regexp the regexp to test\n\
@return true of the language described by the regular expression is empty" );

auto RegExpEmptyUnboundedRegExp = registration::AbstractRegister < regexp::properties::RegExpEmpty, bool, const regexp::UnboundedRegExp < > & > ( regexp::properties::RegExpEmpty::languageIsEmpty, "regexp" ).setDocumentation (
"Determines whether regular expression is describes an empty language (regexp == \\0)\n\
\n\
@param regexp the regexp to test\n\
@return true of the language described by the regular expression is empty" );

} /* namespace */
