/*
 * RegExpIterate.cpp
 *
 *  Created on: 17. 10. 2014
 *	  Author: Jan Travnicek
 */

#include "RegExpIterate.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto RegExpIterateFormalRegExpFormalRegExp = registration::AbstractRegister < regexp::transform::RegExpIterate, regexp::FormalRegExp < >, const regexp::FormalRegExp < > & > ( regexp::transform::RegExpIterate::iterate, "regexp" ).setDocumentation (
"Implements iteration of regular expression.\n\
\n\
@param regexp the regexp to iterate\n\
@return regexp describing @p regexp *" );

auto RegExpIterateUnboundedRegExpUnboundedRegExp = registration::AbstractRegister < regexp::transform::RegExpIterate, regexp::UnboundedRegExp < >, const regexp::UnboundedRegExp < > & > ( regexp::transform::RegExpIterate::iterate, "regexp" ).setDocumentation (
"Implements iteration of regular expression.\n\
\n\
@param regexp the regexp to iterate\n\
@return regexp describing @p regexp *" );

} /* namespace */
