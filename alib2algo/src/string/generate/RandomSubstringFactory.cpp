/*
 * RandomSubstringFactory.cpp
 *
 *  Created on: 27. 3. 2014
 *	  Author: Jan Travnicek
 */

#include "RandomSubstringFactory.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto RandomSubstringFactoryLinearString = registration::AbstractRegister < string::generate::RandomSubstringFactory, string::LinearString < >, size_t, const string::LinearString < > & > ( string::generate::RandomSubstringFactory::generateSubstring, "size", "string" ).setDocumentation (
"Generates a random substring of a given size from a string.\n\
\n\
@param size the length of generated substring\n\
@param string the source string of the substring\n\
@return a substring of the @p string" );

} /* namespace */
