/*
 * Repetition.cpp
 *
 *  Created on: 27.3.2020
 *      Author: Jan Jirak
 */

#include "Repetition.h"
#include <registration/AlgoRegistration.hpp>

namespace {
// todo Note is it necessary to register
auto Repetition = registration::AbstractRegister < string::properties::Repetition, ext::tuple < size_t, size_t, size_t >, const string::LinearString < > & > ( string::properties::Repetition::construct );

} /* namespace */
