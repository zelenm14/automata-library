/*
 * StringReverse.h
 *
 *  Created on: 30. 9. 2019
 *	  Author: Jan Travnicek
 */

#ifndef STRING_REVERSE_H_
#define STRING_REVERSE_H_

#include <string/LinearString.h>

namespace string {

namespace transform {

/**
 * Implements the reverse of a string.
 *
 */
class StringReverse {
public:
	/**
	 * Implements the reverse of a string.
	 *
	 * \tparam SymbolType the type of symbols in the string
	 *
	 * \param arg the string to reverse
	 *
	 * \return string arg ^ R
	 */
	template < class SymbolType >
	static string::LinearString < SymbolType > reverse ( const string::LinearString < SymbolType > & arg );

};

template < class SymbolType >
string::LinearString < SymbolType > StringReverse::reverse ( const string::LinearString < SymbolType > & arg ) {
	ext::vector < SymbolType > content;
	content.insert ( content.end ( ), arg.getContent ( ).rbegin ( ), arg.getContent ( ).rend ( ) );

	return string::LinearString < SymbolType > ( arg.getAlphabet ( ), content );
}

} /* namespace transform */

} /* namespace string */

#endif /* STRING_REVERSE_H_ */
