//
// Created by shushiri on 25.2.19.
//

#include "RestrictedApproximateCoversComputation.h"
#include <registration/AlgoRegistration.hpp>

namespace stringology::cover {

auto RestictedApproximateCoversLinearString = registration::AbstractRegister < RestrictedApproximateCoversComputation, ext::set < ext::pair < string::LinearString < DefaultSymbolType >, unsigned > >, const string::LinearString < > &, unsigned > ( RestrictedApproximateCoversComputation::compute );

} /* namespace stringology::cover */
