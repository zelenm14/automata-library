//
// Created by shushiri on 1.1.19.
//

#include "ExactCoversComputation.h"
#include <registration/AlgoRegistration.hpp>

namespace stringology::cover {

auto ExactCoversLinearString = registration::AbstractRegister < ExactCoversComputation, ext::set < string::LinearString < DefaultSymbolType > >, const string::LinearString < > & > ( ExactCoversComputation::compute );

}  /* namespace stringology::cover */
