/*
 * SuffixTrieNaive.cpp
 *
 *  Created on: 1. 11. 2014
 *      Author: Tomas Pecka
 */

#include "SuffixTrieNaive.h"

#include <string/LinearString.h>
#include <registration/AlgoRegistration.hpp>

namespace {

auto suffixTrieNaiveLinearString = registration::AbstractRegister < stringology::indexing::SuffixTrieNaive, indexes::stringology::SuffixTrie < DefaultSymbolType >, const string::LinearString < > & > ( stringology::indexing::SuffixTrieNaive::construct );

} /* namespace */
