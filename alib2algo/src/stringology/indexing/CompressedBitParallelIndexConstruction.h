/*
 * CompressedBitParallelIndex.h
 *
 *  Created on: 6. 2. 2017
 *      Author: Jan Travnicek
 */

#ifndef COMPRESSED_BIT_PARALLEL_INDEX_CONSTRUCTION_H_
#define COMPRESSED_BIT_PARALLEL_INDEX_CONSTRUCTION_H_

#include <indexes/stringology/CompressedBitParallelIndex.h>
#include <string/LinearString.h>
#include <exception/CommonException.h>

namespace stringology {

namespace indexing {

/**
 * Constructs a compressed bit parallel index for given string.
 *
 */

class CompressedBitParallelIndexConstruction {
public:
	/**
	 * Creates suffix trie
	 * @param string string to construct suffix trie for
	 * @return automaton
	 */
	template < class SymbolType >
	static indexes::stringology::CompressedBitParallelIndex < SymbolType > construct ( const string::LinearString < SymbolType > & w );

};

template < class SymbolType >
indexes::stringology::CompressedBitParallelIndex < SymbolType > CompressedBitParallelIndexConstruction::construct ( const string::LinearString < SymbolType > & w ) {
	ext::map < SymbolType, common::SparseBoolVector > res;
	for ( const SymbolType & symbol : w.getAlphabet ( ) )
		res [ symbol ].resize ( w.getContent ( ).size ( ) );

	for ( unsigned i = 0; i < w.getContent ( ).size ( ); ++i )
		res [ w.getContent ( ) [ i ] ] [ i ] = true;

	return indexes::stringology::CompressedBitParallelIndex < SymbolType > ( w.getAlphabet ( ), std::move ( res ) );
}

} /* namespace indexing */

} /* namespace stringology */

#endif /* COMPRESSED_BIT_PARALLEL_INDEX_CONSTRUCTION_H_ */
