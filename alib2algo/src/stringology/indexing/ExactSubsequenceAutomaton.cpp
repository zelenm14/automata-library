/*
 * ExactSubsequenceAutomaton.cpp
 *
 *  Created on: 7. 4. 2015
 *      Author: Jan Travnicek
 */

#include "ExactSubsequenceAutomaton.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ExactSubsequenceAutomatonLinearString = registration::AbstractRegister < stringology::indexing::ExactSubsequenceAutomaton, automaton::DFA < DefaultSymbolType, unsigned >, const string::LinearString < > & > ( stringology::indexing::ExactSubsequenceAutomaton::construct );

} /* namespace */
