/*
 * PositionHeapNaive.cpp
 *
 *  Created on: 6. 2. 2017
 *      Author: Jan Travnicek
 */

#include "PositionHeapNaive.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto positionHeapNaiveLinearString = registration::AbstractRegister < stringology::indexing::PositionHeapNaive, indexes::stringology::PositionHeap < DefaultSymbolType >, const string::LinearString < > & > ( stringology::indexing::PositionHeapNaive::construct );

} /* namespace */
