/*
 * HammingMatchingAutomaton.cpp
 *
 *  Created on: 12. 3. 2018
 *      Author: Tomas Capek
 */

#include "LevenshteinMatchingAutomaton.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto LevenshteinMatchingAutomatonLinearString = registration::AbstractRegister <stringology::matching::LevenshteinMatchingAutomaton, automaton::EpsilonNFA < DefaultSymbolType, ext::pair<unsigned int, unsigned int>>, const string::LinearString < > &, unsigned > ( stringology::matching::LevenshteinMatchingAutomaton::construct );

auto LevenshteinMatchingAutomatonWildcardLinearString = registration::AbstractRegister <stringology::matching::LevenshteinMatchingAutomaton, automaton::EpsilonNFA < DefaultSymbolType, ext::pair<unsigned int, unsigned int>>, const string::WildcardLinearString < > &, unsigned > ( stringology::matching::LevenshteinMatchingAutomaton::construct );

} /* namespace */
