/*
 * LevenshteinSequenceMatchingAutomaton.cpp
 *
 *  Created on: 29. 3. 2018
 *      Author: Tomas Capek
 */

#include "GeneralizedLevenshteinSequenceMatchingAutomaton.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto GeneralizedLevenshteinSequenceMatchingAutomatonLinearString = registration::AbstractRegister <stringology::matching::GeneralizedLevenshteinSequenceMatchingAutomaton, automaton::EpsilonNFA < DefaultSymbolType, ext::pair<unsigned int, unsigned int> >, const string::LinearString < > &, unsigned > ( stringology::matching::GeneralizedLevenshteinSequenceMatchingAutomaton::construct );

auto GeneralizedLevenshteinSequenceMatchingAutomatonWildcardLinearString = registration::AbstractRegister <stringology::matching::GeneralizedLevenshteinSequenceMatchingAutomaton, automaton::EpsilonNFA < DefaultSymbolType, ext::pair<unsigned int, unsigned int> >, const string::WildcardLinearString < > &, unsigned > ( stringology::matching::GeneralizedLevenshteinSequenceMatchingAutomaton::construct );

} /* namespace */
