/*
 * Author: Radovan Cerveny
 */

#include "DAWGMatcherConstruction.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto DAWGMatcherConstructionLinearString = registration::AbstractRegister < stringology::matching::DAWGMatcherConstruction, indexes::stringology::SuffixAutomaton < DefaultSymbolType >, const string::LinearString < > & > ( stringology::matching::DAWGMatcherConstruction::construct );

} /* namespace */
