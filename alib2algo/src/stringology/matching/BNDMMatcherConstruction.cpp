/*
 * BNDMMatcherConstruction.cpp
 *
 *  Created on: 6. 2. 2017
 *      Author: Jan Travnicek
 */

#include "BNDMMatcherConstruction.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto BNDMIndexConstructionLinearString = registration::AbstractRegister < stringology::matching::BNDMMatcherConstruction, indexes::stringology::BitSetIndex < >, const string::LinearString < > & > ( stringology::matching::BNDMMatcherConstruction::construct );

} /* namespace */
