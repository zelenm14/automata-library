/*
 * LyndonFactoring.cpp
 *
 *  Created on: 30. 8. 2018
 *      Author: Jan Travnicek
 */

#include "LyndonFactoring.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto lyndonFactoringString = registration::AbstractRegister < stringology::properties::LyndonFactoring, ext::vector < unsigned >, const string::LinearString < > & > ( stringology::properties::LyndonFactoring::factorize ).setDocumentation (
"Computes the lyndon factoring of a given nonempty string (Duval algorithm)\n\
\n\
@param string the nonempty string to factorize\n\
@return positions where the string is split to nyldon factors" );

} /* namespace */
