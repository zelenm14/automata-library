/*
 * HammingDynamicProgramming.cpp
 *
 *  Created on: 12. 3. 2018
 *      Author: Tomas Capek
 */

#include "HammingDynamicProgramming.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto HammingDynamicProgrammingLinearString = registration::AbstractRegister < stringology::simulations::HammingDynamicProgramming, ext::set < unsigned >, const string::LinearString < > &, const string::LinearString < > &, unsigned > ( stringology::simulations::HammingDynamicProgramming::search );

} /* namespace */
