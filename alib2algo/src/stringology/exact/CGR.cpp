/*
 * CGR.cpp
 *
 *  Created on: 19. 3. 2020
 *      Author: Jan Jirak
 */

#include "CGR.h"
#include <registration/AlgoRegistration.hpp>

namespace {

    auto CGR = registration::AbstractRegister < stringology::exact::CGR, ext::set < unsigned >, const string::LinearString < > &, const string::LinearString < > & > ( stringology::exact::CGR::match );

} /* namespace */
