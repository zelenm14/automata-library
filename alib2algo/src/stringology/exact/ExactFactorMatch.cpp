/*
 * ExactFactorMatch.cpp
 *
 *  Created on: 9. 2. 2014
 *      Author: Jan Travnicek
 */

#include "ExactFactorMatch.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ExactFactorMatchLinearString = registration::AbstractRegister < stringology::exact::ExactFactorMatch, ext::set < unsigned >, const string::LinearString < > &, const string::LinearString < > & > ( stringology::exact::ExactFactorMatch::match );

} /* namespace */
