/*
 * DeadZoneUsingBadCharacterShift.cpp
 *
 *  Created on: 5. 11. 2014
 *      Author: Radomir Polach, Tomas Pecka
 */

#include "DeadZoneUsingBadCharacterShift.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto DeadZoneUsingBadCharacterShiftLinearStringLinearString = registration::AbstractRegister < stringology::exact::DeadZoneUsingBadCharacterShift, ext::set < unsigned >, const string::LinearString < > &, const string::LinearString < > & > ( stringology::exact::DeadZoneUsingBadCharacterShift::match );

} /* namespace */
