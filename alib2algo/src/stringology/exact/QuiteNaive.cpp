/*
 * QuiteNaive.cpp
 *
 *  Created on: 19. 3. 2020
 *      Author: Jan Jirak
 */

#include "QuiteNaive.h"
#include <registration/AlgoRegistration.hpp>

namespace {

    auto QuiteNaive = registration::AbstractRegister < stringology::exact::QuiteNaive, ext::set < unsigned >, const string::LinearString < > &, const string::LinearString < > & > ( stringology::exact::QuiteNaive::match );

} /* namespace */
