/*
 * CompressedBitParallelismFactors.cpp
 *
 *  Created on: 2. 1. 2017
 *      Author: Jan Travnicek
 */

#include "CompressedBitParallelismFactors.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto CompressedBitParallelismFactorsLinearString = registration::AbstractRegister < stringology::query::CompressedBitParallelismFactors, ext::set < unsigned >, const indexes::stringology::CompressedBitParallelIndex < > &, const string::LinearString < > & > ( stringology::query::CompressedBitParallelismFactors::query );

} /* namespace */
