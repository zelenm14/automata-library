/*
 * SuffixArrayFactors.cpp
 *
 *  Created on: 10. 1. 2017
 *      Author: Jan Travnicek
 */

#include "SuffixArrayFactors.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto SuffixArrayFactorsLinearString = registration::AbstractRegister < stringology::query::SuffixArrayFactors, ext::set < unsigned >, const indexes::stringology::SuffixArray < > &, const string::LinearString < > & > ( stringology::query::SuffixArrayFactors::query );

} /* namespace */
