/*
 * Author: Radovan Cerveny
 */

#include "BackwardOracleMatching.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto BackwardOracleMatchingLinearStringLinearString = registration::AbstractRegister < stringology::query::BackwardOracleMatching, ext::set < unsigned >, const string::LinearString < > &, const automaton::DFA < > & > ( stringology::query::BackwardOracleMatching::match );

} /* namespace */
