//
// Created by dolanver on 12/11/19.
//

#ifndef RESTRICTEDAPPROXIMATESEEDCOMPUTATION_H
#define RESTRICTEDAPPROXIMATESEEDCOMPUTATION_H


#include <alib/set>
#include <alib/pair>
#include <string/LinearString.h>
#include <stringology/seed/ApproximateSeedComputation.h>


namespace stringology::seed {

/**
 * Class to compute the set of all restricted approximate seeds of a given string with maximum Hamming distance
 *
 * Source: Veronika Dolanska: Implementace automatovych algoritmu na hledani jader (2020)
 */
class RestrictedApproximateSeedComputation {
public:
	/**
	 * Computes all restricted k-approximate seeds of a string, approximation is under Hamming distance
	 * Source: Veronika Dolanska: Implementace automatovych algoritmu na hledani jader (2020)
	 *
	 * @param pattern - string for which seeds are computed
	 * @param distance - max allowed Hamming distance k
	 * @return set of all restricted k-approximate seeds of the input string and their minimal Hamming distance
	 */
	template<class SymbolType>
	static ext::set < ext::pair < string::LinearString < SymbolType >, unsigned > > compute ( const string::LinearString < SymbolType > & pattern, unsigned distance ){
		return stringology::seed::ApproximateSeedComputation::compute( pattern, distance, true );
	}
};

}
#endif // RESTRICTEDAPPROXIMATESEEDCOMPUTATION_H
