//
// Created by dolanver on 2/16/20.
//

#include <registration/AlgoRegistration.hpp>
#include "ApproximateSeedComputation.h"


namespace stringology::seed {
    auto ApproximateSeedsLinearString = registration::AbstractRegister < ApproximateSeedComputation, ext::set<ext::pair<string::LinearString < DefaultSymbolType >, unsigned > >, const string::LinearString < > &, unsigned > ( ApproximateSeedComputation::compute );

}