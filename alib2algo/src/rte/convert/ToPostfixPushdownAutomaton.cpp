/*
 * ToPostfixPushdownAutomaton.h
 *
 *  Created on: 11. 4. 2016
 *	  Author: Tomas Pecka
 */

#include "ToPostfixPushdownAutomaton.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ToAutomatonFormalRegExp = registration::AbstractRegister < rte::convert::ToPostfixPushdownAutomaton, automaton::NPDA < ext::variant < common::ranked_symbol < DefaultSymbolType >, alphabet::EndSymbol >, ext::variant < common::ranked_symbol < ext::pair < DefaultSymbolType, unsigned > >, alphabet::BottomOfTheStackSymbol >, char >, const rte::FormalRTE < > & > ( rte::convert::ToPostfixPushdownAutomaton::convert );

} /* namespace */
