/*
 * ToPostfixPushdownAutomatonGlushkovNaive.h
 *
 *  Created on: 11. 4. 2016
 *	  Author: Tomas Pecka
 */

#include "ToPostfixPushdownAutomatonGlushkovNaive.h"

#include <registration/AlgoRegistration.hpp>

namespace {

auto ToPostfixPushdownAutomatonGlushkovNaiveFormalRTE = registration::AbstractRegister < rte::convert::ToPostfixPushdownAutomatonGlushkovNaive, automaton::NPDA < ext::variant < common::ranked_symbol < DefaultSymbolType >, alphabet::EndSymbol >, ext::variant < common::ranked_symbol < ext::pair < DefaultSymbolType, unsigned > >, alphabet::BottomOfTheStackSymbol >, char >, const rte::FormalRTE < > & > ( rte::convert::ToPostfixPushdownAutomatonGlushkovNaive::convert );

} /* namespace */
