#include <catch2/catch.hpp>

#include <string/LinearString.h>
#include <stringology/simulations/GeneralizedLevenshteinDynamicProgramming.h>

TEST_CASE ( "Generalized Levenshtein Dynamic Programming", "[unit][algo][stringology][simulations]" ) {
	SECTION ( "Table" ) {
		auto text = string::LinearString<>("adbcbaabadbbca");
		auto pattern = string::LinearString<>("adbbca");

		ext::vector<ext::vector<unsigned int>> expected_result = {
			ext::vector<unsigned int>({0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}),
			ext::vector<unsigned int>({1, 0, 1, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0}),
			ext::vector<unsigned int>({2, 1, 0, 1, 2, 2, 1, 1, 1, 1, 0, 1, 2, 2, 1}),
			ext::vector<unsigned int>({3, 2, 1, 0, 1, 2, 2, 2, 1, 2, 1, 0, 1, 2, 2}),
			ext::vector<unsigned int>({4, 3, 2, 1, 1, 1, 2, 3, 2, 2, 2, 1, 0, 1, 2}),
			ext::vector<unsigned int>({5, 4, 3, 2, 1, 1, 2, 3, 3, 3, 3, 2, 1, 0, 1}),
			ext::vector<unsigned int>({6, 5, 4, 3, 2, 2, 1, 2, 4, 3, 4, 3, 2, 1, 0}),
		};

		CHECK(expected_result == stringology::simulations::GeneralizedLevenshteinDynamicProgramming::compute_table(text, pattern));
	}

	SECTION ( "Occurences" ) {
		auto text = string::LinearString<>("adbcbaabadbbca");
		auto pattern = string::LinearString<>("adbbca");

		ext::set<unsigned int> expected_result = {3, 4, 5, 6, 7, 9, 11, 12, 13, 14};
		auto result = stringology::simulations::GeneralizedLevenshteinDynamicProgramming::search(text, pattern, 3);

		CHECK(expected_result == result);
	}
}
