//
// Created by dolan on 4/9/2020.
//

#include <catch2/catch.hpp>
#include <string/LinearString.h>
#include <stringology/seed/ApproximateSeedComputation.h>

TEST_CASE ( "Approximate seed computation", "[unit][stringology][seed]" ) {
	string::LinearString < char > pattern;
	ext::set < ext::pair < string::LinearString < char >, unsigned > > expectedSeeds;

	SECTION ( "Compute approximate seeds for empty string with Hamming distance = 1" ) {
		pattern = string::LinearString < char > ( "" );
		expectedSeeds = { };
	}

	SECTION ( "Compute approximate seeds for 'abab' with Hamming distance = 1" ) {
		pattern = string::LinearString < char > ( "abab" );
		expectedSeeds = {
			{ string::LinearString < char >( { 'a', 'b' }, { 'a', 'b' } ), 0 },
			{ string::LinearString < char >( { 'a', 'b' }, { 'a', 'b', 'a' } ), 0 },
			{ string::LinearString < char >( { 'a', 'b' }, { 'b', 'a' } ), 0 },
			{ string::LinearString < char >( { 'a', 'b' }, { 'b', 'a', 'b' } ), 0 },
			{ string::LinearString < char >( { 'a', 'b' }, { 'a', 'a' } ), 1 },
			{ string::LinearString < char >( { 'a', 'b' }, { 'a', 'a', 'a' } ), 1 },
			{ string::LinearString < char >( { 'a', 'b' }, { 'a', 'a', 'b' } ), 1 },
			{ string::LinearString < char >( { 'a', 'b' }, { 'a', 'b', 'b'} ), 1 },
			{ string::LinearString < char >( { 'a', 'b' }, { 'b', 'b' } ), 1 },
			{ string::LinearString < char >( { 'a', 'b' }, { 'b', 'b', 'b' } ), 1 },
			{ string::LinearString < char >( { 'a', 'b' }, { 'b', 'b', 'a' } ), 1 },
			{ string::LinearString < char >( { 'a', 'b' }, { 'b', 'a', 'a' } ), 1 },
		};
	}

	SECTION ( "Compute approximate seeds for 'aa' with Hamming distance = 1" ) {
		pattern = string::LinearString < char > ( "aa" );
		expectedSeeds = {
			{ string::LinearString < char > ( { 'a' }, { 'a' } ), 0 }
		};
	}

	SECTION ( "Compute approximate seeds for 'ab' with Hamming distance = 1" ) {
		pattern = string::LinearString < char > ( "ab" );
		expectedSeeds = { };
	}

	CHECK ( stringology::seed::ApproximateSeedComputation::compute( pattern, 1 ) == expectedSeeds );
}
