#include <catch2/catch.hpp>

#include <string/LinearString.h>
#include <stringology/exact/ExactFactorMatch.h>

TEST_CASE ( "Exact String Match", "[unit][algo][stringology][exact]" ) {
	SECTION ( "Basic" ) {
		string::LinearString < > linear1({DefaultSymbolType(0), DefaultSymbolType(1), DefaultSymbolType(2), DefaultSymbolType(3)});
		string::LinearString < > linear2({DefaultSymbolType(1), DefaultSymbolType(2), DefaultSymbolType(3)});

		CHECK ( stringology::exact::ExactFactorMatch::match ( linear1, linear2 ).count ( 1u ) );
	}
}
