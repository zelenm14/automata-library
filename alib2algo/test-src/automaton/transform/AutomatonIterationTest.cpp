#include <catch2/catch.hpp>

#include <alib/list>

#include "automaton/transform/AutomatonIteration.h"
#include "automaton/transform/AutomatonIterationEpsilonTransition.h"

#include "automaton/simplify/MinimizeBrzozowski.h"
#include "automaton/simplify/Normalize.h"
#include "automaton/simplify/EpsilonRemoverIncoming.h"
#include "automaton/simplify/Trim.h"
#include "automaton/simplify/Total.h"
#include "automaton/determinize/Determinize.h"

TEST_CASE ( "Automata Iteration", "[unit][algo][automaton][transform]" ) {
	SECTION ( "NFA/DFA" ) {
		// Melichar 2.83
		DefaultStateType q0 = DefaultStateType(0);
		DefaultStateType q1 = DefaultStateType(1);
		DefaultStateType q2 = DefaultStateType(2);
		DefaultStateType q3 = DefaultStateType(3);
		DefaultSymbolType a = DefaultSymbolType('a');
		DefaultSymbolType b = DefaultSymbolType('b');

		automaton::DFA < > m1(q1);
		m1.setStates({q1, q2, q3});
		m1.addFinalState(q3);
		m1.setInputAlphabet({a, b});
		m1.addTransition(q1, a, q2);
		m1.addTransition(q2, b, q2);
		m1.addTransition(q2, a, q3);
		m1.addTransition(q3, a, q1);

		automaton::EpsilonNFA < > res(q0);
		res.setStates({q0, q1, q2, q3});
		res.setInputAlphabet({a, b});
		res.setFinalStates({q0, q3});

		res.addTransition(q1, a, q2);
		res.addTransition(q2, b, q2);
		res.addTransition(q2, a, q3);
		res.addTransition(q3, a, q1);

		res.addTransition(q0, q1);
		res.addTransition(q3, q1);

		auto i2 = automaton::transform::AutomatonIterationEpsilonTransition::iteration(m1);
		auto i1 = automaton::transform::AutomatonIteration::iteration(m1);

		auto mdfa1 (automaton::simplify::Normalize::normalize(automaton::simplify::Trim::trim(automaton::simplify::MinimizeBrzozowski::minimize(automaton::simplify::EpsilonRemoverIncoming::remove(i1)))));
		auto mdfa2 (automaton::simplify::Normalize::normalize(automaton::simplify::Trim::trim(automaton::simplify::MinimizeBrzozowski::minimize(automaton::simplify::EpsilonRemoverIncoming::remove(i2)))));
		auto mdfa3 (automaton::simplify::Normalize::normalize(automaton::simplify::Trim::trim(automaton::simplify::MinimizeBrzozowski::minimize(automaton::simplify::EpsilonRemoverIncoming::remove(res)))));

		CHECK(mdfa1 == mdfa2);
		CHECK(mdfa1 == mdfa3);
	}
}
