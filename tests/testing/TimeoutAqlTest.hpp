#ifndef _TIMEOUT_AQL_TEST_HPP__
#define _TIMEOUT_AQL_TEST_HPP__

#include <catch2/catch.hpp>
#include <chrono>
#include <filesystem>
#include <fstream>

using namespace std::literals::chrono_literals;

void _TimeoutAqlTest ( const std::chrono::microseconds & timeout, const std::filesystem::path & file, bool timeoutError );
void _TimeoutAqlTest ( const std::chrono::microseconds & timeout, const std::vector < std::string > & queries, bool timeoutError );

/**
 * @param timeout timeout (use std chrono literals) ofthe test
 * @param queries list of queries to execute
 * @param timeoutError is timeout an error?
 */
template < class D >
void TimeoutAqlTest ( const D & timeout, const std::vector < std::string > & queries, bool timeoutError = false ) {
	_TimeoutAqlTest ( std::chrono::duration_cast < std::chrono::microseconds > ( timeout ), queries, timeoutError );
}

/**
 * @param timeout timeout (use std chrono literals) ofthe test
 * @param file path to file to test
 * @param timeoutError is timeout an error?
 */
template < class D >
void TimeoutAqlTest ( const D & timeout, const std::filesystem::path & file, bool timeoutError ) {
	_TimeoutAqlTest ( std::chrono::duration_cast < std::chrono::microseconds > ( timeout ), file, timeoutError );
}

#endif /* _TIMEOUT_AQL_TEST_HPP__ */
