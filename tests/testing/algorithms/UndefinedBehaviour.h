/*
 * UndefinedBehaviour.h
 *
 *  Created on: 4. 4. 2020
 *	  Author: Tomas Pecka
 */

#ifndef UNDEFINED_BEHAVIOUR_H_
#define UNDEFINED_BEHAVIOUR_H_

namespace debug {

class UndefinedBehaviour {

public:
	static int undefined_behaviour ( );
};

} /* namespace debug */

#endif /* UNDEFINED_BEHAVIOUR_H_ */
