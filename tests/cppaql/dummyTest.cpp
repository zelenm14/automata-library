#include <catch2/catch.hpp>
#include <alib/vector>

#include "testing/TimeoutAqlTest.hpp"
#include "testing/TestFiles.hpp"

TEST_CASE ( "AQL Test", "[integration][dummy][!shouldfail]" ) {
	ext::vector < std::string > qs = {
		"execute 1",
		"exec"
	};

	TimeoutAqlTest ( 1s, qs );
}

TEST_CASE ( "Segfault Test", "[integration][dummy][!hide][!shouldfail]" ) {
	ext::vector < std::string > qs = {
		"execute debug::Random @ int",
		"execute debug::Segfault"
	};

	TimeoutAqlTest ( 1s, qs );
}

TEST_CASE ( "Sanitizer Test", "[integration][dummy][!hide][!shouldfail]" ) {
	ext::vector < std::string > qs;
	SECTION ( "asan" ) {
		qs = {
			"execute debug::Random @ int",
			"execute debug::Segfault",
		};
	}

	SECTION ( "ubsan 1" ) {
		qs = {
			"execute debug::Random @ int",
			"execute debug::UndefinedBehaviour",
		};
	}

	SECTION ( "ubsan 2" ) {
		qs = {
			"execute debug::Random @ int",
			"execute debug::Segfault",
		};
	}

	TimeoutAqlTest ( 1s, qs );
}

TEST_CASE ( "Failed Test", "[integration][dummy][!shouldfail]" ) {
	ext::vector < std::string > qs = {
		"quit compare::PrimitiveCompare 1 2",
	};

	TimeoutAqlTest ( 1s, qs );
}

TEST_CASE ( "Timeout Test", "[integration][dummy][!shouldfail]" ) {
	ext::vector < std::string > qs = {
		"execute \"generated some output\"",
		"execute \"generated some output for the second time\"",
	};

	TimeoutAqlTest ( 1us, qs, true );
}
