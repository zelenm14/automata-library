/*
 * InputFileRegistry.cpp
 *
 *  Created on: 28. 8. 2017
 *	  Author: Jan Travnicek
 */

#include <registry/InputFileRegistry.hpp>
#include <abstraction/XmlComposerAbstraction.hpp>

namespace abstraction {

ext::map < std::string, std::unique_ptr < InputFileRegistry::Entry > > & InputFileRegistry::getEntries ( ) {
	static ext::map < std::string, std::unique_ptr < Entry > > inputFileHandlers;
	return inputFileHandlers;
}

void InputFileRegistry::registerInputFileHandler ( const std::string & fileType, std::shared_ptr < abstraction::OperationAbstraction > ( * callback ) ( const std::string & type, const ext::vector < std::string > & templateParams ) ) {
	auto iter = getEntries ( ).insert ( std::make_pair ( fileType, std::unique_ptr < Entry > ( new EntryImpl ( callback ) ) ) );
	if ( ! iter.second )
		throw std::invalid_argument ( "Entry " + iter.first->first + " already registered." );
}

void InputFileRegistry::unregisterInputFileHandler ( const std::string & fileType ) {
	if ( getEntries ( ).erase ( fileType ) == 0u )
		throw std::invalid_argument ( "Entry " + fileType + " not registered." );
}

std::shared_ptr < abstraction::OperationAbstraction > InputFileRegistry::getAbstraction ( const std::string & fileType, const std::string & type, const ext::vector < std::string > & templateParams ) {
	auto res = getEntries ( ).find ( fileType );
	if ( res == getEntries ( ).end ( ) )
		throw exception::CommonException ( "Entry " + fileType + " not available." );

	return res->second->getAbstraction ( type, templateParams );
}

std::shared_ptr < abstraction::OperationAbstraction > InputFileRegistry::EntryImpl::getAbstraction ( const std::string & type, const ext::vector < std::string > & templateParams ) const {
	return m_callback ( type, templateParams );
}

} /* namespace abstraction */
