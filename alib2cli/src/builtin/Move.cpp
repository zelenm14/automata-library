/*
 * Move.cpp
 *
 *  Created on: 18. 12. 2019
 *	  Author: Jan Travnicek
 */

#include "Move.h"

#include <registration/AlgoRegistration.hpp>

namespace {

auto move = ext::Register < void > ( [ ] ( ) {
		abstraction::AlgorithmRegistry::registerRaw < cli::builtin::Move > ( cli::builtin::Move::move, ext::pair < std::string, abstraction::TypeQualifiers::TypeQualifierSet > { "auto", abstraction::TypeQualifiers::TypeQualifierSet::RREF }, ext::vector < ext::tuple < std::string, abstraction::TypeQualifiers::TypeQualifierSet, std::string > > { { "auto", abstraction::TypeQualifiers::TypeQualifierSet::LREF, "arg0" } } );
	}, [ ] ( ) {
		abstraction::AlgorithmRegistry::unregisterRaw < cli::builtin::Move > ( ext::vector < ext::pair < std::string, abstraction::TypeQualifiers::TypeQualifierSet > > { { "auto", abstraction::TypeQualifiers::TypeQualifierSet::LREF } } );
	} );

} /* namespace */
