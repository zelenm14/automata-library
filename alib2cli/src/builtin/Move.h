/*
 * Move.h
 *
 *  Created on: 18. 12. 2019
 *	  Author: Jan Travnicek
 */

#ifndef _MOVE_H_
#define _MOVE_H_

#include <abstraction/Value.hpp>
#include <vector>

namespace cli {

namespace builtin {

/**
 * Builtin move on abstraction values.
 *
 */
class Move {
public:
	static std::shared_ptr < abstraction::Value > move ( std::vector < std::shared_ptr < abstraction::Value > > params ) {
		if ( params [ 0 ]->isTemporary ( ) ) {
			abstraction::TypeQualifiers::TypeQualifierSet typeQualifiers = abstraction::TypeQualifiers::TypeQualifierSet::RREF;
			if ( params [ 0 ]->getTypeQualifiers ( ) && abstraction::TypeQualifiers::TypeQualifierSet::CONST )
				typeQualifiers = typeQualifiers | abstraction::TypeQualifiers::TypeQualifierSet::CONST;

			return params [ 0 ]->clone ( typeQualifiers, true );
		} else {
			abstraction::TypeQualifiers::TypeQualifierSet typeQualifiers = abstraction::TypeQualifiers::TypeQualifierSet::RREF;
			if ( params [ 0 ]->getTypeQualifiers ( ) && abstraction::TypeQualifiers::TypeQualifierSet::CONST )
				typeQualifiers = typeQualifiers | abstraction::TypeQualifiers::TypeQualifierSet::CONST;

			return std::make_shared < abstraction::ValueReference > ( params [ 0 ], typeQualifiers, true );
		}
	}
};

} /* namespace builtin */

} /* namespace cli */

#endif /* _MOVE_H_ */
