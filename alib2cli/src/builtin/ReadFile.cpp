/*
 * ReadFile.cpp
 *
 *  Created on: 21. 8. 2017
 *	  Author: Jan Travnicek
 */

#include "ReadFile.h"
#include <registration/AlgoRegistration.hpp>
#include <fstream>
#include <streambuf>
#include <global/GlobalData.h>

#include <exception/CommonException.h>

namespace cli {

namespace builtin {

std::string ReadFile::read ( const std::string & filename ) {
	if ( filename == "-" ) {
		common::Streams::in >> std::noskipws;
		return std::string ( ( std::istreambuf_iterator < char > ( common::Streams::in ) ), std::istreambuf_iterator < char > ( ) );
	} else {
		std::ifstream t ( filename );
		if ( ! t.is_open ( ) ) {
			throw exception::CommonException ( "File could not be opened." );
		}
		t >> std::noskipws;
		return std::string ( ( std::istreambuf_iterator < char > ( t ) ), std::istreambuf_iterator < char > ( ) );
	}
}

auto ReadFileString = registration::AbstractRegister < ReadFile, std::string, const std::string & > ( ReadFile::read, "filename" ).setDocumentation (
"Reads the content of a file into a string.\n\
\n\
@param filename the name of read file\n\
@return the content of the file" );

} /* namespace builtin */

} /* namespace cli */

