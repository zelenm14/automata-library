#ifndef _CLI_BINDED_ARG_H__H_
#define _CLI_BINDED_ARG_H__H_

#include <ast/Arg.h>
#include <alib/string>

namespace cli {

class BindedArg final : public Arg {
	std::string m_bind;

public:
	BindedArg ( std::string bind ) : m_bind ( std::move ( bind ) ) {
	}

	std::string eval ( Environment & environment ) const override {
		return environment.getBinding ( m_bind );
	}
};

} /* namespace cli */

#endif /* _CLI_BINDED_ARG_H__H_ */
