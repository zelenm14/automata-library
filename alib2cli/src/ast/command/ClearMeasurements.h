#ifndef _CLI_CLEAR_MEASUREMENTS_H_
#define _CLI_CLEAR_MEASUREMENTS_H_

#include <ast/Command.h>
#include <environment/Environment.h>

#include <alib/measure>

namespace cli {

class ClearMeasurements : public Command {
public:
	CommandResult run ( Environment & ) const override {
		measurements::reset ( );
		return CommandResult::OK;
	}
};

} /* namespace cli */

#endif /* _CLI_CLEAR_MEASUREMENTS_H_ */
