#ifndef _CLI_CONTINUE_COMMAND_H_
#define _CLI_CONTINUE_COMMAND_H_

#include <ast/Command.h>
#include <environment/Environment.h>

namespace cli {

class ContinueCommand : public Command {
public:
	CommandResult run ( Environment & /* environment */ ) const override {
		return CommandResult::CONTINUE;
	}
};

} /* namespace cli */

#endif /* _CLI_CONTINUE_COMMAND_H_ */
