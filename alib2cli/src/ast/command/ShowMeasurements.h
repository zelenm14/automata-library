#ifndef _CLI_SHOW_MEASUREMENTS_H_
#define _CLI_SHOW_MEASUREMENTS_H_

#include <ast/Command.h>
#include <environment/Environment.h>

#include <alib/measure>

namespace cli {

class ShowMeasurements : public Command {
	measurements::MeasurementFormat m_format;

public:
	ShowMeasurements ( measurements::MeasurementFormat format ) : m_format ( format ) {
	}

	CommandResult run ( Environment & ) const override {
		common::Streams::out << m_format << measurements::results ( ) << std::endl;
		return CommandResult::OK;
	}
};

} /* namespace cli */

#endif /* _CLI_SHOW_MEASUREMENTS_H_ */
