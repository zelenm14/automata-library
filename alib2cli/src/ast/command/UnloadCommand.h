#ifndef _CLI_UNLOAD_COMMAND_H_
#define _CLI_UNLOAD_COMMAND_H_

#include <ast/Command.h>
#include <environment/Environment.h>

#include <common/LibraryLoader.h>

namespace cli {

class UnloadCommand : public Command {
	std::string m_libraryName;

public:
	UnloadCommand ( std::string libraryName ) : m_libraryName ( std::move ( libraryName ) ) {
	}

	CommandResult run ( Environment & ) const override {
		cli::LibraryLoader::unload ( m_libraryName );
		return CommandResult::OK;
	}
};

} /* namespace cli */

#endif /* _CLI_UNLOAD_COMMAND_H_ */
