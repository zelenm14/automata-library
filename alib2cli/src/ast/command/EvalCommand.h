#ifndef _CLI_EVAL_COMMAND_H_
#define _CLI_EVAL_COMMAND_H_

#include <ast/Command.h>
#include <environment/Environment.h>

#include <readline/StringLineInterface.h>
#include <iostream>

namespace cli {

class EvalCommand : public Command {
	std::string m_code;

public:
	EvalCommand ( std::string code ) : m_code ( std::move ( code ) ) {
	}

	CommandResult run ( Environment & environment ) const override {
		CommandResult state = environment.execute ( std::make_shared < cli::StringLineInterface > ( cli::StringLineInterface ( m_code ) ) );

		if ( state != cli::CommandResult::QUIT )
			state = cli::CommandResult::OK;

		return state;
	}
};

} /* namespace cli */

#endif /* _CLI_EVAL_COMMAND_H_ */
