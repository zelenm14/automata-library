#ifndef _CLI_DATA_TYPE_INTROSPECTION_COMMAND_H_
#define _CLI_DATA_TYPE_INTROSPECTION_COMMAND_H_

#include <ast/Command.h>
#include <environment/Environment.h>

#include <registry/XmlRegistry.h>

namespace cli {

class DataTypesIntrospectionCommand : public Command {
	std::unique_ptr < cli::Arg > m_param;

public:
	DataTypesIntrospectionCommand ( std::unique_ptr < cli::Arg > param ) : m_param ( std::move ( param ) ) {
	}

	void printTypes ( const ext::set < std::string > & types ) const {
		for ( const std::string & type : types )
			common::Streams::out << type << std::endl;
	}

	CommandResult run ( Environment & environment ) const override {
		std::string param;
		if ( m_param != nullptr )
			param = m_param->eval ( environment );

		if ( param.empty ( ) ) {
			printTypes ( abstraction::XmlRegistry::listDataTypes ( ) );
		} else if ( param.find ( "::", param.size ( ) - 2 ) != std::string::npos ) {
			printTypes ( abstraction::XmlRegistry::listDataTypeGroup ( param ) );
		} else {
			throw exception::CommonException ( "Invalid DataType introspection param" );
		}
		return CommandResult::OK;
	}
};

} /* namespace cli */

#endif /* _CLI_DATA_TYPE_INTROSPECTION_COMMAND_H_ */
