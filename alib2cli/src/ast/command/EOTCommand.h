#ifndef _CLI_EOT_COMMAND_H_
#define _CLI_EOT_COMMAND_H_

#include <ast/Command.h>
#include <environment/Environment.h>

namespace cli {

class EOTCommand : public Command {
public:
	CommandResult run ( Environment & /* environment */ ) const override {
		return CommandResult::EOT;
	}
};

} /* namespace cli */

#endif /* _CLI_EOT_COMMAND_H_ */
