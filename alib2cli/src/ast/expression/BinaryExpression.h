#ifndef _CLI_BINARY_EXPRESSION_H_
#define _CLI_BINARY_EXPRESSION_H_

#include <ast/Expression.h>

#include <common/Operators.hpp>
#include <common/EvalHelper.h>

namespace cli {

class BinaryExpression final : public Expression {
	abstraction::Operators::BinaryOperators m_operation;
	std::unique_ptr < Expression > m_left;
	std::unique_ptr < Expression > m_right;

public:
	BinaryExpression ( abstraction::Operators::BinaryOperators operation, std::unique_ptr < Expression > left, std::unique_ptr < Expression > right ) : m_operation ( operation ), m_left ( std::move ( left ) ), m_right ( std::move ( right ) ) {
	}

	std::shared_ptr < abstraction::Value > translateAndEval ( Environment & environment ) const override {
		ext::vector < std::shared_ptr < abstraction::Value > > params;
		params.push_back ( m_left->translateAndEval ( environment ) );
		params.push_back ( m_right->translateAndEval ( environment ) );

		abstraction::AlgorithmCategories::AlgorithmCategory category = abstraction::AlgorithmCategories::AlgorithmCategory::NONE;

		return abstraction::EvalHelper::evalOperator ( environment, m_operation, params, category );
	}

};

} /* namespace cli */

#endif /* _CLI_BINARY_EXPRESSION_H_ */
