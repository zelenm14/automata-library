#ifndef _CLI_PREFIX_EXPRESSION_H_
#define _CLI_PREFIX_EXPRESSION_H_

#include <ast/Expression.h>

namespace cli {

class PrefixExpression final : public Expression {
	abstraction::Operators::PrefixOperators m_operation;
	std::unique_ptr < Expression > m_expr;

public:
	PrefixExpression ( abstraction::Operators::PrefixOperators operation, std::unique_ptr < Expression > expr ) : m_operation ( operation ), m_expr ( std::move ( expr ) ) {
	}

	std::shared_ptr < abstraction::Value > translateAndEval ( Environment & environment ) const override {
		ext::vector < std::shared_ptr < abstraction::Value > > params;
		params.push_back ( m_expr->translateAndEval ( environment ) );

		abstraction::AlgorithmCategories::AlgorithmCategory category = abstraction::AlgorithmCategories::AlgorithmCategory::NONE;

		return abstraction::EvalHelper::evalOperator ( environment, m_operation, params, category );
	}

};

} /* namespace cli */

#endif /* _CLI_PREFIX_EXPRESSION_H_ */
