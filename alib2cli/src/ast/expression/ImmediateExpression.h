#ifndef _IMMEDIATE_EXPRESSION_H_
#define _IMMEDIATE_EXPRESSION_H_

#include <ast/Expression.h>

namespace cli {

template < class Type >
class ImmediateExpression final : public Expression {
	Type m_value;

public:
	ImmediateExpression ( Type value ) : m_value ( std::move ( value ) ) {
	}

	std::shared_ptr < abstraction::Value > translateAndEval ( Environment & environment ) const override {
		Type copy = m_value;
		std::shared_ptr < abstraction::ValueHolder < Type > > value = std::make_shared < abstraction::ValueHolder < Type > > ( std::move ( copy ), true );
		environment.holdTemporary ( value );
		return value;
	}

};

} /* namespace cli */

#endif /* _IMMEDIATE_EXPRESSION_H_ */
