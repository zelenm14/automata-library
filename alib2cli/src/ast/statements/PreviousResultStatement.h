#ifndef _CLI_PREVIOUS_RESULT_STATEMENT_H_
#define _CLI_PREVIOUS_RESULT_STATEMENT_H_

#include <ast/Statement.h>

namespace cli {

class PreviousResultStatement final : public Statement {

public:
	std::shared_ptr < abstraction::Value > translateAndEval ( const std::shared_ptr < abstraction::Value > & prev, Environment & ) const override {
		if ( prev == nullptr )
			throw std::invalid_argument ( "There is no previous result to use." );
		return prev;
	}

};

} /* namespace cli */

#endif /* _CLI_PREVIOUS_RESULT_PARAM_H_ */
