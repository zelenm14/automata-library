#ifndef _IMMEDIATE_STATEMENT_H_
#define _IMMEDIATE_STATEMENT_H_

#include <ast/Statement.h>

namespace cli {

template < class Type >
class ImmediateStatement final : public Statement {
	std::shared_ptr < abstraction::ValueHolder < Type > > m_value;

public:
	ImmediateStatement ( Type value ) : m_value ( std::make_shared < abstraction::ValueHolder < Type > > ( std::move ( value ), true ) ) {
	}

	std::shared_ptr < abstraction::Value > translateAndEval ( const std::shared_ptr < abstraction::Value > &, Environment & ) const override {
		return m_value;
	}

};

} /* namespace cli */

#endif /* _IMMEDIATE_STATEMENT_H_ */
