#ifndef _CLI_STATEMENT_LIST_H_
#define _CLI_STATEMENT_LIST_H_

#include <ast/Statement.h>

namespace cli {

class StatementList final : public Statement {
	ext::vector < std::shared_ptr < Statement > > m_statements;

public:
	StatementList ( ext::vector < std::shared_ptr < Statement > > statements ) : m_statements ( std::move ( statements ) ) {
	}

	std::shared_ptr < abstraction::Value > translateAndEval ( const std::shared_ptr < abstraction::Value > &, Environment & environment ) const override {
		if ( m_statements.empty ( ) )
			throw std::invalid_argument ( "Statement list can't be empty" );

		std::shared_ptr < abstraction::Value > next = m_statements [ 0 ]->translateAndEval ( nullptr, environment );
		for ( size_t i = 1; i < m_statements.size ( ); ++ i )
			next = m_statements [ i ]->translateAndEval ( next, environment );

		return next;
	}

	void append ( std::shared_ptr < Statement > statement ) {
		m_statements.emplace_back ( std::move ( statement ) );
	}

};

} /* namespace cli */

#endif /* _CLI_STATEMENT_LIST_H_ */
