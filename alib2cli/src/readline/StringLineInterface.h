/*
 * StringLineInterface.h
 *
 *  Created on: 20. 3. 2017
 *	  Author: Jan Travnicek
 */

#ifndef _STRING_LINE_INTERFACE_H_
#define _STRING_LINE_INTERFACE_H_

#include <string>
#include <utility>

#include <readline/LineInterface.h>

namespace cli {

class StringLineInterface final : public cli::LineInterface {
	std::string m_string;

	bool readline ( std::string & line, bool ) override {
		if ( m_string == "" )
			return false;

		line = std::exchange ( m_string, "" );
		return true;
	}

public:
	StringLineInterface ( std::string string ) : m_string ( std::move ( string ) ) {
	}
};

} // namespace cli

#endif /* _STRING_LINE_INTERFACE_H_ */
