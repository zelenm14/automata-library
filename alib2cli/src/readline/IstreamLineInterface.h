/*
 * IstreamLineInterface.h
 *
 *  Created on: 20. 3. 2017
 *	  Author: Jan Travnicek
 */

#ifndef _ISTREAM_LINE_INTERFACE_H_
#define _ISTREAM_LINE_INTERFACE_H_

#include <string>

#include <readline/LineInterface.h>

namespace cli {

/**
 * Stream line reader serves as an adaptor to read from arbitrary stream.
 *
 * \tparam Stream the type of the stream to read from. It can be either reference or value.
 */
template < class Stream >
class IstreamLineInterface final : public cli::LineInterface {
	Stream m_is;

	bool readline ( std::string & line, bool ) override {
		return ( bool ) std::getline ( m_is, line );
	}

public:
	IstreamLineInterface ( Stream ifs ) : m_is ( std::forward < Stream > ( ifs ) ) {
	}
};

} // namespace cli

#endif /* _ISTREAM_LINE_INTERFACE_H_ */
