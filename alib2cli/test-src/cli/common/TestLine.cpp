#include "TestLine.hpp"

#include <lexer/Lexer.h>
#include <parser/Parser.h>

#include <readline/StringLineInterface.h>

void testLine ( std::string line, cli::Environment & environment ) {
	cli::Parser ( cli::Lexer ( cli::CharSequence ( cli::StringLineInterface ( line ) ) ) ).parse ( )->run ( environment );
}
