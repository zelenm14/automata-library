/*
 * DataType.cpp
 *
 *  Created on: Dec 7, 2017
 *      Author: Jan Travnicek
 */

#include <alib/typeinfo>
#include <registration/ValuePrinterRegistration.hpp>
#include "DataType.h"

namespace {

// registration of a ValuePrinter. This "printer" is used for instance in AQL when the instance is returned
auto valuePrinter = registration::ValuePrinterRegister < example::DataType > ( );

}
