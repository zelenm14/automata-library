/*
 * DataType.h
 *
 *  Created on: Dec 7, 2017
 *      Author: Jan Travnicek
 */

#ifndef DATATYPE_H_
#define DATATYPE_H_

#include <ostream>

namespace example {

/**
 * Example of a simple datatype
 */
class DataType {
	int m_a;

public:
	DataType ( int a ) : m_a ( a ) {
	}

	int getA ( ) const {
		return m_a;
	}

	/* For ValuePrinter (see cpp) */
	friend std::ostream & operator << ( std::ostream & out, const DataType & type ) {
		return out << "(DataType " << type.getA ( ) << ")";
	}
};

} /* namespace example */

#endif /* DATATYPE_H_ */
