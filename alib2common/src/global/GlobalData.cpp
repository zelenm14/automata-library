/*
 * GlobalData.cpp
 *
 *  Created on: 9. 2. 2014
 *	  Author: Jan Travnicek
 */

#include "GlobalData.h"
#include <cstdlib>

#include <alib/iostream>
#include <alib/string>

namespace common {

bool GlobalData::verbose = false;

bool GlobalData::measure = false;

bool GlobalData::optimizeXml = true;

int GlobalData::argc = 0;

char * * GlobalData::argv = nullptr;

ext::reference_wrapper < std::istream > Streams::in = std::cin;
ext::reference_wrapper < std::ostream > Streams::out = std::cout;
ext::reference_wrapper < std::ostream > Streams::err = std::cerr;
ext::reference_wrapper < std::ostream > Streams::log = std::clog;
ext::reference_wrapper < std::ostream > Streams::measure = ext::cmeasure;

} /* common */

namespace ext {

std::ostream & operator << ( ext::reference_wrapper < std::ostream > & os, std::ostream & ( * func ) ( std::ostream & ) ) {
	os.get () << func;
	return os;
}

std::ostream & operator << ( ext::reference_wrapper < std::ostream > & os, std::ios_base & ( * func ) ( std::ios_base & ) ) {
	os.get () << func;
	return os;
}

std::istream & operator >> ( ext::reference_wrapper < std::istream > & is, std::istream & ( * func ) ( std::istream & ) ) {
	is.get () >> func;
	return is;
}

std::istream & operator >> ( ext::reference_wrapper < std::istream > & is, std::ios_base & ( * func ) ( std::ios_base & ) ) {
	is.get () >> func;
	return is;
}

} /* namespace ext */
