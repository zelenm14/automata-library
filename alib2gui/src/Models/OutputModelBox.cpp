#include <Models/OutputModelBox.hpp>

#include <cassert>

OutputModelBox::OutputModelBox()
    : ModelBox(ModelType::Output, 1)
{}

std::shared_ptr<abstraction::Value> OutputModelBox::evaluate() {
    assert(this->inputs.size() == 1);
    if (!this->inputs[0])
        return nullptr;
    this->result = inputs[0]->getCachedResultOrEvaluate();
    return this->result;
}

std::string OutputModelBox::getName() const {
    return "OUTPUT";
}
