#include <Models/InputModelBox.hpp>

InputModelBox::InputModelBox()
    : ModelBox(ModelType::Input, 0)
{}

std::shared_ptr<abstraction::Value> InputModelBox::evaluate() {
    return this->result;
}

std::string InputModelBox::getName() const {
    return "INPUT";
}
