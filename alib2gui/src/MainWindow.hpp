#pragma once
#include <memory>

#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QMainWindow>
#include <QtCore/QStringListModel>

#include <Algorithm/Algorithm.hpp>
#include <Graphics/GraphicsScene.hpp>

namespace Ui {
    class MainWindow;
}

class GraphicsBox;

class MainWindow : public QMainWindow
{
    Q_OBJECT
    friend class GraphicsView;

public:
    explicit MainWindow();
    ~MainWindow ( ) override;

    static MainWindow* getInstance();

private:
    void clearScene();

    void setupMenu();

    void displayError(const QString& text) const;

    const std::string& getCursorData() const { return this->cursorData; };
    void setCursorData(const std::string& p_data);

private slots:
    void on_RunBtn_clicked();
    void on_actionOpen_triggered();
    void on_actionSave_triggered();
    void on_actionClear_triggered();

public:
    static const std::string ADD_INPUT_CURSOR_DATA;

private:
    std::unique_ptr<Ui::MainWindow> ui;
    std::unique_ptr<GraphicsScene> scene;
    GraphicsBox* outputBox = nullptr;
    std::string cursorData;

    static MainWindow* instance;
};


