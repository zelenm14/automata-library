#pragma once
#include <Graphics/Connection/ConnectionBox.hpp>

class InputConnectionBox : public ConnectionBox {
    friend class Connection;

public:
    explicit InputConnectionBox(GraphicsBox* parent, size_t p_slot);

    void setConnection(Connection* p_connection);
    const Connection* getConnection() const { return this->connection; }
    size_t getSlot() const { return this->slot; }

private:
    void on_Disconnect() override;

protected:
    Connection* connection = nullptr;
    size_t slot;
};


