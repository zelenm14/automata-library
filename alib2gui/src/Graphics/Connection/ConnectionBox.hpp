#pragma once
#include <memory>

#include <QtWidgets/QGraphicsRectItem>

#include <Graphics/Connection/Connection.hpp>

class InputConnectionBox;
class GraphicsBox;
class OutputConnectionBox;

class ConnectionBox : public QObject, public QGraphicsRectItem {
    Q_OBJECT

public:
    enum class Type
    {
        Input,
        Output
    };

    static const QColor defaultColor;

    Type getType() const { return this->type; }
    GraphicsBox* getParent() const;

protected:
    ConnectionBox(GraphicsBox* parent, Type p_type);

    void mousePressEvent(QGraphicsSceneMouseEvent* event) override;
    void mouseMoveEvent(QGraphicsSceneMouseEvent* event) override;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent* event) override;

    void contextMenuEvent(QGraphicsSceneContextMenuEvent* event) override;

private:
    QGraphicsLineItem* tempLine = nullptr;
    Type type;

private slots:
    virtual void on_Disconnect() = 0;
};


