#pragma once

#include <QtWidgets/QGraphicsItem>

class ConnectionBox;
class InputConnectionBox;
class OutputConnectionBox;

class Connection : public QGraphicsItem {
public:
    Connection(OutputConnectionBox* origin, InputConnectionBox* target);

    QRectF boundingRect() const override;

    void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) override;

    void destroy();

    const OutputConnectionBox* getOriginConnectionBox() const { return this->originConnectionBox; }
    const InputConnectionBox* getTargetConnectionBox() const { return this->targetConnectionBox; }

private:
    void drawDirectConnection(QPainter* painter, const QPointF& originPoint, const QPointF& targetPoint);
    void drawAroundConnection(QPainter* painter, const QPointF& originPoint, const QPointF& targetPoint);
    void recalculateBoundingRect(const QPointF& a, const QPointF& b);

    QRectF boundRect;

    OutputConnectionBox* originConnectionBox;
    InputConnectionBox* targetConnectionBox;
};


