#pragma once
#include <memory>

#include <QDialog>

#include <abstraction/Value.hpp>

#include <ui_OutputDialog.h>

enum class TabType {
    Text,
    XML,
    DOT,
    Image
};

class OutputDialog : public QDialog
{
Q_OBJECT

public:
    explicit OutputDialog(std::shared_ptr<abstraction::Value> p_object);

private slots:
    void on_saveBtn_clicked();

private:
    void hideTab(QWidget* tab);

    TabType getCurrentTabType() const;
    std::pair<QString, QString> getCurrentTabFileFilter() const;

    std::unique_ptr<Ui::OutputDialog> ui;
    std::shared_ptr<abstraction::Value> object;
};


