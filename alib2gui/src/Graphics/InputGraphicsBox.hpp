#pragma once
#include <Graphics/GraphicsBox.hpp>

#include <Models/InputModelBox.hpp>

class InputGraphicsBox : public GraphicsBox {
    Q_OBJECT
public:
    InputGraphicsBox(std::unique_ptr<InputModelBox> p_modelBox, const QPointF& p_pos);

    void updateColor();

protected:
    void contextMenuEvent(QGraphicsSceneContextMenuEvent* event) override;

private slots:
    void on_SetInput();
};


