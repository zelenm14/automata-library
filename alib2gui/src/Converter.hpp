#pragma once

#include <tuple>
#include <optional>

#include <QString>
#include <QImage>

#include <abstraction/Value.hpp>

namespace Converter {
    std::optional<QString> toString(const std::shared_ptr<abstraction::Value>& data);
    std::optional<QString> toDOT(const std::shared_ptr<abstraction::Value>& data);
    std::optional<QString> toXML(const std::shared_ptr<abstraction::Value>& data, bool indent = true);
    std::optional<QImage> toPNG(const std::shared_ptr<abstraction::Value>& data);

    std::shared_ptr<abstraction::Value> tryParse(const QString& input);
    std::shared_ptr<abstraction::Value> parseXML(const QString& xml);
    std::shared_ptr<abstraction::Value> parseText(const QString& txt);

    void saveToImage(const std::shared_ptr<abstraction::Value>& data, const QString& filename);
}


