#pragma once
#include <QImage>

namespace GraphvizIntegrator
{
    enum PictureFormat
    {
        PNG,
        SVG
    };

    QImage createImage(const QString& dotData , PictureFormat format);
    bool createImageFile(const QString& dotData, QString filename , PictureFormat format);

    PictureFormat formatFromFilename(const QString& format);
}


