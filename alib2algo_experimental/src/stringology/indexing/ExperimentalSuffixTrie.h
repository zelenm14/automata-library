/*
 * ExperimentalSuffixTrie.h
 *
 *  Created on: 1. 11. 2014
 *      Author: Tomas Pecka
 */

#ifndef EXPERIMENTAL_SUFFIX_TRIE_H_
#define EXPERIMENTAL_SUFFIX_TRIE_H_

#include <indexes/stringology/SuffixTrieTerminatingSymbol.h>
#include <string/LinearStringTerminatingSymbol.h>

namespace stringology {

namespace indexing {

/**
 * Constructs suffix trie for given string.
 *
 * Source: Lectures MI-EVY (CTU in Prague), Year 2014, Lecture 3, slide 4
 */

class ExperimentalSuffixTrie {
public:
	/**
	 * Creates suffix trie
	 * @param string string to construct suffix trie for
	 * @return automaton
	 */
	static indexes::SuffixTrieTerminatingSymbol construct ( const string::LinearStringTerminatingSymbol & w );

};

} /* namespace indexing */

} /* namespace stringology */

#endif /* EXPERIMENTAL_SUFFIX_TRIE_H_ */
