/*
 * ExperimentalSuffixTrie.cpp
 *
 *  Created on: 1. 11. 2014
 *      Author: Tomas Pecka
 */

#include "ExperimentalSuffixTrie.h"

#include <string/LinearStringTerminatingSymbol.h>
#include <registration/AlgoRegistration.hpp>

namespace stringology {

namespace indexing {

indexes::SuffixTrieTerminatingSymbol ExperimentalSuffixTrie::construct ( const string::LinearStringTerminatingSymbol & w ) {
	indexes::SuffixTrieTerminatingSymbol res ( w.getAlphabet ( ), w.getTerminatingSymbol ( ) );

	for ( unsigned int i = 0; i < w.getContent ( ).size ( ); i++ ) {
		unsigned int k = i;
		indexes::SuffixTrieNodeTerminatingSymbol * n = & res.getRoot ( );

		 // inlined slow_find_one from MI-EVY lectures
		while ( k < w.getContent ( ).size ( ) && n->hasChild ( w.getContent ( )[k] ) )
			n = & n->getChild ( w.getContent ( )[k++] );

		for ( ; k < w.getContent ( ).size ( ); k++ )
			n = & n->addChild ( w.getContent ( )[k], indexes::SuffixTrieNodeTerminatingSymbol ( { } ) );
	}

	return res;
}

} /* namespace indexing */

} /* namespace stringology */

namespace {

auto SuffixTrieTerminatingSymbolLinearStringTerminatingSymbol = registration::AbstractRegister < stringology::indexing::ExperimentalSuffixTrie, indexes::SuffixTrieTerminatingSymbol, const string::LinearStringTerminatingSymbol & > ( stringology::indexing::ExperimentalSuffixTrie::construct );

} /* namespace */
