/*
 * LRParser.h
 *
 *  Created on: 30. 4. 2015
 *	  Author: Martin Kocicka
 */

#ifndef LR_PARSER_H_
#define LR_PARSER_H_

#include <grammar/ContextFree/CFG.h>
#include <grammar/parsing/LRParserTypes.h>

#include <alib/vector>

namespace grammar {

namespace parsing {

class LRParser {
public:
	static DefaultSymbolType getEndOfInputSymbol ( const grammar::CFG < > & originalGrammar );

	static grammar::CFG < > getAugmentedGrammar ( grammar::CFG < > originalGrammar );

	static bool parse ( const LRActionTable & actionTable, const LRGotoTable & gotoTable, const LR0Items & initialState, const ext::vector < DefaultSymbolType > & input );
};

} /* namespace parsing */

} /* namespace grammar */

#endif /* LR_PARSER_H_ */
