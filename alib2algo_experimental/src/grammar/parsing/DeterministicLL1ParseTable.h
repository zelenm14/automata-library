/*
 * DeterministicLL1ParseTable.h
 *
 *  Created on: 9. 6. 2015
 *	  Author: Jan Travnicek
 */

#ifndef DETERMINISTIC_LL_1_PARSE_TABLE_H_
#define DETERMINISTIC_LL_1_PARSE_TABLE_H_

#include <alib/vector>
#include <alib/set>
#include <alib/map>

#include <common/DefaultSymbolType.h>

namespace grammar {

namespace parsing {

class DeterministicLL1ParseTable {
public:
	static ext::map < ext::pair < ext::vector < DefaultSymbolType >, DefaultSymbolType >, ext::vector < DefaultSymbolType > > parseTable ( const ext::map < ext::pair < ext::vector < DefaultSymbolType >, DefaultSymbolType >, ext::set < ext::vector < DefaultSymbolType > > > & parseTable );
};

} /* namespace parsing */

} /* namespace grammar */

#endif /* DETERMINISTIC_LL_1_PARSE_TABLE_H_ */
