/*
 * CornerSubstitution.h
 *
 *  Created on: 9. 6. 2015
 *	  Author: Jan Travnicek
 */

#ifndef CORNER_SUBSTITUTION_H_
#define CORNER_SUBSTITUTION_H_

#include <grammar/ContextFree/CFG.h>

namespace grammar::parsing {

class CornerSubstitution {
public:
	static void cornerSubstitution ( grammar::CFG < > & grammar, const DefaultSymbolType & terminal, const DefaultSymbolType & nonterminal );

};

} /* namespace grammar::parsing */

#endif /* CORNER_SUBSTITUTION_H_ */
