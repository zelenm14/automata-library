/*
 * ExtractRightContext.h
 *
 *  Created on: 9. 6. 2015
 *	  Author: Jan Travnicek
 */

#ifndef EXTRACT_RIGHT_CONTEXT_H_
#define EXTRACT_RIGHT_CONTEXT_H_

#include <grammar/ContextFree/CFG.h>
#include <alib/set>

namespace grammar {

namespace parsing {

class ExtractRightContext {
public:
	static void extractRightContext ( grammar::CFG < > & grammar, const DefaultSymbolType & terminal, const ext::set < DefaultSymbolType > & nonterminals );

};

} /* namespace parsing */

} /* namespace grammar */

#endif /* EXTRACT_RIGHT_CONTEXT_H_ */
