/*
 * HandleFirstFollowConflict.h
 *
 *  Created on: 9. 6. 2015
 *	  Author: Jan Travnicek
 */

#ifndef HANDLE_FIRST_FOLLOW_CONFLICT_H_
#define HANDLE_FIRST_FOLLOW_CONFLICT_H_

#include <grammar/ContextFree/CFG.h>
#include <alib/vector>
#include <alib/set>

namespace grammar {

namespace parsing {

class HandleFirstFollowConflict {
public:
	static void handleFirstFollowConflict ( grammar::CFG < > & grammar, const DefaultSymbolType & terminal, const DefaultSymbolType & nonterminal, const ext::set < ext::vector < ext::variant < DefaultSymbolType, DefaultSymbolType > > > & /* rhsds */ );
};

} /* namespace parsing */

} /* namespace grammar */

#endif /* HANDLE_FIRST_FOLLOW_CONFLICT_H_ */
