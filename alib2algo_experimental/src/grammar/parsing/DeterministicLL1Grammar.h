/*
 * DeterministicLL1Grammar.h
 *
 *  Created on: 9. 6. 2015
 *	  Author: Jan Travnicek
 */

#ifndef DETERMINISTIC_LL_1_GRAMMAR_H_
#define DETERMINISTIC_LL_1_GRAMMAR_H_

#include <grammar/ContextFree/CFG.h>

namespace grammar {

namespace parsing {

class DeterministicLL1Grammar {
public:
	static grammar::CFG < > convert ( const grammar::CFG < > & param );

};

} /* namespace parsing */

} /* namespace grammar */

#endif /* DETERMINISTIC_LL_1_GRAMMAR_H_ */
