/*
 * MinimizeGenerator.h
 *
 *  Created on: 6. 11. 2017
 *	  Author: Jan Travnicek
 */

#ifndef MINIMIZE_GENERATOR_H_
#define MINIMIZE_GENERATOR_H_

#include <automaton/FSM/DFA.h>

namespace automaton {

namespace generate {

class MinimizeGenerator {
public:
	static automaton::DFA < std::string, unsigned > generateMinimizeDFA ( size_t statesMinimal, size_t statesDuplicates, size_t statesUnreachable, size_t statesUseless, size_t alphabetSize, bool randomizedAlphabet, double density, size_t expectedSteps );
};

} /* namespace generate */

} /* namespace automaton */

#endif /* MINIMIZE_GENERATOR_H_ */
