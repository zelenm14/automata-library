/**
 * optional.hpp
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 * https://gist.github.com/tibordp/6909880
 *
 * Created on: May 16, 2019
 * Created: Jan Travnicek
 */

#ifndef __OPTIONAL_HPP_
#define __OPTIONAL_HPP_

#include <optional>
#include <sstream>

namespace ext {

template < typename T >
class optional : public std::optional < T > {
public:
	/**
	 * Inherit constructors of the standard optional
	 */
	using std::optional< T >::optional; // NOLINT(modernize-use-equals-default)

	/**
	 * Inherit operator = of the standard optional
	 */
	using std::optional< T >::operator =;
#ifndef __clang__

	/**
	 * Default constructor needed by g++ since it is not inherited
	 */
	optional ( ) = default;

	/**
	 * Copy constructor needed by g++ since it is not inherited
	 */
	optional ( const optional & other ) = default;

	/**
	 * Move constructor needed by g++ since it is not inherited
	 */
	optional ( optional && other ) = default;

	/**
	 * Copy operator = needed by g++ since it is not inherited
	 */
	optional & operator = ( optional && other ) = default;

	/**
	 * Move operator = needed by g++ since it is not inherited
	 */
	optional & operator = ( const optional & other ) = default;
#endif

	// Comparisons between optional values.
	template < typename U >
	constexpr bool operator == ( const optional < U > & __rhs ) const{
		const optional < T > & __lhs = * this;
		return static_cast < bool > ( __lhs ) == static_cast < bool > ( __rhs ) && ( ! __lhs || * __lhs == * __rhs);
	}

	template < std::three_way_comparable_with < T > U >
	constexpr std::compare_three_way_result_t < T, U > operator <=> ( const optional < U > & __y ) const {
		const optional < T > & __x = * this;
		return __x && __y ? *__x <=> *__y : static_cast < bool > ( __x ) <=> static_cast < bool > ( __y );
	}

	// Comparisons with nullopt.
	constexpr bool operator==( std::nullopt_t ) const noexcept {
		const optional < T > & __lhs = * this;
		return ! __lhs;
	}

	constexpr std::strong_ordering operator <=> ( std::nullopt_t ) const noexcept {
		const optional < T > & __x = * this;
		return static_cast < bool > ( __x ) <=> false;
	}

	// Comparisons with value type.
	template < typename U >
	constexpr bool operator == ( const U & __rhs ) const {
		const optional < T > & __lhs = * this;
		return __lhs && *__lhs == __rhs;
	}

	template < typename U >
	constexpr std::compare_three_way_result_t < T, U > operator <=> ( const U & __y ) const {
		const optional < T > & __x = * this;
		return static_cast < bool > ( __x ) ? *__x <=> __y : std::strong_ordering::less;
	}

};

/**
 * \brief
 * Operator to print the optional to the output stream.
 *
 * \param out the output stream
 * \param optional the optional to print
 *
 * \tparam T the type of value inside the optional
 *
 * \return the output stream from the \p out
 */
template< class T >
std::ostream & operator << ( std::ostream & out, const ext::optional < T > & optional ) {
	if ( ! optional )
		return out << "void";
	else
		return out << optional.value ( );
}


/**
 * \brief
 * Overload of to_string function.
 *
 * \param value the optional to be converted to string
 *
 * \tparam T the type of values inside the optional
 *
 * \return string representation
 */
template < class T >
std::string to_string ( const ext::optional < T > & value ) {
	std::stringstream ss;
	ss << value;
	return ss.str();
}

} /* namespace ext */

#endif /* __OPTIONAL_HPP_ */
