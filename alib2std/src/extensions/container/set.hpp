/*
 * set.hpp
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Created on: Apr 1, 2013
 * Author: Jan Travnicek
 */

#ifndef __SET_HPP_
#define __SET_HPP_

#include <set>
#include <ostream>
#include <sstream>
#include <string>

#include <extensions/range.hpp>

namespace ext {

/**
 * Class extending the set class from the standard library. Original reason is to allow printing of the container with overloaded operator <<.
 *
 * The class mimics the behavior of the set from the standatd library.
 *
 * \tparam T the type of keys inside the set
 * \tparam Cmp the comparator type used to order keys
 * \tparam Alloc the allocator of values of type T
 */
template < typename T, typename Cmp = std::less < >, typename Alloc = std::allocator < T > >
class set : public std::set < T, Cmp, Alloc > {
public:
	/**
	 * Inherit constructors of the standard set
	 */
	using std::set < T, Cmp, Alloc >::set; // NOLINT(modernize-use-equals-default)

	/**
	 * Inherit operator = of the standard set
	 */
	using std::set < T, Cmp, Alloc >::operator =;
#ifndef __clang__

	/**
	 * Default constructor needed by g++ since it is not inherited
	 */
	set ( ) = default;

	/**
	 * Copy constructor needed by g++ since it is not inherited
	 */
	set ( const set & other ) = default;

	/**
	 * Move constructor needed by g++ since it is not inherited
	 */
	set ( set && other ) = default;

	/**
	 * Copy operator = needed by g++ since it is not inherited
	 */
	set & operator = ( set && other ) = default;

	/**
	 * Move operator = needed by g++ since it is not inherited
	 */
	set & operator = ( const set & other ) = default;
#endif
	/**
	 * Constructor from range of values.
	 *
	 * \tparam Iterator the type of range iterator
	 *
	 * \param range the source range
	 */
	template < class Iterator >
	explicit set ( const ext::iterator_range < Iterator > & range ) : set ( range.begin ( ), range.end ( ) ) {
	}

	/**
	 * \brief
	 * Inherited behavior of begin for non-const instance.
	 *
	 * \return iterator the first element of set
	 */
	auto begin ( ) & {
		return std::set < T, Cmp, Alloc >::begin ( );
	}

	/**
	 * \brief
	 * Inherited behavior of begin for const instance.
	 *
	 * \return const_iterator the first element of set
	 */
	auto begin ( ) const & {
		return std::set < T, Cmp, Alloc >::begin ( );
	}

	/**
	 * \brief
	 * New variant of begin for rvalues.
	 *
	 * \return move_iterator the first element of set
	 */
	auto begin ( ) && {
		return make_set_move_iterator ( this->begin ( ) );
	}

	/**
	 * \brief
	 * Inherited behavior of end for non-const instance.
	 *
	 * \return iterator to one after the last element of set
	 */
	auto end ( ) & {
		return std::set < T, Cmp, Alloc >::end ( );
	}

	/**
	 * \brief
	 * Inherited behavior of end for const instance.
	 *
	 * \return const_iterator to one after the last element of set
	 */
	auto end ( ) const & {
		return std::set < T, Cmp, Alloc >::end ( );
	}

	/**
	 * \brief
	 * New variant of end for rvalues.
	 *
	 * \return move_iterator to one after the last element of set
	 */
	auto end ( ) && {
		return make_set_move_iterator ( this->end ( ) );
	}

	/**
	 * \brief
	 * Make range of non-const begin to end iterators.
	 *
	 * \return full range over container values
	 */
	auto range ( ) & {
		auto endIter = end ( );
		auto beginIter = begin ( );
		return ext::iterator_range < decltype ( endIter ) > ( beginIter, endIter );
	}

	/**
	 * \brief
	 * Make range of non-const begin to end iterators.
	 *
	 * \return full range over container values
	 */
	auto range ( ) const & {
		auto endIter = end ( );
		auto beginIter = begin ( );
		return ext::iterator_range < decltype ( endIter ) > ( beginIter, endIter );
	}

	/**
	 * \brief
	 * Make range of move begin to end iterators.
	 *
	 * \return full range over container values
	 */
	auto range ( ) && {
		auto endIter = std::move ( * this ).end ( );
		auto beginIter = std::move ( * this ).begin ( );
		return ext::iterator_range < decltype ( endIter ) > ( beginIter, endIter );
	}

	/**
	 * \brief
	 * Make range of elements with key equal to the @p key.
	 *
	 * \tparam K the key used in the query
	 *
	 * \param key the value used in the query
	 *
	 * \return selected range of elements
	 */
	template < class K >
	auto equal_range ( K && key ) const & {
		auto res = std::set < T, Cmp, Alloc >::equal_range ( std::forward < K > ( key ) );
		return ext::iterator_range < decltype ( res.first ) > ( res.first, res.second );
	}

	/**
	 * \brief
	 * Make range of elements with key equal to the @p key.
	 *
	 * \tparam K the key used in the query
	 *
	 * \param key the value used in the query
	 *
	 * \return selected range of elements
	 */
	template < class K >
	auto equal_range ( K && key ) & {
		auto res = std::set < T, Cmp, Alloc >::equal_range ( std::forward < K > ( key ) );
		return ext::iterator_range < decltype ( res.first ) > ( res.first, res.second );
	}

	/**
	 * \brief
	 * Make range of elements with key equal to the @p key.
	 *
	 * \tparam K the key used in the query
	 *
	 * \param key the value used in the query
	 *
	 * \return selected range of elements
	 */
	template < class K >
	auto equal_range ( K && key ) && {
		auto res = std::set < T, Cmp, Alloc >::equal_range ( std::forward < K > ( key ) );
		return ext::make_iterator_range ( make_set_move_iterator < T > ( res.first ), make_set_move_iterator < T > ( res.second ) );
	}

};

/**
 * \brief
 * Operator to print the set to the output stream.
 *
 * \param out the output stream
 * \param set the set to print
 *
 * \tparam T the type of keys inside the set
 * \tparam R the type of values inside the set
 * \tparam Ts ... remaining unimportant template parameters of the set
 *
 * \return the output stream from the \p out
 */
template< class T, class ... Ts >
std::ostream& operator<<(std::ostream& out, const ext::set<T, Ts ...>& list) {
	out << "{";

	bool first = true;
	for(const T& item : list) {
		if(!first) out << ", ";
		first = false;
		out << item;
	}

	out << "}";
	return out;
}

/**
 * \brief
 * Overload of to_string function.
 *
 * \param value the set to be converted to string
 *
 * \tparam T the type of values inside the set
 * \tparam Ts ... remaining unimportant template parameters of the set
 *
 * \return string representation
 */
template < class T, class ... Ts >
std::string to_string ( const ext::set < T, Ts ... > & value ) {
	std::stringstream ss;
	ss << value;
	return ss.str();
}

/**
 * \brief
 * Implementation of union of two sets.
 *
 * \tparam T the type of values stored in unioned sets
 *
 * \param first the first set to union
 * \param second the second set to union
 *
 * \return set representing union
 */
template < class T >
ext::set < T > operator +( const ext::set < T > & first, const ext::set < T > & second ) {
	ext::set < T > res ( first );

	res.insert ( second.begin ( ), second.end ( ) );
	return res;
}

} /* namespace ext */

#endif /* __SET_HPP_ */
