/*
 * istream.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Created on: Apr 1, 2013
 * Author: Jan Travnicek
 */

#ifndef __ISTREAM_H_
#define __ISTREAM_H_

#include <istream>

#include <extensions/container/string.hpp>

namespace ext {

/**
 * \brief
 * Read into a constant string. The implementation tests whether exact same sequence represented as \p str is in the \p in stream. In case it is, it reads it. In case it is not the stream is not advanced and fail bit is set.
 *
 * \param in the input stream
 * \param str the string to test
 *
 * \return the in stream either with fail bit set if str is not in the stream or advanced by reading str.
 */
std::istream & operator >> ( std::istream & in, const ext::string & str );

} /* namespace ext */

#endif /* __ISTREAM_H_ */
