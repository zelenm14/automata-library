/*
 * istream.cpp
 *
 * Created on: Apr 1, 2013
 * Author: Jan Travnicek
 */

#include "istream.h"

namespace ext {

std::istream & oprr ( std::istream & in, const std::string & str, bool start ) {
	if ( str.empty ( ) ) return in;

	char c_str = str[0];
	char c_in  = in.peek ( );
	in.get ( );

	if ( c_in == EOF ) {
		in.clear ( std::ios::failbit );
		return in;
	}

	if ( in.good ( ) ) {
		if ( start && ( ( c_in == ' ' ) || ( c_in == '\n' ) || ( c_in == '\t' ) ) )
			oprr ( in, str, start );
		else if ( c_str == c_in )
			oprr ( in, str.substr ( 1 ), false );
		else
			in.clear ( std::ios::failbit );
	}

	if ( in.fail ( ) ) {
		in.clear ( );
		in.putback ( c_in );
		in.clear ( std::ios::failbit );
	}

	return in;
}

std::istream & operator >>( std::istream & in, const ext::string & str ) {
	return oprr ( in, str, true );
}

} /* namespace ext */
