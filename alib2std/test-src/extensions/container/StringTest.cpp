#include <catch2/catch.hpp>
#include <alib/string>

namespace {
	class A {
		public:
			explicit operator std::string ( ) const {
				return "";
			}
	};
}

TEST_CASE ( "String", "[unit][std][container]" ) {
	SECTION ( "ToString" ) {
		CHECK ( ext::to_string ( std::string ( "1" ) ) == "1" );
		CHECK ( ext::to_string ( 1 ) == "1" );
		CHECK ( ext::to_string ( 1ul ) == "1" );

		A a;
		CHECK ( ext::to_string ( a ) == "" );
	}

	SECTION ( "Explode" ) {
		{
			std::string one ( "a::bb::ccc" );
			ext::vector < std::string > oneExploded = ext::explode ( one, "::" );

			CAPTURE ( oneExploded );

			CHECK ( oneExploded.size ( ) == 3 );
			CHECK ( oneExploded [ 0 ] == "a" );
			CHECK ( oneExploded [ 1 ] == "bb" );
			CHECK ( oneExploded [ 2 ] == "ccc" );
		}
		{
			std::string one ( "a::bb::ccc::" );
			ext::vector < std::string > oneExploded = ext::explode ( one, "::" );

			CHECK ( oneExploded.size ( ) == 4 );
			CHECK ( oneExploded [ 0 ] == "a" );
			CHECK ( oneExploded [ 1 ] == "bb" );
			CHECK ( oneExploded [ 2 ] == "ccc" );
			CHECK ( oneExploded [ 3 ] == "" );
		}
		{
			std::string one ( "" );
			ext::vector < std::string > oneExploded = ext::explode ( one, "::" );

			CHECK ( oneExploded.size ( ) == 1 );
			CHECK ( oneExploded [ 0 ] == "" );
		}
		{
			std::string one ( "::aa" );
			ext::vector < std::string > oneExploded = ext::explode ( one, "::" );

			CAPTURE ( oneExploded );

			CHECK ( oneExploded.size ( ) == 2 );
			CHECK ( oneExploded [ 0 ] == "" );
			CHECK ( oneExploded [ 1 ] == "aa" );
		}
		{
			std::string one ( "::" );
			ext::vector < std::string > oneExploded = ext::explode ( one, "::" );

			CHECK ( oneExploded.size ( ) == 2 );
			CHECK ( oneExploded [ 0 ] == "" );
			CHECK ( oneExploded [ 1 ] == "" );
		}
	}
}
