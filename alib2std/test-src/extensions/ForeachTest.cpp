#include <catch2/catch.hpp>

#include <alib/list>
#include <alib/vector>
#include <alib/foreach>
#include <alib/set>

TEST_CASE ( "Foreach Test", "[unit][std][bits]" ) {
	SECTION ( "Foreach" ) {
		{
			ext::vector<int> vector1 {1, 2, 3};
			ext::list<int> list1 {2, 3, 4};

			int i = 1;
			for(const ext::tuple<const int&, const int&>& elements : ext::make_tuple_foreach(vector1, list1)) {
				CHECK(std::get<0>(elements) == i);
				CHECK(std::get<1>(elements) == i + 1);
				i++;
			}
		}
		{
			ext::vector<int> vector1 {1, 2, 3};
			ext::list<int> list1 {2, 3, 4};
			ext::set<int> set1 {3, 4, 5};

			int i = 1;
			for(const ext::tuple<const int&, const int&, const int&>& elements : ext::make_tuple_foreach(vector1, list1, set1)) {
				CHECK(std::get<0>(elements) == i);
				CHECK(std::get<1>(elements) == i + 1);
				CHECK(std::get<2>(elements) == i + 2);
				i++;
			}
		}
	}
}
