/*
 * IndexFromXMLParser.h
 *
 *  Created on: Nov 16, 2014
 *      Author: Jan Travnicek
 */

#ifndef INDEX_FROM_XML_PARSER_H_
#define INDEX_FROM_XML_PARSER_H_

#include <alib/set>
#include <alib/deque>

#include <sax/Token.h>
#include "../stringology/SuffixTrieNodeTerminatingSymbol.h"

namespace indexes {

/**
 * Parser used to get indexes from XML parsed into list of Tokens.
 */
class IndexFromXMLParser {
public:
	static SuffixTrieNodeTerminatingSymbol * parseSuffixTrieNodeTerminatingSymbol ( ext::deque < sax::Token >::iterator & input );
	static ext::set < DefaultSymbolType > parseAlphabet ( ext::deque < sax::Token >::iterator & input );
};

} /* namespace indexes */

#endif /* INDEX_FROM_XML_PARSER_H_ */
