/*
 * IndexToXMLComposer.cpp
 *
 *  Created on: Nov 16, 2014
 *      Author: Jan Travnicek
 */

#include "IndexToXMLComposer.h"
#include <core/xmlApi.hpp>

namespace indexes {

void IndexToXMLComposer::composeAlphabet ( ext::deque < sax::Token > & out, const ext::set < DefaultSymbolType > & symbols ) {
	out.emplace_back ( sax::Token ( "alphabet", sax::Token::TokenType::START_ELEMENT ) );

	for ( const auto & symbol : symbols )
		core::xmlApi < DefaultSymbolType >::compose ( out, symbol );

	out.emplace_back ( sax::Token ( "alphabet", sax::Token::TokenType::END_ELEMENT ) );
}

void IndexToXMLComposer::composeNode ( ext::deque < sax::Token > & out, const SuffixTrieNodeTerminatingSymbol & node ) {
	out.emplace_back ( sax::Token ( "node", sax::Token::TokenType::START_ELEMENT ) );

	for ( const auto & child : node.getChildren ( ) ) {
		out.emplace_back ( sax::Token ( "child", sax::Token::TokenType::START_ELEMENT ) );
		core::xmlApi < DefaultSymbolType >::compose ( out, child.first );
		composeNode ( out, * child.second );
		out.emplace_back ( sax::Token ( "child", sax::Token::TokenType::END_ELEMENT ) );
	}

	out.emplace_back ( sax::Token ( "node", sax::Token::TokenType::END_ELEMENT ) );
}

} /* namespace indexes */
