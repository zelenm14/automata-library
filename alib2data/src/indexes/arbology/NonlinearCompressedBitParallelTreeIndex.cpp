/*
 * NonlinearCompressedBitParallelTreeIndex.cpp
 *
 *  Created on: 22. 8. 2017
 *      Author: Jan Travnicek
 */

#include "NonlinearCompressedBitParallelTreeIndex.h"

#include <registration/ValuePrinterRegistration.hpp>

namespace {

auto valuePrinter = registration::ValuePrinterRegister < indexes::arbology::NonlinearCompressedBitParallelTreeIndex < > > ( );

} /* namespace */
