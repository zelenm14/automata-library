/*
 * PositionHeap.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: Nov 23, 2013
 *      Author: Jan Travnicek
 */

#ifndef _XML_POSITION_HEAP_H_
#define _XML_POSITION_HEAP_H_

#include <indexes/stringology/PositionHeap.h>

#include <sax/FromXMLParserHelper.h>
#include <core/xmlApi.hpp>

#include <primitive/xml/Unsigned.h>
#include <container/xml/ObjectsTrie.h>

#include <string/xml/LinearString.h>

namespace core {

template < class SymbolType >
struct xmlApi < indexes::stringology::PositionHeap < SymbolType > > {
	static indexes::stringology::PositionHeap < SymbolType > parse ( ext::deque < sax::Token >::iterator & input );
	static bool first ( const ext::deque < sax::Token >::const_iterator & input );
	static std::string xmlTagName ( );
	static void compose ( ext::deque < sax::Token > & output, const indexes::stringology::PositionHeap < SymbolType > & index );
};

template < class SymbolType >
indexes::stringology::PositionHeap < SymbolType > xmlApi < indexes::stringology::PositionHeap < SymbolType > >::parse ( ext::deque < sax::Token >::iterator & input ) {
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
	ext::trie < SymbolType, unsigned > root = core::xmlApi < ext::trie < SymbolType, unsigned > >::parse ( input );
	string::LinearString < SymbolType > string = core::xmlApi < string::LinearString < SymbolType > >::parse ( input );
	indexes::stringology::PositionHeap < SymbolType > res ( std::move ( root ), std::move ( string ) );

	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::END_ELEMENT, xmlTagName ( ) );
	return res;
}

template < class SymbolType >
bool xmlApi < indexes::stringology::PositionHeap < SymbolType > >::first ( const ext::deque < sax::Token >::const_iterator & input ) {
	return sax::FromXMLParserHelper::isToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
}

template < class SymbolType >
std::string xmlApi < indexes::stringology::PositionHeap < SymbolType > >::xmlTagName ( ) {
	return "PositionHeap";
}

template < class SymbolType >
void xmlApi < indexes::stringology::PositionHeap < SymbolType > >::compose ( ext::deque < sax::Token > & output, const indexes::stringology::PositionHeap < SymbolType > & index ) {
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::START_ELEMENT );
	core::xmlApi < ext::trie < SymbolType, unsigned > >::compose ( output, index.getRoot ( ) );
	core::xmlApi < string::LinearString < SymbolType > >::compose ( output, index.getString ( ) );
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::END_ELEMENT );
}

} /* namespace core */

#endif /* _XML_POSITION_HEAP_H_ */
