/*
 * BitParallelIndex.cpp
 *
 *  Created on: Jan 8, 2017
 *      Author: Jan Travnicek
 */

#include "BitParallelIndex.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < indexes::stringology::BitParallelIndex < > > ( );
auto xmlRead = registration::XmlReaderRegister < indexes::stringology::BitParallelIndex < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, indexes::stringology::BitParallelIndex < > > ( );

} /* namespace */
