/*
 * FactorOracleAutomaton.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: Jan 8, 2017
 *      Author: Jan Travnicek
 */

#ifndef _XML_FACTOR_ORACLE_AUTOMATON_H_
#define _XML_FACTOR_ORACLE_AUTOMATON_H_

#include <indexes/stringology/FactorOracleAutomaton.h>

#include <sax/FromXMLParserHelper.h>
#include <core/xmlApi.hpp>

#include <primitive/xml/Unsigned.h>

#include <automaton/xml/FSM/DFA.h>

namespace core {

template < class SymbolType >
struct xmlApi < indexes::stringology::FactorOracleAutomaton < SymbolType > > {
	static indexes::stringology::FactorOracleAutomaton < SymbolType > parse ( ext::deque < sax::Token >::iterator & input );
	static bool first ( const ext::deque < sax::Token >::const_iterator & input );
	static std::string xmlTagName ( );
	static void compose ( ext::deque < sax::Token > & output, const indexes::stringology::FactorOracleAutomaton < SymbolType > & index );
};

template < class SymbolType >
indexes::stringology::FactorOracleAutomaton < SymbolType > xmlApi < indexes::stringology::FactorOracleAutomaton < SymbolType > >::parse ( ext::deque < sax::Token >::iterator & input ) {
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
	automaton::DFA < SymbolType, unsigned > automaton = core::xmlApi < automaton::DFA < SymbolType, unsigned > >::parse ( input );
	indexes::stringology::FactorOracleAutomaton < SymbolType > res ( std::move ( automaton ) );

	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::END_ELEMENT, xmlTagName ( ) );
	return res;
}

template < class SymbolType >
bool xmlApi < indexes::stringology::FactorOracleAutomaton < SymbolType > >::first ( const ext::deque < sax::Token >::const_iterator & input ) {
	return sax::FromXMLParserHelper::isToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
}

template < class SymbolType >
std::string xmlApi < indexes::stringology::FactorOracleAutomaton < SymbolType > >::xmlTagName ( ) {
	return "FactorOracleAutomaton";
}

template < class SymbolType >
void xmlApi < indexes::stringology::FactorOracleAutomaton < SymbolType > >::compose ( ext::deque < sax::Token > & output, const indexes::stringology::FactorOracleAutomaton < SymbolType > & index ) {
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::START_ELEMENT );
	core::xmlApi < automaton::DFA < SymbolType, unsigned > >::compose ( output, index.getAutomaton ( ) );
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::END_ELEMENT );
}

} /* namespace core */

#endif /* _XML_FACTOR_ORACLE_AUTOMATON_H_ */
