/*
 * SuffixAutomaton.cpp
 *
 *  Created on: Jan 8, 2017
 *      Author: Jan Travnicek
 */

#include "SuffixAutomaton.h"

#include <registration/CastRegistration.hpp>
#include <registration/ValuePrinterRegistration.hpp>

namespace {

auto DFAFromSuffixAutomaton = registration::CastRegister < automaton::DFA < DefaultSymbolType, unsigned >, indexes::stringology::SuffixAutomaton < > > ( );

auto valuePrinter = registration::ValuePrinterRegister < indexes::stringology::SuffixAutomaton < > > ( );

} /* namespace */
