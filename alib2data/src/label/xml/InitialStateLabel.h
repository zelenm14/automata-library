/*
 * InitialStateLabel.h
 *
 *  Created on: Mar 26, 2013
 *      Author: Jan Travnicek
 */

#ifndef _XML_INITIAL_STATE_LABEL_H_
#define _XML_INITIAL_STATE_LABEL_H_

#include <label/InitialStateLabel.h>
#include <core/xmlApi.hpp>

namespace core {

template < >
struct xmlApi < label::InitialStateLabel > {
	static label::InitialStateLabel parse ( ext::deque < sax::Token >::iterator & input );
	static bool first ( const ext::deque < sax::Token >::const_iterator & input );
	static std::string xmlTagName ( );
	static void compose ( ext::deque < sax::Token > & output, const label::InitialStateLabel & data );
};

} /* namespace core */

#endif /* _XML_INITIAL_STATE_LABEL_H_ */
