/*
 * FinalStateLabel.cpp
 *
 *  Created on: Mar 26, 2013
 *      Author: Jan Travicek
 */

#include "FinalStateLabel.h"

#include <object/Object.h>

#include <registration/ValuePrinterRegistration.hpp>

namespace label {

FinalStateLabel::FinalStateLabel() = default;

std::ostream & operator << ( std::ostream & out, const FinalStateLabel & ) {
	return out << "(FinalStateLabel)";
}

FinalStateLabel::operator std::string ( ) const {
	return FinalStateLabel::instance < std::string > ( );
}

} /* namespace label */

namespace {

auto valuePrinter = registration::ValuePrinterRegister < label::FinalStateLabel > ( );

} /* namespace */
