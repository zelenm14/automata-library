/*
 * WildcardLinearString.h
 *
 * Created on: Apr 12, 2018
 * Author: Jan Travnicek
 */

#ifndef _XML_WILDCARD_LINEAR_STRING_H_
#define _XML_WILDCARD_LINEAR_STRING_H_

#include <string/WildcardLinearString.h>
#include <core/xmlApi.hpp>

#include <string/xml/common/StringFromXmlParserCommon.h>
#include <string/xml/common/StringToXmlComposerCommon.h>

namespace core {

template < typename SymbolType >
struct xmlApi < string::WildcardLinearString < SymbolType > > {
	static string::WildcardLinearString < SymbolType > parse ( ext::deque < sax::Token >::iterator & input );
	static bool first ( const ext::deque < sax::Token >::const_iterator & input );
	static std::string xmlTagName ( );
	static void compose ( ext::deque < sax::Token > & output, const string::WildcardLinearString < SymbolType > & input );
};

template < typename SymbolType >
string::WildcardLinearString < SymbolType > xmlApi < string::WildcardLinearString < SymbolType > >::parse ( ext::deque < sax::Token >::iterator & input ) {
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
	ext::set<SymbolType> alphabet = string::StringFromXmlParserCommon::parseAlphabet < SymbolType > ( input );
	ext::vector<SymbolType> content = string::StringFromXmlParserCommon::parseContent < SymbolType > ( input );
	SymbolType wildcard = string::StringFromXmlParserCommon::parseWildcard < SymbolType > ( input );
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::END_ELEMENT, xmlTagName ( ) );
	return string::WildcardLinearString < SymbolType > ( std::move ( alphabet ), std::move ( content ), std::move ( wildcard ) );
}

template < typename SymbolType >
bool xmlApi < string::WildcardLinearString < SymbolType > >::first ( const ext::deque < sax::Token >::const_iterator & input ) {
	return sax::FromXMLParserHelper::isToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
}

template < typename SymbolType >
std::string xmlApi < string::WildcardLinearString < SymbolType > >::xmlTagName ( ) {
	return "WildcardLinearString";
}

template < typename SymbolType >
void xmlApi < string::WildcardLinearString < SymbolType > >::compose ( ext::deque < sax::Token > & output, const string::WildcardLinearString < SymbolType > & input ) {
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::START_ELEMENT );
	string::StringToXmlComposerCommon::composeAlphabet ( output, input.getAlphabet ( ) );
	string::StringToXmlComposerCommon::composeContent ( output, input.getContent ( ) );
	string::StringToXmlComposerCommon::composeWildcard ( output, input.getWildcardSymbol ( ) );
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::END_ELEMENT );
}

} /* namespace core */

#endif /* _XML_WILDCARD_LINEAR_STRING_H_ */
