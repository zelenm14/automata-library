/*
 * String.h
 *
 *  Created on: Apr 10, 2013
 *      Author: Jan Travnicek
 */

#ifndef STRING_H_
#define STRING_H_

#include <alib/set>
#include <alib/vector>

#include <common/DefaultSymbolType.h>

#include "LinearString.h"

namespace string {

/**
 * Wrapper around strings.
 */
class String;

template < class SymbolType >
string::LinearString < SymbolType > stringFrom ( const SymbolType & symbol ) {
	return string::String { string::LinearString < SymbolType > { ext::vector < SymbolType > { symbol } } };
}

string::LinearString < char > stringFrom ( const std::string & string );

string::LinearString < char > stringFrom ( const char * string );

template < class SymbolType >
string::LinearString < SymbolType > stringFrom ( const ext::vector < SymbolType > & str ) {
	return string::LinearString < SymbolType > { str };
}


} /* namespace string */

#endif /* STRING_H_ */
