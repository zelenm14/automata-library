/*
 * FormalRegExpEmpty.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: Jan 30, 2014
 *      Author: Jan Travnicek
 */

#ifndef FORMAL_REG_EXP_EMPTY_H_
#define FORMAL_REG_EXP_EMPTY_H_

#include <sstream>

#include "FormalRegExpElement.h"

namespace regexp {

/**
 * \brief Represents the empty expression in the regular expression. The node can't have any children.
 *
 * The structure is derived from NullaryNode disallowing adding any child.
 *
 * The node can be visited by the FormalRegExpElement < SymbolType >::Visitor
 *
 * \tparam SymbolType used for the terminal alphabet
 */
template < class SymbolType >
class FormalRegExpEmpty : public ext::NullaryNode < FormalRegExpElement < SymbolType > > {
	/**
	 * @copydoc regexp::FormalRegExpElement < SymbolType >::accept ( ) const
	 */
	void accept ( typename FormalRegExpElement < SymbolType >::Visitor & visitor ) const override {
		visitor.visit ( * this );
	}

public:
	/**
	 * \brief Creates a new instance of the empty node.
	 */
	explicit FormalRegExpEmpty ( ) = default;

	/**
	 * @copydoc FormalRegExpElement::clone ( ) const &
	 */
	FormalRegExpEmpty < SymbolType > * clone ( ) const & override;

	/**
	 * @copydoc FormalRegExpElement::clone ( ) const &
	 */
	FormalRegExpEmpty < SymbolType > * clone ( ) && override;

	/**
	 * @copydoc FormalRegExpElement::clone ( ) const &
	 */
	ext::smart_ptr < UnboundedRegExpElement < SymbolType > > asUnbounded ( ) const override;

	/**
	 * @copydoc FormalRegExpElement::testSymbol() const
	 */
	bool testSymbol ( const SymbolType & symbol ) const override;

	/**
	 * @copydoc RegExpElement::computeMinimalAlphabet()
	 */
	void computeMinimalAlphabet ( ext::set < SymbolType > & alphabet ) const override;

	/**
	 * @copydoc FormalRegExpElement::checkAlphabet()
	 */
	bool checkAlphabet ( const ext::set < SymbolType > & alphabet ) const override;

	/**
	 * @copydoc FormalRegExpElement < SymbolType >::operator <=> ( const FormalRegExpElement < SymbolType > & other ) const;
	 */
	std::strong_ordering operator <=> ( const FormalRegExpElement < SymbolType > & other ) const override {
		if ( ext::type_index ( typeid ( * this ) ) == ext::type_index ( typeid ( other ) ) ) return * this <=> ( decltype ( * this ) ) other;

		return ext::type_index ( typeid ( * this ) ) <=> ext::type_index ( typeid ( other ) );
	}

	/**
	 * The three way comparison implementation
	 *
	 * \param other the other instance
	 *
	 * \returns the ordering between this object and the @p other.
	 */
	std::strong_ordering operator <=> ( const FormalRegExpEmpty < SymbolType > & ) const;

	/**
	 * @copydoc FormalRegExpElement < SymbolType >::operator == ( const FormalRegExpElement < SymbolType > & other ) const;
	 */
	bool operator == ( const FormalRegExpElement < SymbolType > & other ) const override {
		if ( ext::type_index ( typeid ( * this ) ) == ext::type_index ( typeid ( other ) ) ) return * this == ( decltype ( * this ) ) other;

		return false;
	}

	/**
	 * The equality comparison implementation.
	 *
	 * \param other the other object to compare with.
	 *
	 * \returns true if this and other objects are equal, false othervise
	 */
	bool operator == ( const FormalRegExpEmpty < SymbolType > & ) const;

	/**
	 * @copydoc base::CommonBase < FormalRegExpElement < SymbolType > >::operator >> ( std::ostream & )
	 */
	void operator >>( std::ostream & out ) const override;

	/**
	 * @copydoc base::CommonBase < FormalRegExpElement < SymbolType > >::operator std::string ( )
	 */
	explicit operator std::string ( ) const override;

	/**
	 * @copydoc regexp::FormalRegExpElement < SymbolType >::normalize ( ) &&
	 */
	ext::smart_ptr < FormalRegExpElement < DefaultSymbolType > > normalize ( ) && override {
		return ext::smart_ptr < FormalRegExpElement < DefaultSymbolType > > ( new FormalRegExpEmpty < DefaultSymbolType > ( ) );
	}
};

} /* namespace regexp */

#include "../unbounded/UnboundedRegExpEmpty.h"

namespace regexp {

template < class SymbolType >
FormalRegExpEmpty < SymbolType > * FormalRegExpEmpty < SymbolType >::clone ( ) const & {
	return new FormalRegExpEmpty ( * this );
}

template < class SymbolType >
FormalRegExpEmpty < SymbolType > * FormalRegExpEmpty < SymbolType >::clone ( ) && {
	return new FormalRegExpEmpty ( std::move ( * this ) );
}

template < class SymbolType >
ext::smart_ptr < UnboundedRegExpElement < SymbolType > > FormalRegExpEmpty < SymbolType >::asUnbounded ( ) const {
	return ext::smart_ptr < UnboundedRegExpElement < SymbolType > > ( new UnboundedRegExpEmpty < SymbolType > ( ) );
}

template < class SymbolType >
std::strong_ordering FormalRegExpEmpty < SymbolType >::operator <=> ( const FormalRegExpEmpty < SymbolType > & ) const {
	return std::strong_ordering::equal;
}

template < class SymbolType >
bool FormalRegExpEmpty < SymbolType >::operator == ( const FormalRegExpEmpty < SymbolType > & ) const {
	return true;
}

template < class SymbolType >
void FormalRegExpEmpty < SymbolType >::operator >>( std::ostream & out ) const {
	out << "(FormalRegExpEmpty)";
}

template < class SymbolType >
bool FormalRegExpEmpty < SymbolType >::testSymbol ( const SymbolType & ) const {
	return false;
}

template < class SymbolType >
void FormalRegExpEmpty < SymbolType >::computeMinimalAlphabet ( ext::set < SymbolType > & ) const {
}

template < class SymbolType >
bool FormalRegExpEmpty < SymbolType >::checkAlphabet ( const ext::set < SymbolType > & ) const {
	return true;
}

template < class SymbolType >
FormalRegExpEmpty < SymbolType >::operator std::string ( ) const {
	std::stringstream ss;
	ss << * this;
	return ss.str ( );
}

} /* namespace regexp */

extern template class regexp::FormalRegExpEmpty < DefaultSymbolType >;

#endif /* FORMAL_REG_EXP_EMPTY_H_ */
