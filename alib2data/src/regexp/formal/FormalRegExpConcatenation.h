/*
 * FormalRegExpConcatenation.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: Nov 27, 2013
 *      Author: Martin Zak
 */

#ifndef FORMAL_REG_EXP_CONCATENATION_H_
#define FORMAL_REG_EXP_CONCATENATION_H_

#include <sstream>
#include <alib/utility>

#include <exception/CommonException.h>

#include "FormalRegExpElement.h"

namespace regexp {

/**
 * \brief Represents the concatenation operator in the regular expression. The node must have exactly two children.
 *
 * The structure is derived from BinaryNode that provides the children accessors.
 *
 * The node can be visited by the FormalRegExpElement < SymbolType >::Visitor
 *
 * \tparam SymbolType used for the terminal alphabet
 */
template < class SymbolType >
class FormalRegExpConcatenation : public ext::BinaryNode < FormalRegExpElement < SymbolType > > {
	/**
	 * @copydoc regexp::FormalRegExpElement < SymbolType >::accept ( ) const
	 */
	void accept ( typename FormalRegExpElement < SymbolType >::Visitor & visitor ) const override {
		visitor.visit ( * this );
	}

public:
	/**
	 * \brief Creates a new instance of the concatenation node with explicit concatenated elements
	 *
	 * \param left the first concatenated element
	 * \param right the second concatenated element
	 */
	explicit FormalRegExpConcatenation ( FormalRegExpElement < SymbolType > && left, FormalRegExpElement < SymbolType > && right );

	/**
	 * \brief Creates a new instance of the concatenation node with explicit concatenated elements
	 *
	 * \param left the first concatenated element
	 * \param right the second concatenated element
	 */
	explicit FormalRegExpConcatenation ( const FormalRegExpElement < SymbolType > & left, const FormalRegExpElement < SymbolType > & right );

	/**
	 * @copydoc FormalRegExpElement::clone ( ) const &
	 */
	FormalRegExpConcatenation < SymbolType > * clone ( ) const & override;

	/**
	 * @copydoc FormalRegExpElement::clone ( ) const &
	 */
	FormalRegExpConcatenation < SymbolType > * clone ( ) && override;

	/**
	 * @copydoc FormalRegExpElement::clone ( ) const &
	 */
	ext::smart_ptr < UnboundedRegExpElement < SymbolType > > asUnbounded ( ) const override;

	/**
	 * @copydoc FormalRegExpElement::testSymbol() const
	 */
	bool testSymbol ( const SymbolType & symbol ) const override;

	/**
	 * @copydoc FormalRegExpElement::computeMinimalAlphabet()
	 */
	void computeMinimalAlphabet ( ext::set < SymbolType > & alphabet ) const override;

	/**
	 * @copydoc FormalRegExpElement::checkAlphabet()
	 */
	bool checkAlphabet ( const ext::set < SymbolType > & alphabet ) const override;

	/**
	 * Getter of the left alternated element
	 *
	 * \return left alternated element
	 */
	const FormalRegExpElement < SymbolType > & getLeftElement ( ) const;

	/**
	 * Getter of the right alternated element
	 *
	 * \return right alternated element
	 */
	const FormalRegExpElement < SymbolType > & getRightElement ( ) const;

	/**
	 * Getter of the left alternated element
	 *
	 * \return left alternated element
	 */
	FormalRegExpElement < SymbolType > & getLeftElement ( );

	/**
	 * Getter of the right alternated element
	 *
	 * \return right alternated element
	 */
	FormalRegExpElement < SymbolType > & getRightElement ( );

	/**
	 * Setter of the left alternated element
	 *
	 * \param element left alternated element
	 */
	void setLeftElement ( FormalRegExpElement < SymbolType > && element );

	/**
	 * Setter of the left alternated element
	 *
	 * \param element left alternated element
	 */
	void setLeftElement ( const FormalRegExpElement < SymbolType > & element );

	/**
	 * Setter of the right alternated element
	 *
	 * \param element right alternated element
	 */
	void setRightElement ( FormalRegExpElement < SymbolType > && element );

	/**
	 * Setter of the right alternated element
	 *
	 * \param element right alternated element
	 */
	void setRightElement ( const FormalRegExpElement < SymbolType > & element );

	/**
	 * @copydoc FormalRegExpElement < SymbolType >::operator <=> ( const FormalRegExpElement < SymbolType > & other ) const;
	 */
	std::strong_ordering operator <=> ( const FormalRegExpElement < SymbolType > & other ) const override {
		if ( ext::type_index ( typeid ( * this ) ) == ext::type_index ( typeid ( other ) ) ) return * this <=> ( decltype ( * this ) ) other;

		return ext::type_index ( typeid ( * this ) ) <=> ext::type_index ( typeid ( other ) );
	}

	/**
	 * The three way comparison implementation
	 *
	 * \param other the other instance
	 *
	 * \returns the ordering between this object and the @p other.
	 */
	std::strong_ordering operator <=> ( const FormalRegExpConcatenation < SymbolType > & ) const;

	/**
	 * @copydoc FormalRegExpElement < SymbolType >::operator == ( const FormalRegExpElement < SymbolType > & other ) const;
	 */
	bool operator == ( const FormalRegExpElement < SymbolType > & other ) const override {
		if ( ext::type_index ( typeid ( * this ) ) == ext::type_index ( typeid ( other ) ) ) return * this == ( decltype ( * this ) ) other;

		return false;
	}

	/**
	 * The equality comparison implementation.
	 *
	 * \param other the other object to compare with.
	 *
	 * \returns true if this and other objects are equal, false othervise
	 */
	bool operator == ( const FormalRegExpConcatenation < SymbolType > & ) const;

	/**
	 * @copydoc base::CommonBase < FormalRegExpElement < SymbolType > >::operator >> ( std::ostream & )
	 */
	void operator >>( std::ostream & out ) const override;

	/**
	 * @copydoc base::CommonBase < FormalRegExpElement < SymbolType > >::operator std::string ( )
	 */
	explicit operator std::string ( ) const override;

	/**
	 * @copydoc regexp::FormalRegExpElement < SymbolType >::normalize ( ) &&
	 */
	ext::smart_ptr < FormalRegExpElement < DefaultSymbolType > > normalize ( ) && override {
		return ext::smart_ptr < FormalRegExpElement < DefaultSymbolType > > ( new FormalRegExpConcatenation < DefaultSymbolType > ( std::move ( * std::move ( getLeftElement ( ) ).normalize ( ) ), std::move ( * std::move ( getRightElement ( ) ).normalize ( ) ) ) );
	}
};

} /* namespace regexp */

#include "../unbounded/UnboundedRegExpConcatenation.h"

namespace regexp {

template < class SymbolType >
FormalRegExpConcatenation < SymbolType >::FormalRegExpConcatenation ( FormalRegExpElement < SymbolType > && left, FormalRegExpElement < SymbolType > && right ) : ext::BinaryNode < FormalRegExpElement < SymbolType > > ( std::move ( left ), std::move ( right ) ) {
}

template < class SymbolType >
FormalRegExpConcatenation < SymbolType >::FormalRegExpConcatenation ( const FormalRegExpElement < SymbolType > & left, const FormalRegExpElement < SymbolType > & right ) : FormalRegExpConcatenation ( ext::move_copy ( left ), ext::move_copy ( right ) ) {
}

template < class SymbolType >
const FormalRegExpElement < SymbolType > & FormalRegExpConcatenation < SymbolType >::getLeftElement ( ) const {
	return this->getLeft ( );
}

template < class SymbolType >
const FormalRegExpElement < SymbolType > & FormalRegExpConcatenation < SymbolType >::getRightElement ( ) const {
	return this->getRight ( );
}

template < class SymbolType >
FormalRegExpElement < SymbolType > & FormalRegExpConcatenation < SymbolType >::getLeftElement ( ) {
	return this->getLeft ( );
}

template < class SymbolType >
FormalRegExpElement < SymbolType > & FormalRegExpConcatenation < SymbolType >::getRightElement ( ) {
	return this->getRight ( );
}

template < class SymbolType >
void FormalRegExpConcatenation < SymbolType >::setLeftElement ( FormalRegExpElement < SymbolType > && element ) {
	this->setLeft ( std::move ( element ) );
}

template < class SymbolType >
void FormalRegExpConcatenation < SymbolType >::setLeftElement ( const FormalRegExpElement < SymbolType > & element ) {
	setLeftElement ( ext::move_copy ( element ) );
}

template < class SymbolType >
void FormalRegExpConcatenation < SymbolType >::setRightElement ( FormalRegExpElement < SymbolType > && element ) {
	this->setRight ( std::move ( element ) );
}

template < class SymbolType >
void FormalRegExpConcatenation < SymbolType >::setRightElement ( const FormalRegExpElement < SymbolType > & element ) {
	setRightElement ( ext::move_copy ( element ) );
}

template < class SymbolType >
FormalRegExpConcatenation < SymbolType > * FormalRegExpConcatenation < SymbolType >::clone ( ) const & {
	return new FormalRegExpConcatenation ( * this );
}

template < class SymbolType >
FormalRegExpConcatenation < SymbolType > * FormalRegExpConcatenation < SymbolType >::clone ( ) && {
	return new FormalRegExpConcatenation ( std::move ( * this ) );
}

template < class SymbolType >
ext::smart_ptr < UnboundedRegExpElement < SymbolType > > FormalRegExpConcatenation < SymbolType >::asUnbounded ( ) const {
	UnboundedRegExpConcatenation < SymbolType > * res = new UnboundedRegExpConcatenation < SymbolType > ( );

	res->appendElement ( std::move ( * getLeftElement ( ).asUnbounded ( ) ) );
	res->appendElement ( std::move ( * getRightElement ( ).asUnbounded ( ) ) );

	return ext::smart_ptr < UnboundedRegExpElement < SymbolType > > ( res );
}

template < class SymbolType >
std::strong_ordering FormalRegExpConcatenation < SymbolType >::operator <=> ( const FormalRegExpConcatenation < SymbolType > & other ) const {
	return std::tie ( getLeftElement ( ), getRightElement ( ) ) <=> std::tie ( other.getLeftElement ( ), other.getRightElement ( ) );
}

template < class SymbolType >
bool FormalRegExpConcatenation < SymbolType >::operator == ( const FormalRegExpConcatenation < SymbolType > & other ) const {
	return std::tie ( getLeftElement ( ), getRightElement ( ) ) == std::tie ( other.getLeftElement ( ), other.getRightElement ( ) );
}

template < class SymbolType >
void FormalRegExpConcatenation < SymbolType >::operator >>( std::ostream & out ) const {
	out << "(FormalRegExpConcatenation";
	out << " " << getLeftElement ( );
	out << " " << getRightElement ( );
	out << ")";
}

template < class SymbolType >
bool FormalRegExpConcatenation < SymbolType >::testSymbol ( const SymbolType & symbol ) const {
	return getLeftElement ( ).testSymbol ( symbol ) && getRightElement ( ).testSymbol ( symbol );
}

template < class SymbolType >
void FormalRegExpConcatenation < SymbolType >::computeMinimalAlphabet ( ext::set < SymbolType > & alphabet ) const {
	getLeftElement ( ).computeMinimalAlphabet ( alphabet );
	getRightElement ( ).computeMinimalAlphabet ( alphabet );
}

template < class SymbolType >
bool FormalRegExpConcatenation < SymbolType >::checkAlphabet ( const ext::set < SymbolType > & alphabet ) const {
	return getLeftElement ( ).checkAlphabet ( alphabet ) && getRightElement ( ).checkAlphabet ( alphabet );
}

template < class SymbolType >
FormalRegExpConcatenation < SymbolType >::operator std::string ( ) const {
	std::stringstream ss;
	ss << * this;
	return ss.str ( );
}

} /* namespace regexp */

extern template class regexp::FormalRegExpConcatenation < DefaultSymbolType >;

#endif /* FORMAL_REG_EXP_CONCATENATION_H_ */
