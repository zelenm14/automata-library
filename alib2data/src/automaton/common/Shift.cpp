/*
 * Shift.cpp
 *
 *  Created on: Dec 8, 2013
 *      Author: Martin Zak
 */

#include "Shift.h"
#include "../AutomatonException.h"

namespace automaton {

std::string SHIFT_NAMES[] = {"LEFT", "RIGHT", "NONE" };

int shiftToInt(const automaton::Shift& shift) {
	switch(shift) {
	case automaton::Shift::LEFT:
		return 0;
	case automaton::Shift::RIGHT:
		return 1;
	case automaton::Shift::NONE:
		return 2;
	}
	throw AutomatonException("Invalid shift value");
}

automaton::Shift intToShift(int value) {
	switch(value) {
	case 0:
		return automaton::Shift::LEFT;
	case 1:
		return automaton::Shift::RIGHT;
	case 2:
		return automaton::Shift::NONE;
	}
	throw AutomatonException("Invalid shift value");
}

std::ostream& operator<<(std::ostream& out, const automaton::Shift&) {

	return out;
}

} /* namepsace automaton */
