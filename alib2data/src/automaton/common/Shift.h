/*
 * Shift.h
 *
 *  Created on: Oct 13, 2013
 *      Author: Martin Zak
 */

#ifndef SHIFT_H_
#define SHIFT_H_

#include <alib/string>

namespace automaton {

/**
 * Represent movement of the reading head in the Turing machine.
 */
enum class Shift {
	LEFT, RIGHT, NONE
};

int shiftToInt(const automaton::Shift& shift);
automaton::Shift intToShift(int value);

extern std::string SHIFT_NAMES[3];

std::ostream& operator<<(std::ostream& out, const automaton::Shift&);

} /* namespace automaton */

#endif /* SHIFT_H_ */
