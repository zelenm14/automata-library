/*
 * OneTapeDTM.cpp
 *
 *  Created on: Apr 24, 2013
 *      Author: Jan Travnicek
 */

#include "OneTapeDTM.h"

#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < automaton::OneTapeDTM < > > ( );
auto xmlRead = registration::XmlReaderRegister < automaton::OneTapeDTM < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, automaton::OneTapeDTM < > > ( );

} /* namespace */
