/*
 * NFTA.cpp
 *
 *  Created on: Mar 21, 2015
 *      Author: Stepan Plachy
 */

#include "NFTA.h"
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < automaton::NFTA < > > ( );
auto xmlRead = registration::XmlReaderRegister < automaton::NFTA < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, automaton::NFTA < > > ( );

} /* namespace */
