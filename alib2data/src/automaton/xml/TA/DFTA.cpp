/*
 * DFTA.cpp
 *
 *  Created on: Apr 14, 2015
 *      Author: Stepan Plachy
 */

#include "DFTA.h"
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < automaton::DFTA < > > ( );
auto xmlRead = registration::XmlReaderRegister < automaton::DFTA < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, automaton::DFTA < > > ( );

} /* namespace */
