/*
 * RealTimeHeightDeterministicNPDA.cpp
 *
 *  Created on: Apr 10, 2013
 *      Author: Jan Travnicek
 */

#include "RealTimeHeightDeterministicNPDA.h"
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < automaton::RealTimeHeightDeterministicNPDA < > > ( );
auto xmlRead = registration::XmlReaderRegister < automaton::RealTimeHeightDeterministicNPDA < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, automaton::RealTimeHeightDeterministicNPDA < > > ( );

} /* namespace */
