/*
 * EpsilonNFA.cpp
 *
 *  Created on: Mar 25, 2013
 *      Author: Jan Travnicek
 */

#include "EpsilonNFA.h"
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < automaton::EpsilonNFA < > > ( );
auto xmlRead = registration::XmlReaderRegister < automaton::EpsilonNFA < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, automaton::EpsilonNFA < > > ( );

} /* namespace */
