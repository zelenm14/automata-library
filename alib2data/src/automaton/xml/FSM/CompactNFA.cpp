/*
 * CompactNFA.cpp
 *
 *  Created on: Mar 25, 2013
 *      Author: Jan Travnicek
 */

#include "CompactNFA.h"
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < automaton::CompactNFA < > > ( );
auto xmlRead = registration::XmlReaderRegister < automaton::CompactNFA < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, automaton::CompactNFA < > > ( );

} /* namespace */
