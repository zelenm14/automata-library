/*
 * DPDA.cpp
 *
 *  Created on: Apr 10, 2013
 *      Author: Jan Travnicek
 */

#include "DPDA.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::DPDA < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::DPDA < > > ( );

} /* namespace */
