/*
 * SinglePopDPDA.cpp
 *
 *  Created on: Apr 10, 2013
 *      Author: Jan Travnicek
 */

#include "SinglePopDPDA.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::SinglePopDPDA < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::SinglePopDPDA < > > ( );

} /* namespace */
