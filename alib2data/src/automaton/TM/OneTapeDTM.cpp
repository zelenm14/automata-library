/*
 * OneTapeDTM.cpp
 *
 *  Created on: Apr 24, 2013
 *      Author: Jan Travnicek
 */

#include "OneTapeDTM.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::OneTapeDTM < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::OneTapeDTM < > > ( );

} /* namespace */
