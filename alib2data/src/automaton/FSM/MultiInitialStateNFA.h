/*
 * MultiInitialStateNFA.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: Mar 25, 2013
 *      Author: Jan Travnicek
 */

#ifndef MULTI_INITIAL_STATE_NFA_H_
#define MULTI_INITIAL_STATE_NFA_H_

#include <ostream>
#include <sstream>

#include <alib/multimap>
#include <alib/set>

#include <core/components.hpp>

#include <common/DefaultStateType.h>
#include <common/DefaultSymbolType.h>

#include <automaton/AutomatonException.h>

#include <core/normalize.hpp>
#include <alphabet/common/SymbolNormalize.h>
#include <automaton/common/AutomatonNormalize.h>

#include "NFA.h"
#include "DFA.h"

namespace automaton {

class InputAlphabet;
class States;
class FinalStates;
class InitialStates;

/**
 * \brief
 * Nondeterministic finite automaton with multiple initial states. Accepts regular languages.

 * \details
 * Definition is classical definition of finite automata.
 * A = (Q, T, I, \delta, F),
 * Q (States) = nonempty finite set of states,
 * T (TerminalAlphabet) = finite set of terminal symbols - having this empty won't let automaton do much though,
 * I (InitialState) = finite set of initial states,
 * \delta = transition function of the form A \times a -> P(Q), where A \in Q, a \in T, and P(Q) is a powerset of states,
 * F (FinalStates) = set of final states
 *
 * \tparam SymbolTypeT used for the terminal alphabet
 * \tparam StateTypeT used to the states, and the initial state of the automaton.
 */
template < class SymbolTypeT = DefaultSymbolType, class StateTypeT = DefaultStateType >
class MultiInitialStateNFA final : public core::Components < MultiInitialStateNFA < SymbolTypeT, StateTypeT >, ext::set < SymbolTypeT >, component::Set, InputAlphabet, ext::set < StateTypeT >, component::Set, std::tuple < States, InitialStates, FinalStates > > {
public:
	typedef SymbolTypeT SymbolType;
	typedef StateTypeT StateType;

private:
	/**
	 * Transition function as mapping from a state times an input symbol on the left hand side to a set of states.
	 */
	ext::multimap < ext::pair < StateType, SymbolType >, StateType > transitions;

public:
	/**
	 * \brief Creates a new instance of the automaton
	 *
	 * \param initialState the initial state of the automaton
	 */
	explicit MultiInitialStateNFA ( );

	/**
	 * \brief Creates a new instance of the automaton with a concrete set of states, input alphabet, initial states, and a set of final states.
	 *
	 * \param states the initial set of states of the automaton
	 * \param inputAlphabet the initial input alphabet
	 * \param initialStates the initial set of initial states of the automaton
	 * \param finalStates the initial set of final states of the automaton
	 */
	explicit MultiInitialStateNFA ( ext::set < StateType > states, ext::set < SymbolType > inputAlphabet, ext::set < StateType > initialStates, ext::set < StateType > finalStates );

	/*
	 * \brief Creates a new instance of the automaton based on the Nondeterministic finite automaton.
	 *
	 * \param other the Nondeterministic finite automaton
	 */
	explicit MultiInitialStateNFA ( const NFA < SymbolType, StateType > & other );

	/*
	 * \brief Creates a new instance of the automaton based on the Deterministic finite automaton.
	 *
	 * \param other the Deterministic finite automaton
	 */
	explicit MultiInitialStateNFA ( const DFA < SymbolType, StateType > & other );

	/**
	 * Getter of initial states.
	 *
	 * \returns the initial states of the automaton
	 */
	const ext::set < StateType > & getInitialStates ( ) const & {
		return this->template accessComponent < InitialStates > ( ).get ( );
	}

	/**
	 * Getter of initial states.
	 *
	 * \returns the initial states of the automaton
	 */
	ext::set < StateType > && getInitialStates ( ) && {
		return std::move ( this->template accessComponent < InitialStates > ( ).get ( ) );
	}

	/**
	 * Adder of a initial state.
	 *
	 * \param state the new state to be added to a set of initial states
	 *
	 * \returns true if the state was indeed added
	 */
	bool addInitialState ( StateType state ) {
		return this->template accessComponent < InitialStates > ( ).add ( std::move ( state ) );
	}

	/**
	 * Setter of initial states.
	 *
	 * \param states completely new set of initial states
	 */
	void setInitialStates ( ext::set < StateType > states ) {
		this->template accessComponent < InitialStates > ( ).set ( std::move ( states ) );
	}

	/**
	 * Remover of a initial state.
	 *
	 * \param state a state to be removed from a set of initial states
	 *
	 * \returns true if the state was indeed removed
	 */
	void removeInitialState ( const StateType & state ) {
		this->template accessComponent < InitialStates > ( ).remove ( state );
	}

	/**
	 * Getter of states.
	 *
	 * \returns the states of the automaton
	 */
	const ext::set < StateType > & getStates ( ) const & {
		return this->template accessComponent < States > ( ).get ( );
	}

	/**
	 * Getter of states.
	 *
	 * \returns the states of the automaton
	 */
	ext::set < StateType > && getStates ( ) && {
		return std::move ( this->template accessComponent < States > ( ).get ( ) );
	}

	/**
	 * Adder of a state.
	 *
	 * \param state the new state to be added to a set of states
	 *
	 * \returns true if the state was indeed added
	 */
	bool addState ( StateType state ) {
		return this->template accessComponent < States > ( ).add ( std::move ( state ) );
	}

	/**
	 * Setter of states.
	 *
	 * \param states completely new set of states
	 */
	void setStates ( ext::set < StateType > states ) {
		this->template accessComponent < States > ( ).set ( std::move ( states ) );
	}

	/**
	 * Remover of a state.
	 *
	 * \param state a state to be removed from a set of states
	 *
	 * \returns true if the state was indeed removed
	 */
	void removeState ( const StateType & state ) {
		this->template accessComponent < States > ( ).remove ( state );
	}

	/**
	 * Getter of final states.
	 *
	 * \returns the final states of the automaton
	 */
	const ext::set < StateType > & getFinalStates ( ) const & {
		return this->template accessComponent < FinalStates > ( ).get ( );
	}

	/**
	 * Getter of final states.
	 *
	 * \returns the final states of the automaton
	 */
	ext::set < StateType > && getFinalStates ( ) && {
		return std::move ( this->template accessComponent < FinalStates > ( ).get ( ) );
	}

	/**
	 * Adder of a final state.
	 *
	 * \param state the new state to be added to a set of final states
	 *
	 * \returns true if the state was indeed added
	 */
	bool addFinalState ( StateType state ) {
		return this->template accessComponent < FinalStates > ( ).add ( std::move ( state ) );
	}

	/**
	 * Setter of final states.
	 *
	 * \param states completely new set of final states
	 */
	void setFinalStates ( ext::set < StateType > states ) {
		this->template accessComponent < FinalStates > ( ).set ( std::move ( states ) );
	}

	/**
	 * Remover of a final state.
	 *
	 * \param state a state to be removed from a set of final states
	 *
	 * \returns true if the state was indeed removed
	 */
	void removeFinalState ( const StateType & state ) {
		this->template accessComponent < FinalStates > ( ).remove ( state );
	}

	/**
	 * Getter of the input alphabet.
	 *
	 * \returns the input alphabet of the automaton
	 */
	const ext::set < SymbolType > & getInputAlphabet ( ) const & {
		return this->template accessComponent < InputAlphabet > ( ).get ( );
	}

	/**
	 * Getter of the input alphabet.
	 *
	 * \returns the input alphabet of the automaton
	 */
	ext::set < SymbolType > && getInputAlphabet ( ) && {
		return std::move ( this->template accessComponent < InputAlphabet > ( ).get ( ) );
	}

	/**
	 * Adder of a input symbol.
	 *
	 * \param symbol the new symbol to be added to an input alphabet
	 *
	 * \returns true if the symbol was indeed added
	 */
	bool addInputSymbol ( SymbolType symbol ) {
		return this->template accessComponent < InputAlphabet > ( ).add ( std::move ( symbol ) );
	}

	/**
	 * Adder of input symbols.
	 *
	 * \param symbols new symbols to be added to an input alphabet
	 */
	void addInputSymbols ( ext::set < SymbolType > symbols ) {
		this->template accessComponent < InputAlphabet > ( ).add ( std::move ( symbols ) );
	}

	/**
	 * Setter of input alphabet.
	 *
	 * \param symbols completely new input alphabet
	 */
	void setInputAlphabet ( ext::set < SymbolType > symbols ) {
		this->template accessComponent < InputAlphabet > ( ).set ( std::move ( symbols ) );
	}

	/**
	 * Remover of an input symbol.
	 *
	 * \param symbol a symbol to be removed from an input alphabet
	 *
	 * \returns true if the symbol was indeed removed
	 */
	void removeInputSymbol ( const SymbolType & symbol ) {
		this->template accessComponent < InputAlphabet > ( ).remove ( symbol );
	}

	/**
	 * \brief Add a transition to the automaton.
	 *
	 * \details The transition is in a form A \times a -> B, where A, B \in Q and a \in T
	 *
	 * \param current the source state (A)
	 * \param input the input symbol (a)
	 * \param next the target state (B)
	 *
	 * \throws AutomatonException when transition contains state or symbol not present in the automaton components
	 *
	 * \returns true if the transition was indeed added
	 */
	bool addTransition ( StateType from, SymbolType input, StateType to );

	/**
	 * \brief Removes a transition from the automaton.
	 *
	 * \details The transition is in a form A \times a -> B, where A, B \in Q and a \in T
	 *
	 * \param current the source state (A)
	 * \param input the input symbol (a)
	 * \param next the target state (B)
	 *
	 * \returns true if the transition was indeed removed
	 */
	bool removeTransition ( const StateType & from, const SymbolType & input, const StateType & to );

	/**
	 * Get the transition function of the automaton in its natural form.
	 *
	 * \returns transition function of the automaton
	 */
	const ext::multimap < ext::pair < StateType, SymbolType >, StateType > & getTransitions ( ) const &;

	/**
	 * Get the transition function of the automaton in its natural form.
	 *
	 * \returns transition function of the automaton
	 */
	ext::multimap < ext::pair < StateType, SymbolType >, StateType > && getTransitions ( ) &&;

	/**
	 * Get a subset of the transition function of the automaton, with the source state fixed in the transitions natural representation.
	 *
	 * \param from filter the transition function based on this state as a source state
	 *
	 * \returns a subset of the transition function of the automaton with the source state fixed
	 */
	ext::multimap < ext::pair < StateType, SymbolType >, StateType > getTransitionsFromState ( const StateType & from ) const;

	/**
	 * Get a subset of the transition function of the automaton, with the target state fixed in the transitions natural representation.
	 *
	 * \param to filter the transition function based on this state as a source state
	 *
	 * \returns a subset of the transition function of the automaton with the target state fixed
	 */
	ext::multimap < ext::pair < StateType, SymbolType >, StateType > getTransitionsToState ( const StateType & to ) const;

	/**
	 * \brief Determines whether the automaton is deterministic.
	 *
	 * the automaton is deterministic if and only if:
	 * \li \c contains only 1 initial state.
	 * \li \c size of transition function \delta (from state, input symbol) \leq 1
	 *
	 * \return true if the automaton is deterministic, false otherwise
	 */
	bool isDeterministic ( ) const;

	/**
	 * \brief Determines whether the automaton is total deterministic
	 *
	 * The automaton is deterministic if and only if:
	 * \li \c it is deterministic
	 * \li \c size of transition function \forall states and \forall input symbols \delta (from state, input symbol) = 1
	 *
	 * \return true if the automaton is total deterministic, false otherwise
	 */
	bool isTotal ( ) const;

	/**
	 * The three way comparison implementation
	 *
	 * \param other the other instance
	 *
	 * \returns the ordering between this object and the @p other.
	 */
	auto operator <=> ( const MultiInitialStateNFA & other ) const {
		return std::tie ( getStates ( ), getInputAlphabet ( ), getInitialStates ( ), getFinalStates ( ), transitions ) <=> std::tie ( other.getStates ( ), other.getInputAlphabet ( ), other.getInitialStates ( ), other.getFinalStates ( ), other.getTransitions ( ) );
	}

	/**
	 * The equality comparison implementation.
	 *
	 * \param other the other object to compare with.
	 *
	 * \returns true if this and other objects are equal, false othervise
	 */
	bool operator == ( const MultiInitialStateNFA & other ) const {
		return std::tie ( getStates ( ), getInputAlphabet ( ), getInitialStates ( ), getFinalStates ( ), transitions ) == std::tie ( other.getStates ( ), other.getInputAlphabet ( ), other.getInitialStates ( ), other.getFinalStates ( ), other.getTransitions ( ) );
	}

	/**
	 * Print this object as raw representation to ostream.
	 *
	 * \param out ostream where to print
	 * \param instance object to print
	 *
	 * \returns modified output stream
	 */
	friend std::ostream & operator << ( std::ostream & out, const MultiInitialStateNFA & instance ) {
		return out << "(MultiInitialStateNFA "
			   << "states = " << instance.getStates ( )
			   << "inputAlphabet = " << instance.getInputAlphabet ( )
			   << "initialStates = " << instance.getInitialStates ( )
			   << "finalStates = " << instance.getFinalStates ( )
			   << "transitions = " << instance.getTransitions ( )
			   << ")";
	}

	/**
	 * Casts this instance to as compact as possible string representation.
	 *
	 * \returns string representation of the object
	 */
	explicit operator std::string ( ) const;
};

/**
 * Trait to detect whether the type parameter T is or is not MultiInitialStateNFA. Derived from std::false_type.
 *
 * \tparam T the tested type parameter
 */
template < class T >
class isMultiInitialStateNFA_impl : public std::false_type {};

/**
 * Trait to detect whether the type parameter T is or is not MultiInitialStateNFA. Derived from std::true_type.
 *
 * Specialisation for MultiInitialStateNFA.
 *
 * \tparam SymbolType used for the terminal alphabet of the automaton
 * \tparam StateType used for the terminal alphabet of the automaton
 */
template < class SymbolType, class StateType >
class isMultiInitialStateNFA_impl < MultiInitialStateNFA < SymbolType, StateType > > : public std::true_type {};

/**
 * Constexpr true if the type parameter T is MultiInitialStateNFA, false otherwise.
 *
 * \tparam T the tested type parameter
 */
template < class T >
constexpr bool isMultiInitialStateNFA = isMultiInitialStateNFA_impl < T > { };

template < class SymbolType, class StateType >
MultiInitialStateNFA < SymbolType, StateType >::MultiInitialStateNFA ( ext::set < StateType > states, ext::set < SymbolType > inputAlphabet, ext::set < StateType > initialStates, ext::set < StateType > finalStates ) : core::Components < MultiInitialStateNFA, ext::set < SymbolType >, component::Set, InputAlphabet, ext::set < StateType >, component::Set, std::tuple < States, InitialStates, FinalStates > > ( std::move ( inputAlphabet ), std::move ( states ), std::move ( initialStates ), std::move ( finalStates ) ) {
}

template < class SymbolType, class StateType >
MultiInitialStateNFA < SymbolType, StateType >::MultiInitialStateNFA ( ) : MultiInitialStateNFA ( ext::set < StateType > { }, ext::set < SymbolType > { }, ext::set < StateType > { }, ext::set < StateType > { } ) {
}

template < class SymbolType, class StateType >
MultiInitialStateNFA < SymbolType, StateType >::MultiInitialStateNFA ( const DFA < SymbolType, StateType > & other ) : MultiInitialStateNFA ( other.getStates ( ), other.getInputAlphabet ( ), { other.getInitialState ( ) }, other.getFinalStates ( ) ) {
	transitions.insert ( other.getTransitions ( ).begin ( ), other.getTransitions ( ).end ( ) );
}

template < class SymbolType, class StateType >
MultiInitialStateNFA < SymbolType, StateType >::MultiInitialStateNFA ( const NFA < SymbolType, StateType > & other ) : MultiInitialStateNFA ( other.getStates ( ), other.getInputAlphabet ( ), { other.getInitialState ( ) }, other.getFinalStates ( ) ) {
	transitions = other.getTransitions ( );
}

template < class SymbolType, class StateType >
bool MultiInitialStateNFA < SymbolType, StateType >::addTransition ( StateType from, SymbolType input, StateType to ) {
	if ( !getStates ( ).count ( from ) )
		throw AutomatonException ( "State \"" + ext::to_string ( from ) + "\" doesn't exist." );

	if ( !getInputAlphabet ( ).count ( input ) )
		throw AutomatonException ( "Input symbol \"" + ext::to_string ( input ) + "\" doesn't exist." );

	if ( !getStates ( ).count ( to ) )
		throw AutomatonException ( "State \"" + ext::to_string ( to ) + "\" doesn't exist." );

	auto upper_bound = transitions.upper_bound ( ext::tie ( from, input ) );
	auto lower_bound = transitions.lower_bound ( ext::tie ( from, input ) );
	auto iter = std::lower_bound ( lower_bound, upper_bound, to, [ ] ( const auto & transition, const auto & target ) { return transition.second < target; } );
	if ( iter != upper_bound && to >= iter->second )
		return false;

	ext::pair < StateType, SymbolType > key = ext::make_pair ( std::move ( from ), std::move ( input ) );
	transitions.insert ( iter, std::make_pair ( std::move ( key ), std::move ( to ) ) );
	return true;
}

template < class SymbolType, class StateType >
bool MultiInitialStateNFA < SymbolType, StateType >::removeTransition ( const StateType & from, const SymbolType & input, const StateType & to ) {
	auto upper_bound = transitions.upper_bound ( ext::tie ( from, input ) );
	auto lower_bound = transitions.lower_bound ( ext::tie ( from, input ) );
	auto iter = std::find_if ( lower_bound, upper_bound, [ & ] ( const auto & transition ) { return transition.second == to; } );
	if ( iter == upper_bound )
		return false;

	transitions.erase ( iter );
	return true;
}

template < class SymbolType, class StateType >
const ext::multimap < ext::pair < StateType, SymbolType >, StateType > & MultiInitialStateNFA < SymbolType, StateType >::getTransitions ( ) const & {
	return transitions;
}

template < class SymbolType, class StateType >
ext::multimap < ext::pair < StateType, SymbolType >, StateType > && MultiInitialStateNFA < SymbolType, StateType >::getTransitions ( ) && {
	return std::move ( transitions );
}

template < class SymbolType, class StateType >
ext::multimap < ext::pair < StateType, SymbolType >, StateType > MultiInitialStateNFA < SymbolType, StateType >::getTransitionsFromState ( const StateType & from ) const {
	if ( !getStates ( ).count ( from ) )
		throw AutomatonException ( "State \"" + ext::to_string ( from ) + "\" doesn't exist" );

	ext::multimap < ext::pair < StateType, SymbolType >, StateType > transitionsFromState;

	for ( const std::pair < const ext::pair < StateType, SymbolType >, StateType > & transition : transitions )
		if ( transition.first.first == from )
			transitionsFromState.insert ( transition );

	return transitionsFromState;
}

template < class SymbolType, class StateType >
ext::multimap < ext::pair < StateType, SymbolType >, StateType > MultiInitialStateNFA < SymbolType, StateType >::getTransitionsToState ( const StateType & to ) const {
	if ( !getStates ( ).count ( to ) )
		throw AutomatonException ( "State \"" + ext::to_string ( to ) + "\" doesn't exist" );

	ext::multimap < ext::pair < StateType, SymbolType >, StateType > transitionsToState;

	for ( const std::pair < const ext::pair < StateType, SymbolType >, StateType > & transition : transitions )
		if ( transition.second == to )
			transitionsToState.insert ( transition );

	return transitionsToState;
}

template < class SymbolType, class StateType >
bool MultiInitialStateNFA < SymbolType, StateType >::isDeterministic ( ) const {
	if ( transitions.empty ( ) )
		return true;

	for ( auto iter = transitions.begin ( ); std::next ( iter ) != transitions.end ( ); ++ iter )
		if ( iter->first == std::next ( iter )->first )
			return false;

	return getInitialStates ( ).size ( ) == 1;
}

template < class SymbolType, class StateType >
bool MultiInitialStateNFA < SymbolType, StateType >::isTotal ( ) const {
	return isDeterministic ( ) && transitions.size ( ) == getInputAlphabet ( ).size ( ) * getStates ( ).size ( );
}

template < class SymbolType, class StateType >
MultiInitialStateNFA < SymbolType, StateType >::operator std::string ( ) const {
	std::stringstream ss;
	ss << * this;
	return ss.str ( );
}

} /* namespace automaton */

namespace core {

/**
 * Helper class specifying constraints for the automaton's internal input alphabet component.
 *
 * \tparam SymbolType used for the terminal alphabet of the automaton.
 * \tparam StateType used for the terminal alphabet of the automaton.
 */
template < class SymbolType, class StateType >
class SetConstraint< automaton::MultiInitialStateNFA < SymbolType, StateType >, SymbolType, automaton::InputAlphabet > {
public:
	/**
	 * Returns true if the symbol is still used in some transition of the automaton.
	 *
	 * \param automaton the tested automaton
	 * \param symbol the tested symbol
	 *
	 * \returns true if the symbol is used, false othervise
	 */
	static bool used ( const automaton::MultiInitialStateNFA < SymbolType, StateType > & automaton, const SymbolType & symbol ) {
		for ( const std::pair < const ext::pair < StateType, SymbolType >, StateType > & transition : automaton.getTransitions ( ) )
			if ( transition.first.second == symbol )
				return true;

		return false;
	}

	/**
	 * Returns true as all symbols are possibly available to be elements of the input alphabet.
	 *
	 * \param automaton the tested automaton
	 * \param symbol the tested symbol
	 *
	 * \returns true
	 */
	static bool available ( const automaton::MultiInitialStateNFA < SymbolType, StateType > &, const SymbolType & ) {
		return true;
	}

	/**
	 * All symbols are valid as input symbols.
	 *
	 * \param automaton the tested automaton
	 * \param symbol the tested symbol
	 */
	static void valid ( const automaton::MultiInitialStateNFA < SymbolType, StateType > &, const SymbolType & ) {
	}
};

/**
 * Helper class specifying constraints for the automaton's internal states component.
 *
 * \tparam SymbolType used for the terminal alphabet of the automaton.
 * \tparam StateType used for the terminal alphabet of the automaton.
 */
template < class SymbolType, class StateType >
class SetConstraint< automaton::MultiInitialStateNFA < SymbolType, StateType >, StateType, automaton::States > {
public:
	/**
	 * Returns true if the state is still used in some transition of the automaton.
	 *
	 * \param automaton the tested automaton
	 * \param state the tested state
	 *
	 * \returns true if the state is used, false othervise
	 */
	static bool used ( const automaton::MultiInitialStateNFA < SymbolType, StateType > & automaton, const StateType & state ) {
		if ( automaton.getInitialStates ( ).count ( state ) )
			return true;

		if ( automaton.getFinalStates ( ).count ( state ) )
			return true;

		for ( const std::pair < const ext::pair < StateType, SymbolType >, StateType > & transition : automaton.getTransitions ( ) )
			if ( transition.first.first == state || transition.second == state )
				return true;

		return false;
	}

	/**
	 * Returns true as all states are possibly available to be elements of the states.
	 *
	 * \param automaton the tested automaton
	 * \param state the tested state
	 *
	 * \returns true
	 */
	static bool available ( const automaton::MultiInitialStateNFA < SymbolType, StateType > &, const StateType & ) {
		return true;
	}

	/**
	 * All states are valid as a state of the automaton.
	 *
	 * \param automaton the tested automaton
	 * \param state the tested state
	 */
	static void valid ( const automaton::MultiInitialStateNFA < SymbolType, StateType > &, const StateType & ) {
	}
};

/**
 * Helper class specifying constraints for the automaton's internal final states component.
 *
 * \tparam SymbolType used for the terminal alphabet of the automaton.
 * \tparam StateType used for the terminal alphabet of the automaton.
 */
template < class SymbolType, class StateType >
class SetConstraint< automaton::MultiInitialStateNFA < SymbolType, StateType >, StateType, automaton::FinalStates > {
public:
	/**
	 * Returns false. Final state is only a mark that the automaton itself does require further.
	 *
	 * \param automaton the tested automaton
	 * \param state the tested state
	 *
	 * \returns false
	 */
	static bool used ( const automaton::MultiInitialStateNFA < SymbolType, StateType > &, const StateType & ) {
		return false;
	}

	/**
	 * Determines whether the state is available in the automaton's states set.
	 *
	 * \param automaton the tested automaton
	 * \param state the tested state
	 *
	 * \returns true if the state is already in the set of states of the automaton
	 */
	static bool available ( const automaton::MultiInitialStateNFA < SymbolType, StateType > & automaton, const StateType & state ) {
		return automaton.template accessComponent < automaton::States > ( ).get ( ).count ( state );
	}

	/**
	 * All states are valid as a final state of the automaton.
	 *
	 * \param automaton the tested automaton
	 * \param state the tested state
	 */
	static void valid ( const automaton::MultiInitialStateNFA < SymbolType, StateType > &, const StateType & ) {
	}
};

/**
 * Helper class specifying constraints for the automaton's internal initial states component.
 *
 * \tparam SymbolType used for the terminal alphabet of the automaton.
 * \tparam StateType used for the terminal alphabet of the automaton.
 */
template < class SymbolType, class StateType >
class SetConstraint< automaton::MultiInitialStateNFA < SymbolType, StateType >, StateType, automaton::InitialStates > {
public:
	/**
	 * Returns false. Initial state is only a mark that the automaton itself does require further.
	 *
	 * \param automaton the tested automaton
	 * \param state the tested state
	 *
	 * \returns false
	 */
	static bool used ( const automaton::MultiInitialStateNFA < SymbolType, StateType > &, const StateType & ) {
		return false;
	}

	/**
	 * Determines whether the state is available in the automaton's states set.
	 *
	 * \param automaton the tested automaton
	 * \param state the tested state
	 *
	 * \returns true if the state is already in the set of states of the automaton
	 */
	static bool available ( const automaton::MultiInitialStateNFA < SymbolType, StateType > & automaton, const StateType & state ) {
		return automaton.template accessComponent < automaton::States > ( ).get ( ).count ( state );
	}

	/**
	 * All states are valid as a initial state of the automaton.
	 *
	 * \param automaton the tested automaton
	 * \param state the tested state
	 */
	static void valid ( const automaton::MultiInitialStateNFA < SymbolType, StateType > &, const StateType & ) {
	}
};

/**
 * Helper for normalisation of types specified by templates used as internal datatypes of symbols and states.
 *
 * \returns new instance of the automaton with default template parameters or unmodified instance if the template parameters were already the default ones
 */
template < class SymbolType, class StateType >
struct normalize < automaton::MultiInitialStateNFA < SymbolType, StateType > > {
	static automaton::MultiInitialStateNFA < > eval ( automaton::MultiInitialStateNFA < SymbolType, StateType > && value ) {
		ext::set < DefaultSymbolType > alphabet = alphabet::SymbolNormalize::normalizeAlphabet ( std::move ( value ).getInputAlphabet ( ) );
		ext::set < DefaultStateType > states = automaton::AutomatonNormalize::normalizeStates ( std::move ( value ).getStates ( ) );
		ext::set < DefaultStateType > initialStates = automaton::AutomatonNormalize::normalizeStates ( std::move ( value ).getInitialStates ( ) );
		ext::set < DefaultStateType > finalStates = automaton::AutomatonNormalize::normalizeStates ( std::move ( value ).getFinalStates ( ) );

		automaton::MultiInitialStateNFA < > res ( std::move ( states ), std::move ( alphabet ), std::move ( initialStates ), std::move ( finalStates ) );

		for ( std::pair < ext::pair < StateType, SymbolType >, StateType > && transition : ext::make_mover ( std::move ( value ).getTransitions ( ) ) ) {
			DefaultStateType from = automaton::AutomatonNormalize::normalizeState ( std::move ( transition.first.first ) );
			DefaultSymbolType input = alphabet::SymbolNormalize::normalizeSymbol ( std::move ( transition.first.second ) );
			DefaultStateType target = automaton::AutomatonNormalize::normalizeState ( std::move ( transition.second ) );

			res.addTransition ( std::move ( from ), std::move ( input ), std::move ( target ) );
		}

		return res;
	}
};

} /* namespace core */

extern template class automaton::MultiInitialStateNFA < >;

#endif /* MULTI_INITIAL_STATE_NFA_H_ */
