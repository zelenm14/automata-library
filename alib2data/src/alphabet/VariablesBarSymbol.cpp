/*
 * VariablesBarSymbol.h
 *
 *  Created on: Apr 10, 2013
 *      Author: Jan Travnicek
 */

#include "VariablesBarSymbol.h"

#include <object/Object.h>

#include <registration/ValuePrinterRegistration.hpp>

namespace alphabet {

VariablesBarSymbol::VariablesBarSymbol ( ) = default;

std::ostream & operator << ( std::ostream & out, const VariablesBarSymbol & ) {
	return out << "(Variables bar symbol)";
}

VariablesBarSymbol::operator std::string ( ) const {
	return VariablesBarSymbol::instance < std::string > ( );
}

} /* namespace alphabet */

namespace {

auto valuePrinter = registration::ValuePrinterRegister < alphabet::VariablesBarSymbol > ( );

} /* namespace */
