/*
 * VariablesBarSymbol.h
 *
 *  Created on: Apr 10, 2013
 *      Author: Jan Travnicek
 */

#include "VariablesBarSymbol.h"
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace core {

alphabet::VariablesBarSymbol xmlApi < alphabet::VariablesBarSymbol >::parse ( ext::deque < sax::Token >::iterator & input ) {
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::END_ELEMENT, xmlTagName ( ) );
	return alphabet::VariablesBarSymbol ( );
}

bool xmlApi < alphabet::VariablesBarSymbol >::first ( const ext::deque < sax::Token >::const_iterator & input ) {
	return sax::FromXMLParserHelper::isToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
}

std::string xmlApi < alphabet::VariablesBarSymbol >::xmlTagName ( ) {
	return "VariablesBarSymbol";
}

void xmlApi < alphabet::VariablesBarSymbol >::compose ( ext::deque < sax::Token > & output, const alphabet::VariablesBarSymbol & ) {
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::START_ELEMENT );
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::END_ELEMENT );
}

} /* namespace core */

namespace {

auto xmlWrite = registration::XmlWriterRegister < alphabet::VariablesBarSymbol > ( );
auto xmlRead = registration::XmlReaderRegister < alphabet::VariablesBarSymbol > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, alphabet::VariablesBarSymbol > ( );

} /* namespace */
