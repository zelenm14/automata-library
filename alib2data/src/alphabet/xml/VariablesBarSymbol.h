/*
 * VariablesBarSymbol.h
 *
 *  Created on: Apr 10, 2013
 *      Author: Jan Travnicek
 */

#ifndef _XML_VARIABLES_BAR_SYMBOL_H_
#define _XML_VARIABLES_BAR_SYMBOL_H_

#include <alphabet/VariablesBarSymbol.h>
#include <core/xmlApi.hpp>

namespace core {

template < >
struct xmlApi < alphabet::VariablesBarSymbol > {
	static alphabet::VariablesBarSymbol parse ( ext::deque < sax::Token >::iterator & input );
	static bool first ( const ext::deque < sax::Token >::const_iterator & input );
	static std::string xmlTagName ( );
	static void compose ( ext::deque < sax::Token > & output, const alphabet::VariablesBarSymbol & data );
};

} /* namespace core */

#endif /* _XML_VARIABLES_BAR_SYMBOL_H_ */
