/*
 * BottomOfTheStackSymbol.h
 *
 *  Created on: Jun 19, 2014
 *      Author: Jan Travnicek
 */

#ifndef _XML_BOTTOM_OF_THE_STACK_SYMBOL_H_
#define _XML_BOTTOM_OF_THE_STACK_SYMBOL_H_

#include <alphabet/BottomOfTheStackSymbol.h>
#include <core/xmlApi.hpp>

namespace core {

template < >
struct xmlApi < alphabet::BottomOfTheStackSymbol > {
	static alphabet::BottomOfTheStackSymbol parse ( ext::deque < sax::Token >::iterator & input );
	static bool first ( const ext::deque < sax::Token >::const_iterator & input );
	static std::string xmlTagName ( );
	static void compose ( ext::deque < sax::Token > & output, const alphabet::BottomOfTheStackSymbol & data );
};

} /* namespace core */

#endif /* _XML_BOTTOM_OF_THE_STACK_SYMBOL_H_ */
