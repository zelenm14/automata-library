/*
 * BarSymbol.h
 *
 *  Created on: Apr 10, 2013
 *      Author: Stepan Plachy
 */

#ifndef _XML_BAR_SYMBOL_H_
#define _XML_BAR_SYMBOL_H_

#include <alphabet/BarSymbol.h>
#include <core/xmlApi.hpp>

namespace core {

template < >
struct xmlApi < alphabet::BarSymbol > {
	static alphabet::BarSymbol parse ( ext::deque < sax::Token >::iterator & input );
	static bool first ( const ext::deque < sax::Token >::const_iterator & input );
	static std::string xmlTagName ( );
	static void compose ( ext::deque < sax::Token > & output, const alphabet::BarSymbol & data );
};

} /* namespace core */

#endif /* _XML_BAR_SYMBOL_H_ */
