/*
 * RankedSymbol.cpp
 *
 *  Created on: Apr 10, 2013
 *      Author: Stepan Plachy
 */

#include "RankedSymbol.h"
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < common::ranked_symbol < > > ( );
auto xmlRead = registration::XmlReaderRegister < common::ranked_symbol < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, common::ranked_symbol < > > ( );

} /* namespace */
