/*
 * EndSymbol.cpp
 *
 *  Created on: Jun 19, 2014
 *      Author: Jan Travnicek
 */

#include "EndSymbol.h"
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace core {

alphabet::EndSymbol xmlApi < alphabet::EndSymbol >::parse ( ext::deque < sax::Token >::iterator & input ) {
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::END_ELEMENT, xmlTagName ( ) );
	return alphabet::EndSymbol ( );
}

bool xmlApi < alphabet::EndSymbol >::first ( const ext::deque < sax::Token >::const_iterator & input ) {
	return sax::FromXMLParserHelper::isToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
}

std::string xmlApi < alphabet::EndSymbol >::xmlTagName ( ) {
	return "EndSymbol";
}

void xmlApi < alphabet::EndSymbol >::compose ( ext::deque < sax::Token > & output, const alphabet::EndSymbol & ) {
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::START_ELEMENT );
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::END_ELEMENT );
}

} /* namespace core */

namespace {

auto xmlWrite = registration::XmlWriterRegister < alphabet::EndSymbol > ( );
auto xmlRead = registration::XmlReaderRegister < alphabet::EndSymbol > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, alphabet::EndSymbol > ( );

} /* namespace */
