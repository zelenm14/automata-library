/*
 * BlankSymbol.h
 *
 *  Created on: Mar 26, 2013
 *      Author: Jan Travnicek
 */

#ifndef _XML_BLANK_SYMBOL_H_
#define _XML_BLANK_SYMBOL_H_

#include <alphabet/BlankSymbol.h>
#include <core/xmlApi.hpp>

namespace core {

template < >
struct xmlApi < alphabet::BlankSymbol > {
	static alphabet::BlankSymbol parse ( ext::deque < sax::Token >::iterator & input );
	static bool first ( const ext::deque < sax::Token >::const_iterator & input );
	static std::string xmlTagName ( );
	static void compose ( ext::deque < sax::Token > & output, const alphabet::BlankSymbol & data );
};

} /* namespace core */

#endif /* _XML_BLANK_SYMBOL_H_ */
