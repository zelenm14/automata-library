/*
 * InitialSymbol.cpp
 *
 *  Created on: Mar 26, 2013
 *      Author: Jan Travicek
 */

#include "InitialSymbol.h"

#include <object/Object.h>

#include <registration/ValuePrinterRegistration.hpp>

namespace alphabet {

InitialSymbol::InitialSymbol() = default;

std::ostream & operator << ( std::ostream & out, const InitialSymbol & ) {
	return out << "(InitialSymbol)";
}

InitialSymbol::operator std::string ( ) const {
	return InitialSymbol::instance < std::string > ( );
}

} /* namespace alphabet */

namespace {

auto valuePrinter = registration::ValuePrinterRegister < alphabet::InitialSymbol > ( );

} /* namespace */
