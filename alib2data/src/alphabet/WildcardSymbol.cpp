/*
 * WildcardSymbol.cpp
 *
 *  Created on: Apr 10, 2013
 *      Author: Jan Travnicek
 */

#include "WildcardSymbol.h"

#include <object/Object.h>

#include <registration/ValuePrinterRegistration.hpp>

namespace alphabet {

WildcardSymbol::WildcardSymbol() = default;

std::ostream & operator << ( std::ostream & out, const WildcardSymbol & ) {
	return out << "(WildcardSymbol)";
}

WildcardSymbol::operator std::string ( ) const {
	return WildcardSymbol::instance < std::string > ( );
}

} /* namespace alphabet */

namespace {

auto valuePrinter = registration::ValuePrinterRegister < alphabet::WildcardSymbol > ( );

} /* namespace */
