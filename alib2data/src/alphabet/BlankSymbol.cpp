/*
 * BlankSymbol.cpp
 *
 *  Created on: Mar 26, 2013
 *      Author: Jan Travnicek
 */

#include "BlankSymbol.h"

#include <object/Object.h>

#include <registration/ValuePrinterRegistration.hpp>

namespace alphabet {

BlankSymbol::BlankSymbol() = default;

std::ostream & operator << ( std::ostream & out, const BlankSymbol & ) {
	return out << "(Blank symbol)";
}

BlankSymbol::operator std::string ( ) const {
	return BlankSymbol::instance < std::string > ( );
}

} /* namespace alphabet */

namespace {

auto valuePrinter = registration::ValuePrinterRegister < alphabet::BlankSymbol > ( );

} /* namespace */
