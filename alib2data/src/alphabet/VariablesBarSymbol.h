/*
 * VariablesBarSymbol.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: Apr 10, 2013
 *      Author: Jan Travnicek
 */

#ifndef VARIABLES_BAR_SYMBOL_H_
#define VARIABLES_BAR_SYMBOL_H_

#include <common/ranked_symbol.hpp>
#include <object/Object.h>

namespace alphabet {

/**
 * \brief
 * Represents variables bar symbol used in tree linearization.
 */
class VariablesBarSymbol {
public:
	/**
	 * \brief
	 * Creates a new instance of the symbol.
	 */
	explicit VariablesBarSymbol ( );

	/**
	 * The three way comparison implementation
	 *
	 * \param other the other instance
	 *
	 * \returns the ordering between this object and the @p other.
	 */
	std::strong_ordering operator <=> ( const VariablesBarSymbol & ) const {
		return std::strong_ordering::equal;
	}

	/**
	 * The equality comparison implementation.
	 *
	 * \param other the other object to compare with.
	 *
	 * \returns true if this and other objects are equal, false othervise
	 */
	bool operator == ( const VariablesBarSymbol & ) const {
		return true;
	}

	/**
	 * Print this object as raw representation to ostream.
	 *
	 * \param os ostream where to print
	 * \param instance object to print
	 *
	 * \returns modified output stream
	 */
	friend std::ostream & operator << ( std::ostream & out, const VariablesBarSymbol & instance );

	/**
	 * Casts this instance to as compact as possible string representation.
	 *
	 * \returns string representation of the object
	 */
	explicit operator std::string ( ) const;

	/**
	 * \brief Factory for the symbol construction of the symbol based on given type.
	 */
	template < typename Base >
	static inline typename std::enable_if <   std::is_integral < Base >::value, Base >::type instance ( );

	/**
	 * \brief Factory for the symbol construction of the symbol based on given type.
	 */
	template < typename Base >
	static inline typename std::enable_if < ! std::is_integral < Base >::value, Base >::type instance ( );
};

template < typename Base >
inline typename std::enable_if < std::is_integral < Base >::value, Base >::type VariablesBarSymbol::instance ( ) {
	return '!';
}

template < >
inline object::Object VariablesBarSymbol::instance < object::Object > ( ) {
	return object::Object ( VariablesBarSymbol ( ) );
}

template < >
inline std::string VariablesBarSymbol::instance < std::string > ( ) {
	return std::string ( 1, VariablesBarSymbol::instance < char > ( ) );
}

template < >
inline VariablesBarSymbol VariablesBarSymbol::instance < VariablesBarSymbol > ( ) {
	return VariablesBarSymbol ( );
}

// TODO make partially specialised when needed by classes or variables, functions can be partially specialsed
template < >
inline common::ranked_symbol < > VariablesBarSymbol::instance < common::ranked_symbol < > > ( ) {
	return common::ranked_symbol < > ( VariablesBarSymbol::instance < DefaultSymbolType > ( ), 0u );
}

} /* namespace alphabet */

#endif /* VARIABLES_BAR_SYMBOL_H_ */
