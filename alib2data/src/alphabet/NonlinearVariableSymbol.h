/*
 * NonlinearVariableSymbol.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: Apr 10, 2013
 *      Author: Jan Travnicek
 */

#ifndef NONLINEAR_VARIABLE_SYMBOL_H_
#define NONLINEAR_VARIABLE_SYMBOL_H_

#include <core/normalize.hpp>
#include <alphabet/common/SymbolNormalize.h>

namespace alphabet {

/**
 * \brief
 * Represents the nonlinear variable symbol used in a nonlinear tree pattern.
 */
template < class SymbolType = DefaultSymbolType >
class NonlinearVariableSymbol {
	/**
	 * \brief the symbol of the nonlinear variable.
	 */
	SymbolType m_symbol;

public:
	/**
	 * \brief
	 * Creates a new instance of the nonlinear variable with some underlying base symbol.
	 */
	explicit NonlinearVariableSymbol ( SymbolType symbol );

	/**
	 * Getter of the nonlinear variable's symbol
	 *
	 * \return the symbol of the nonlinear variable
	 */
	const SymbolType & getSymbol ( ) const &;

	/**
	 * Getter of the nonlinear variable's symbol
	 *
	 * \return the symbol of the nonlinear variable
	 */
	SymbolType && getSymbol ( ) &&;

	/**
	 * The three way comparison implementation
	 *
	 * \param other the other instance
	 *
	 * \returns the ordering between this object and the @p other.
	 */
	auto operator <=> ( const NonlinearVariableSymbol & other ) const {
		return m_symbol <=> other.m_symbol;
	}

	/**
	 * The equality comparison implementation.
	 *
	 * \param other the other object to compare with.
	 *
	 * \returns true if this and other objects are equal, false othervise
	 */
	bool operator == ( const NonlinearVariableSymbol & other ) const {
		return m_symbol == other.m_symbol;
	}

	/**
	 * Print this object as raw representation to ostream.
	 *
	 * \param out ostream where to print
	 * \param instance object to print
	 *
	 * \returns modified output stream
	 */
	friend std::ostream & operator << ( std::ostream & out, const NonlinearVariableSymbol < SymbolType > & instance ) {
		return out << "(NonlinearVariableSymbol " << instance.getSymbol ( ) << ")";
	}

	/**
	 * Casts this instance to as compact as possible string representation.
	 *
	 * \returns string representation of the object
	 */
	explicit operator std::string ( ) const;
};

template < class SymbolType >
NonlinearVariableSymbol < SymbolType >::NonlinearVariableSymbol ( SymbolType symbol ) : m_symbol ( std::move ( symbol ) ) {
}

template < class SymbolType >
const SymbolType & NonlinearVariableSymbol < SymbolType >::getSymbol ( ) const & {
	return m_symbol;
}

template < class SymbolType >
SymbolType && NonlinearVariableSymbol < SymbolType >::getSymbol ( ) && {
	return std::move ( m_symbol );
}

template < class SymbolType >
NonlinearVariableSymbol < SymbolType >::operator std::string ( ) const {
	return "$" + ext::to_string ( m_symbol );
}

} /* namespace alphabet */

namespace core {

template < class SymbolType >
struct normalize < alphabet::NonlinearVariableSymbol < SymbolType > > {
	static alphabet::NonlinearVariableSymbol < > eval ( alphabet::NonlinearVariableSymbol < SymbolType > && value ) {
		return alphabet::NonlinearVariableSymbol < > ( alphabet::SymbolNormalize::normalizeSymbol ( std::move ( value ).getSymbol ( ) ) );
	}
};

} /* namespace core */

#endif /* NONLINEAR_VARIABLE_SYMBOL_H_ */
