/*
 * NonContractingGrammar.cpp
 *
 *  Created on: Nov 17, 2013
 *      Author: Jan Travnicek
 */

#include "NonContractingGrammar.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>

template class grammar::NonContractingGrammar < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < grammar::NonContractingGrammar < > > ( );

} /* namespace */
