/*
 * LeftRG.cpp
 *
 *  Created on: Nov 17, 2013
 *      Author: Jan Travnicek
 */

#include "LeftRG.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>

template class grammar::LeftRG < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < grammar::LeftRG < > > ( );

} /* namespace */
