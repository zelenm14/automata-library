/*
 * LG.cpp
 *
 *  Created on: Nov 17, 2013
 *      Author: Jan Travnicek
 */

#include "LG.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>

template class grammar::LG < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < grammar::LG < > > ( );

} /* namespace */
