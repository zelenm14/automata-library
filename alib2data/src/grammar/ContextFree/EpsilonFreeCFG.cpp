/*
 * EpsilonFreeCFG.cpp
 *
 *  Created on: Nov 17, 2013
 *      Author: Jan Travnicek
 */

#include "EpsilonFreeCFG.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>

template class grammar::EpsilonFreeCFG < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < grammar::EpsilonFreeCFG < > > ( );

} /* namespace */
