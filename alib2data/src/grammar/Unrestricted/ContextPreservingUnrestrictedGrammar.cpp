/*
 * ContextPreservingUnrestrictedGrammar.cpp
 *
 *  Created on: Nov 17, 2013
 *      Author: Jan Travnicek
 */

#include "ContextPreservingUnrestrictedGrammar.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>

template class grammar::ContextPreservingUnrestrictedGrammar < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < grammar::ContextPreservingUnrestrictedGrammar < > > ( );

} /* namespace */
