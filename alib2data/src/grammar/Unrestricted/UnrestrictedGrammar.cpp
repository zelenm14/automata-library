/*
 * UnrestrictedGrammar.cpp
 *
 *  Created on: Nov 17, 2013
 *      Author: Jan Travnicek
 */

#include "UnrestrictedGrammar.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>

template class grammar::UnrestrictedGrammar < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < grammar::UnrestrictedGrammar < > > ( );

} /* namespace */
