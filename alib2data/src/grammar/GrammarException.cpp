/*
 * GrammarException.cpp
 *
 *  Created on: Apr 1, 2013
 *      Author: Martin Zak
 */

#include "GrammarException.h"

namespace grammar {

GrammarException::GrammarException(const std::string& cause) :
		CommonException(cause) {
}

} /* namespace grammar */
