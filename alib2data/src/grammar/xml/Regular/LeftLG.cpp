/*
 * LeftLG.cpp
 *
 *  Created on: Nov 17, 2013
 *      Author: Jan Travnicek
 */

#include "LeftLG.h"
#include <grammar/Grammar.h>
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < grammar::LeftLG < > > ( );
auto xmlRead = registration::XmlReaderRegister < grammar::LeftLG < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, grammar::LeftLG < > > ( );

} /* namespace */
