/*
 * NonContractingGrammar.cpp
 *
 *  Created on: Nov 17, 2013
 *      Author: Jan Travnicek
 */

#include "NonContractingGrammar.h"
#include <grammar/Grammar.h>
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < grammar::NonContractingGrammar < > > ( );
auto xmlRead = registration::XmlReaderRegister < grammar::NonContractingGrammar < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, grammar::NonContractingGrammar < > > ( );

} /* namespace */
