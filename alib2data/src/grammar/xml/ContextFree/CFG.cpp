/*
 * CFG.cpp
 *
 *  Created on: Nov 17, 2013
 *      Author: Jan Travnicek
 */

#include "CFG.h"
#include <grammar/Grammar.h>
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < grammar::CFG < > > ( );
auto xmlRead = registration::XmlReaderRegister < grammar::CFG < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, grammar::CFG < > > ( );

} /* namespace */
