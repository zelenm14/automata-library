/*
 * GNF.cpp
 *
 *  Created on: Nov 17, 2013
 *      Author: Jan Travnicek
 */

#include "GNF.h"
#include <grammar/Grammar.h>
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < grammar::GNF < > > ( );
auto xmlRead = registration::XmlReaderRegister < grammar::GNF < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, grammar::GNF < > > ( );

} /* namespace */
