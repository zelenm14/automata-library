/*
 * RankedTree.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: Nov 23, 2013
 *      Author: Stepan Plachy
 */

#ifndef RANKED_TREE_H_
#define RANKED_TREE_H_

#include <common/DefaultSymbolType.h>

namespace tree {

template < class SymbolType = DefaultSymbolType >
class RankedTree;

} /* namespace tree */

#include <sstream>

#include <alib/string>
#include <alib/set>
#include <alib/tree>
#include <alib/iostream>
#include <alib/algorithm>

#include <core/components.hpp>
#include <common/ranked_symbol.hpp>

#include <tree/TreeException.h>
#include <tree/common/TreeAuxiliary.h>

#include <core/normalize.hpp>
#include <tree/common/TreeNormalize.h>

#include "../unranked/UnrankedTree.h"
#include "PostfixRankedTree.h"
#include "PrefixRankedTree.h"

namespace tree {

class GeneralAlphabet;

/**
 * \brief
 * Tree structure represented in its natural representation. The representation is so called ranked, therefore it consists of ranked symbols. The rank of the ranked symbol needs to be compatible with unsigned integer.

 * \details
 * T = (A, C),
 * A (Alphabet) = finite set of ranked symbols,
 * C (Content) = tree in its natural representation
 *
 * \tparam SymbolType used for the symbol part of the ranked symbol
 */
template < class SymbolType >
class RankedTree final : public core::Components < RankedTree < SymbolType >, ext::set < common::ranked_symbol < SymbolType > >, component::Set, GeneralAlphabet > {
	/**
	 * Natural representation of the tree content.
	 */
	ext::tree < common::ranked_symbol < SymbolType > > m_content;

	/**
	 * Checks that symbols of the tree are present in the alphabet.
	 *
	 * \throws TreeException when some symbols of the tree representation are not present in the alphabet
	 *
	 * \param data the tree in its natural representation
	 */
	void checkAlphabet ( const ext::tree < common::ranked_symbol < SymbolType > > & data ) const;

	/**
	 * Checks that the rank of each symbol of a tree node corresponds to the number of child nodes of that same node.
	 *
	 * \throws TreeException when some nodes of a tree have different number of children than the rank of their label states
	 *
	 * \param data the tree in its natural representation
	 */
	void checkArities ( const ext::tree < common::ranked_symbol < SymbolType > > & data ) const;

public:
	/**
	 * \brief Creates a new instance of the tree with concrete alphabet and content.
	 *
	 * \param alphabet the initial alphabet of the tree
	 * \param tree the initial tree in its natural representation
	 */
	explicit RankedTree ( ext::set < common::ranked_symbol < SymbolType > > alphabet, ext::tree < common::ranked_symbol < SymbolType > > tree );

	/**
	 * \brief Creates a new instance of the tree based on the content, the alphabet is implicitly created from the content.
	 *
	 * \param tree the initial tree in its natural representation
	 */
	explicit RankedTree ( ext::tree < common::ranked_symbol < SymbolType > > tree );

	/**
	 * \brief Creates a new instance of the tree based on UnrankedTree, the alphabet is created from the content of the UnrankedTree. The alphabet is created since ranks cannot be deduced.
	 *
	 * \param other the tree represented as UnrankedTree
	 */
	explicit RankedTree ( const UnrankedTree < SymbolType > & other );

	/**
	 * \brief Creates a new instance of the tree based on the PostfixRankedTree. The natural representation is recreated from postfix representation.
	 *
	 * \param other PostfixRankedTree representation of a tree.
	 */
	explicit RankedTree ( const PostfixRankedTree < SymbolType > & other );

	/**
	 * \brief Creates a new instance of the tree based on the PrefixRankedTree. The natural representation is recreated from prefix representation.
	 *
	 * \param other PrefixRankedTree representation of a tree.
	 */
	explicit RankedTree ( const PrefixRankedTree < SymbolType > & other );

	/**
	 * Getter of the alphabet.
	 *
	 * \returns the alphabet of the tree
	 */
	const ext::set < common::ranked_symbol < SymbolType > > & getAlphabet ( ) const & {
		return this->template accessComponent < GeneralAlphabet > ( ).get ( );
	}

	/**
	 * Getter of the alphabet.
	 *
	 * \returns the alphabet of the tree
	 */
	ext::set < common::ranked_symbol < SymbolType > > && getAlphabet ( ) && {
		return std::move ( this->template accessComponent < GeneralAlphabet > ( ).get ( ) );
	}

	/**
	 * Adder of an alphabet symbols.
	 *
	 * \param symbols the new symbols to be added to the alphabet
	 */
	void extendAlphabet ( const ext::set < common::ranked_symbol < SymbolType > > & symbols ) {
		this->template accessComponent < GeneralAlphabet > ( ).add ( symbols );
	}

	/**
	 * Getter of the tree representation.
	 *
	 * \return the natural representation of the tree.
	 */
	const ext::tree < common::ranked_symbol < SymbolType > > & getContent ( ) const &;

	/**
	 * Getter of the tree representation.
	 *
	 * \return the natural representation of the tree.
	 */
	ext::tree < common::ranked_symbol < SymbolType > > && getContent ( ) &&;

	/**
	 * Setter of the representation of the tree.
	 *
	 * \throws TreeException in same situations as checkAlphabet and checkArities
	 *
	 * \param tree new representation of the tree.
	 */
	void setTree ( ext::tree < common::ranked_symbol < SymbolType > > tree );

	/**
	 * The three way comparison implementation
	 *
	 * \param other the other instance
	 *
	 * \returns the ordering between this object and the @p other.
	 */
	auto operator <=> ( const RankedTree & other ) const {
		return std::tie ( m_content, getAlphabet() ) <=> std::tie ( other.m_content, other.getAlphabet() );
	}

	/**
	 * The equality comparison implementation.
	 *
	 * \param other the other object to compare with.
	 *
	 * \returns true if this and other objects are equal, false othervise
	 */
	bool operator == ( const RankedTree & other ) const {
		return std::tie ( m_content, getAlphabet() ) == std::tie ( other.m_content, other.getAlphabet() );
	}

	/**
	 * Print this object as raw representation to ostream.
	 *
	 * \param out ostream where to print
	 * \param instance object to print
	 *
	 * \returns modified output stream
	 */
	friend std::ostream & operator << ( std::ostream & out, const RankedTree & instance ) {
		out << "(RankedTree ";
		out << "alphabet = " << instance.getAlphabet ( );
		out << "content = " << instance.getContent ( );
		out << ")";
		return out;
	}

	/**
	 * Casts this instance to as compact as possible string representation.
	 *
	 * \returns string representation of the object
	 */
	explicit operator std::string ( ) const;

	/**
	 * Hierarchical printer of the tree.
	 *
	 * \param os the output stream destination of the print
	 */
	void nicePrint ( std::ostream & os ) const;
};

template < class SymbolType >
RankedTree < SymbolType >::RankedTree ( ext::set < common::ranked_symbol < SymbolType > > alphabet, ext::tree < common::ranked_symbol < SymbolType > > tree ) : core::Components < RankedTree, ext::set < common::ranked_symbol < SymbolType > >, component::Set, GeneralAlphabet > ( std::move ( alphabet ) ), m_content ( std::move ( tree ) ) {
	checkAlphabet ( m_content );
	checkArities ( m_content );
}

template < class SymbolType >
RankedTree < SymbolType >::RankedTree ( ext::tree < common::ranked_symbol < SymbolType > > tree ) : RankedTree ( ext::set < common::ranked_symbol < SymbolType > > ( tree.prefix_begin ( ), tree.prefix_end ( ) ), tree ) {
}

template < class SymbolType >
RankedTree < SymbolType >::RankedTree ( const UnrankedTree < SymbolType > & other ) : RankedTree ( TreeAuxiliary::unrankedToRanked < SymbolType > ( other.getContent ( ) ) ) {
}

template < class SymbolType >
RankedTree < SymbolType >::RankedTree ( const PostfixRankedTree < SymbolType > & other) : RankedTree ( TreeAuxiliary::postfixToTree < SymbolType > ( other.getContent ( ) ) ) {
}

template < class SymbolType >
RankedTree < SymbolType >::RankedTree ( const PrefixRankedTree < SymbolType > & other) : RankedTree ( TreeAuxiliary::prefixToTree < SymbolType > ( other.getContent ( ) ) ) {
}

template < class SymbolType >
const ext::tree < common::ranked_symbol < SymbolType > > & RankedTree < SymbolType >::getContent ( ) const & {
	return m_content;
}

template < class SymbolType >
ext::tree < common::ranked_symbol < SymbolType > > && RankedTree < SymbolType >::getContent ( ) && {
	return std::move ( m_content );
}

template < class SymbolType >
void RankedTree < SymbolType >::checkAlphabet ( const ext::tree < common::ranked_symbol < SymbolType > > & data ) const {
	ext::set < common::ranked_symbol < SymbolType > > minimalAlphabet ( data.prefix_begin ( ), data.prefix_end ( ) );
	ext::set < common::ranked_symbol < SymbolType > > unknownSymbols;
	std::set_difference ( minimalAlphabet.begin ( ), minimalAlphabet.end ( ), getAlphabet().begin ( ), getAlphabet().end ( ), std::inserter ( unknownSymbols, unknownSymbols.end ( ) ) );

	if ( !unknownSymbols.empty ( ) )
		throw exception::CommonException ( "Input symbols not in the alphabet." );
}

template < class SymbolType >
void RankedTree < SymbolType >::checkArities ( const ext::tree < common::ranked_symbol < SymbolType > > & data ) const {
	if ( ( size_t ) data.getData ( ).getRank ( ) != data.getChildren ( ).size ( ) )
		throw exception::CommonException ( "Invalid rank." );

	for ( const ext::tree < common::ranked_symbol < SymbolType > > & child : data )
		checkArities ( child );
}

template < class SymbolType >
void RankedTree < SymbolType >::setTree ( ext::tree < common::ranked_symbol < SymbolType > > tree ) {
	checkAlphabet ( tree );
	checkArities ( tree );

	this->m_content = std::move ( tree );
}

template < class SymbolType >
void RankedTree < SymbolType >::nicePrint ( std::ostream & os ) const {
	m_content.nicePrint ( os );
}

template < class SymbolType >
RankedTree < SymbolType >::operator std::string ( ) const {
	std::stringstream ss;
	ss << * this;
	return ss.str ( );
}

} /* namespace tree */

namespace core {

/**
 * Helper class specifying constraints for the tree's internal alphabet component.
 *
 * \tparam SymbolType used for the symbol part of the ranked symbols of the alphabet of the pattern.
 */
template < class SymbolType >
class SetConstraint< tree::RankedTree < SymbolType >, common::ranked_symbol < SymbolType >, tree::GeneralAlphabet > {
public:
	/**
	 * Returns true if the symbol is still used in the tree.
	 *
	 * \param tree the tested tree
	 * \param symbol the tested symbol
	 *
	 * \returns true if the symbol is used, false othervise
	 */
	static bool used ( const tree::RankedTree < SymbolType > & tree, const common::ranked_symbol < SymbolType > & symbol ) {
		const ext::tree < common::ranked_symbol < SymbolType > > & m_content = tree.getContent ( );
		return std::find(m_content.prefix_begin(), m_content.prefix_end(), symbol) != m_content.prefix_end();
	}

	/**
	 * Returns true as all symbols are possibly available to be in an alphabet.
	 *
	 * \param tree the tested tree
	 * \param symbol the tested symbol
	 *
	 * \returns true
	 */
	static bool available ( const tree::RankedTree < SymbolType > &, const common::ranked_symbol < SymbolType > & ) {
		return true;
	}

	/**
	 * All symbols are valid as symbols of an alphabet.
	 *
	 * \param tree the tested tree
	 * \param symbol the tested symbol
	 */
	static void valid ( const tree::RankedTree < SymbolType > &, const common::ranked_symbol < SymbolType > & ) {
	}
};

/**
 * Helper for normalisation of types specified by templates used as internal datatypes of symbols.
 *
 * \returns new instance of the tree with default template parameters or unmodified instance if the template parameters were already default ones
 */
template < class SymbolType >
struct normalize < tree::RankedTree < SymbolType > > {
	static tree::RankedTree < > eval ( tree::RankedTree < SymbolType > && value ) {
		ext::set < common::ranked_symbol < DefaultSymbolType > > alphabet = alphabet::SymbolNormalize::normalizeRankedAlphabet ( std::move ( value ).getAlphabet ( ) );
		ext::tree < common::ranked_symbol < DefaultSymbolType > > content = tree::TreeNormalize::normalizeRankedTree ( std::move ( value ).getContent ( ) );

		return tree::RankedTree < > ( std::move ( alphabet ), std::move ( content ) );
	}
};

} /* namespace core */

extern template class tree::RankedTree < >;

#endif /* RANKED_TREE_H_ */
