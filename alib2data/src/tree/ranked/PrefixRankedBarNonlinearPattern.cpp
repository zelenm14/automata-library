/*
 * PrefixRankedBarNonlinearPattern.cpp
 *
 *  Created on: Jul 20, 2016
 *      Author: Jan Travnicek
 */

#include "PrefixRankedBarNonlinearPattern.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>

template class tree::PrefixRankedBarNonlinearPattern < >;

namespace {

auto components = registration::ComponentRegister < tree::PrefixRankedBarNonlinearPattern < > > ( );

auto valuePrinter = registration::ValuePrinterRegister < tree::PrefixRankedBarNonlinearPattern < > > ( );

auto PrefixRankedBarNonlinearPatternFromRankedTree = registration::CastRegister < tree::PrefixRankedBarNonlinearPattern < >, tree::RankedTree < > > ( );
auto PrefixRankedBarNonlinearPatternFromRankedPattern = registration::CastRegister < tree::PrefixRankedBarNonlinearPattern < >, tree::RankedPattern < > > ( );
auto PrefixRankedBarNonlinearPatternFromRankedNonlinearPattern = registration::CastRegister < tree::PrefixRankedBarNonlinearPattern < >, tree::RankedNonlinearPattern < > > ( );
auto PrefixRankedBarNonlinearPatternFromPrefixRankedBarTree = registration::CastRegister < tree::PrefixRankedBarNonlinearPattern < >, tree::PrefixRankedBarTree < > > ( );
auto PrefixRankedBarNonlinearPatternFromPrefixRankedBarPattern = registration::CastRegister < tree::PrefixRankedBarNonlinearPattern < >, tree::PrefixRankedBarPattern < > > ( );

auto LinearStringFromPrefixRankedBarNonlinearPattern = registration::CastRegister < string::LinearString < common::ranked_symbol < > >, tree::PrefixRankedBarNonlinearPattern < > > ( );

} /* namespace */
