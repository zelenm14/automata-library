/*
 * PostfixRankedTree.cpp
 *
 *  Created on: May 4, 2017
 *      Author: Aleksandr Shatrovskii
 */

#include "PostfixRankedTree.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>

template class tree::PostfixRankedTree < >;

namespace {

auto components = registration::ComponentRegister < tree::PostfixRankedTree < > > ( );

auto valuePrinter = registration::ValuePrinterRegister < tree::PostfixRankedTree < > > ( );

auto PostfixRankedTreeFromRankedTree = registration::CastRegister < tree::PostfixRankedTree < >, tree::RankedTree < > > ( );

auto LinearStringFromPostfixRankedTree = registration::CastRegister < string::LinearString < common::ranked_symbol < > >, tree::PostfixRankedTree < > > ( );

} /* namespace */
