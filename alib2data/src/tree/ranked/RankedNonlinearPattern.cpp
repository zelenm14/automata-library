/*
 * RankedNonlinearPattern.cpp
 *
 *  Created on: Nov 23, 2013
 *      Author: Jan Travnicek
 */

#include "RankedNonlinearPattern.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>

template class tree::RankedNonlinearPattern < >;

namespace {

auto components = registration::ComponentRegister < tree::RankedNonlinearPattern < > > ( );

auto valuePrinter = registration::ValuePrinterRegister < tree::RankedNonlinearPattern < > > ( );

} /* namespace */
