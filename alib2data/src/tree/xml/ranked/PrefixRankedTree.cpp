/*
 * PrefixRankedTree.cpp
 *
 *  Created on: Nov 23, 2013
 *      Author: Jan Travnicek
 */

#include "PrefixRankedTree.h"
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < tree::PrefixRankedTree < > > ( );
auto xmlRead = registration::XmlReaderRegister < tree::PrefixRankedTree < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, tree::PrefixRankedTree < > > ( );

} /* namespace */
