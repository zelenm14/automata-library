/*
 * RankedNonlinearPattern.cpp
 *
 *  Created on: Nov 23, 2013
 *      Author: Jan Travnicek
 */

#include "RankedNonlinearPattern.h"
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < tree::RankedNonlinearPattern < > > ( );
auto xmlRead = registration::XmlReaderRegister < tree::RankedNonlinearPattern < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, tree::RankedNonlinearPattern < > > ( );

} /* namespace */
