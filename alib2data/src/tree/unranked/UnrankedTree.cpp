/*
 * UnrankedTree.cpp
 *
 *  Created on: Nov 23, 2013
 *      Author: Stepan Plachy
 */

#include "UnrankedTree.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>

template class tree::UnrankedTree < >;

namespace {

auto components = registration::ComponentRegister < tree::UnrankedTree < > > ( );

auto valuePrinter = registration::ValuePrinterRegister < tree::UnrankedTree < > > ( );

auto UnrankedTreeFromRankedTree = registration::CastRegister < tree::UnrankedTree < >, tree::RankedTree < > > ( );

} /* namespace */
