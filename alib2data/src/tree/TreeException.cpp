/*
 * TreeException.cpp
 *
 *  Created on: Nov 16, 2014
 *      Author: Stepan Plachy
 */

#include "TreeException.h"

namespace tree {

TreeException::TreeException(const std::string& cause) :
		CommonException(cause) {
}

} /* namespace tree */
