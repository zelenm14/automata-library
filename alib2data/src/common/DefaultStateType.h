/*
 * DefaultStateType.h
 *
 *  Created on: Nov 29, 2016
 *      Author: Jan Travnicek
 */

#ifndef DEFAULT_STATE_TYPE_H_
#define DEFAULT_STATE_TYPE_H_

#include <object/Object.h>

typedef object::Object DefaultStateType;

#endif /* DEFAULT_STATE_TYPE_H_ */
