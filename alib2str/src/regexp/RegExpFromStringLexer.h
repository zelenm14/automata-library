/*
 * RegExpFromStringLexer.h
 *
 *  Created on: Nov 23, 2013
 *      Author: Jan Travnicek
 */

#ifndef REG_EXP_FROM_STRING_LEXER_H_
#define REG_EXP_FROM_STRING_LEXER_H_

#include <alib/string>
#include <alib/istream>
#include <common/lexer.hpp>

namespace regexp {

class RegExpFromStringLexer : public ext::Lexer < RegExpFromStringLexer > {
public:
	enum class TokenType {
		LPAR,
		RPAR,
		PLUS,
		STAR,
		EPS,
		EMPTY,
		TEOF,
		ERROR
	};

	static Token next(std::istream& input);
};

} /* namespace regexp */

#endif /* REG_EXP_FROM_STRING_LEXER_H_ */
