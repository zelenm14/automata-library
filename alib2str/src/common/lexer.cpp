/*
 * lexer.cpp
 *
 * Created on: 4. 3. 2019
 * Author: Jan Travnicek
 */

#include "lexer.hpp"
#include <alib/iterator>
#include <alib/string>
#include <alib/istream>

namespace ext {

void BasicLexer::putback ( std::istream & input, const std::string & data ) {
	input.clear ( );

	for ( char character : ext::make_reverse ( data ) )
		input.putback ( character );
}

bool BasicLexer::test ( std::istream & input, const std::string & value ) {
	if ( testAndConsume ( input, value ) ) {
		putback ( input, value );
		return true;
	} else {
		return false;
	}
}

void BasicLexer::consume ( std::istream & input, const std::string & value ) {
	if ( ! testAndConsume ( input, value ) )
		throw std::runtime_error ( "Can't consume " + value + " from input stream." );
}

bool BasicLexer::testAndConsume ( std::istream & input, const std::string & value ) {
	return ( bool ) ( input >> ext::string ( value ) );
}

} /* namespace ext */
