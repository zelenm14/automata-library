/*
 * CFG.cpp
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#include <alib/pair>

#include <object/Object.h>
#include <primitive/string/Unsigned.h>
#include <container/string/ObjectsPair.h>
#include <container/string/ObjectsSet.h>
#include <container/string/ObjectsVariant.h>
#include <alphabet/string/RankedSymbol.h>
#include <alphabet/string/SymbolOrEpsilon.h>

#include <registration/StringRegistration.hpp>

namespace {

auto stringWriteGroup1 = registration::StringWriterRegisterTypeInGroup < object::Object, ext::pair < ext::set < ext::pair < object::Object, object::Object > >, common::symbol_or_epsilon < object::Object > > > ( );
auto stringWriteGroup2 = registration::StringWriterRegisterTypeInGroup < object::Object, ext::set < ext::pair < object::Object, object::Object > > > ( );
auto stringWriteGroup3 = registration::StringWriterRegisterTypeInGroup < object::Object, ext::pair < object::Object, unsigned > > ( );
auto stringWriteGroup4 = registration::StringWriterRegisterTypeInGroup < object::Object, ext::pair < unsigned, unsigned > > ( );
auto stringWriteGroup5 = registration::StringWriterRegisterTypeInGroup < object::Object, ext::set < common::ranked_symbol < ext::pair < object::Object, unsigned int > > > > ( );

} /* namespace */
