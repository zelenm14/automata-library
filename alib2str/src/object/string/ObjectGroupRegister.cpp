/*
 * ObjectGroupGegister.cpp
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#include <object/Object.h>

#include <registration/StringRegistration.hpp>

namespace {

auto stringReaderGroup = registration::StringReaderGroupRegister < object::Object > ( );

} /* namespace */
