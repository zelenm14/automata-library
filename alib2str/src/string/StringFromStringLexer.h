/*
 * StringFromStringLexer.h
 *
 *  Created on: Nov 23, 2013
 *      Author: Jan Travnicek
 */

#ifndef STRING_FROM_STRING_LEXER_H_
#define STRING_FROM_STRING_LEXER_H_

#include <alib/string>
#include <alib/istream>
#include <common/lexer.hpp>

namespace string {

class StringFromStringLexer : public ext::Lexer < StringFromStringLexer > {
public:
	enum class TokenType {
		LESS,
		GREATER,
		QUOTE,
		TERM,
		TEOF,
		ERROR
	};

	static Token next(std::istream& input);
};

} /* namespace string */

#endif /* STRING_FROM_STRING_LEXER_H_ */
