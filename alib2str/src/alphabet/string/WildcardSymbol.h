/*
 * WildcardSymbol.h
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#ifndef _STRING_WILDCARD_SYMBOL_H_
#define _STRING_WILDCARD_SYMBOL_H_

#include <alphabet/WildcardSymbol.h>
#include <core/stringApi.hpp>

namespace core {

template < >
struct stringApi < alphabet::WildcardSymbol > {
	static alphabet::WildcardSymbol parse ( std::istream & input );
	static bool first ( std::istream & input );
	static void compose ( std::ostream & output, const alphabet::WildcardSymbol & symbol );
};

} /* namespace core */

#endif /* _STRING_WILDCARD_SYMBOL_H_ */
