/*
 * InitialSymbol.cpp
 *
 * Created on: Nov 21, 2017
 * Author: Tomas Pecka
 */

#include "InitialSymbol.h"
#include <alphabet/InitialSymbol.h>
#include <object/Object.h>

#include <registration/StringRegistration.hpp>

namespace core {

alphabet::InitialSymbol stringApi < alphabet::InitialSymbol >::parse ( std::istream & ) {
	throw exception::CommonException ( "parsing InitialSymbol from string not implemented" );
}

bool stringApi < alphabet::InitialSymbol >::first ( std::istream & ) {
	return false;
}

void stringApi < alphabet::InitialSymbol >::compose ( std::ostream & output, const alphabet::InitialSymbol & ) {
	output << "#I";
}

} /* namespace core */

namespace {

auto stringWrite = registration::StringWriterRegister < alphabet::InitialSymbol > ( );

auto stringWriteGroup = registration::StringWriterRegisterTypeInGroup < object::Object, alphabet::InitialSymbol > ( );

} /* namespace */
