/*
 * BarSymbol.h
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#ifndef _STRING_BAR_SYMBOL_H_
#define _STRING_BAR_SYMBOL_H_

#include <alphabet/BarSymbol.h>
#include <core/stringApi.hpp>

namespace core {

template < >
struct stringApi < alphabet::BarSymbol > {
	static alphabet::BarSymbol parse ( std::istream & input );
	static bool first ( std::istream & input );
	static void compose ( std::ostream & output, const alphabet::BarSymbol & symbol );
};

} /* namespace core */

#endif /* _STRING_BAR_SYMBOL_H_ */
