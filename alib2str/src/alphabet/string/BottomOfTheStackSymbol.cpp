/*
 * BottomOfTheStackSymbol.cpp
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#include "BottomOfTheStackSymbol.h"
#include <alphabet/BottomOfTheStackSymbol.h>
#include <object/Object.h>

#include <registration/StringRegistration.hpp>

namespace core {

alphabet::BottomOfTheStackSymbol stringApi < alphabet::BottomOfTheStackSymbol >::parse ( std::istream & ) {
	throw exception::CommonException("parsing BottomOfTheStackSymbol from string not implemented");
}

bool stringApi < alphabet::BottomOfTheStackSymbol >::first ( std::istream & ) {
	return false;
}

void stringApi < alphabet::BottomOfTheStackSymbol >::compose ( std::ostream & output, const alphabet::BottomOfTheStackSymbol & ) {
	output << "#T";
}

} /* namespace core */

namespace {

auto stringWrite = registration::StringWriterRegister < alphabet::BottomOfTheStackSymbol > ( );

auto stringWriteGroup = registration::StringWriterRegisterTypeInGroup < object::Object, alphabet::BottomOfTheStackSymbol > ( );

} /* namespace */
