/*
 * StartSymbol.h
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#ifndef _STRING_START_SYMBOL_H_
#define _STRING_START_SYMBOL_H_

#include <alphabet/StartSymbol.h>
#include <core/stringApi.hpp>

namespace core {

template < >
struct stringApi < alphabet::StartSymbol > {
	static alphabet::StartSymbol parse ( std::istream & input );
	static bool first ( std::istream & input );
	static void compose ( std::ostream & output, const alphabet::StartSymbol & symbol );
};

} /* namespace core */

#endif /* _STRING_START_SYMBOL_H_ */
