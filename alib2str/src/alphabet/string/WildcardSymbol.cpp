/*
 * WildcardSymbol.cpp
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#include "WildcardSymbol.h"
#include <alphabet/WildcardSymbol.h>
#include <object/Object.h>

#include <registration/StringRegistration.hpp>

namespace core {

alphabet::WildcardSymbol stringApi < alphabet::WildcardSymbol >::parse ( std::istream & ) {
	throw exception::CommonException("parsing WildcardSymbol from string not implemented");
}

bool stringApi < alphabet::WildcardSymbol >::first ( std::istream & ) {
	return false;
}

void stringApi < alphabet::WildcardSymbol >::compose ( std::ostream & output, const alphabet::WildcardSymbol & ) {
	output << "#S";
}

} /* namespace core */

namespace {

auto stringWrite = registration::StringWriterRegister < alphabet::WildcardSymbol > ( );

auto stringWriteGroup = registration::StringWriterRegisterTypeInGroup < object::Object, alphabet::WildcardSymbol > ( );

} /* namespace */
