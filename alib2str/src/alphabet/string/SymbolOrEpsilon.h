/*
 * SymbolOrEpsilon.h
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#ifndef _STRING_SYMBOL_OR_EPSILON_H_
#define _STRING_SYMBOL_OR_EPSILON_H_

#include <common/symbol_or_epsilon.hpp>
#include <core/stringApi.hpp>

namespace core {

template < class SymbolType >
struct stringApi < common::symbol_or_epsilon < SymbolType > > {
	static common::symbol_or_epsilon < SymbolType > parse ( std::istream & input );
	static bool first ( std::istream & input );
	static void compose ( std::ostream & output, const common::symbol_or_epsilon < SymbolType > & symbol );
};

template < class SymbolType >
common::symbol_or_epsilon < SymbolType > stringApi < common::symbol_or_epsilon < SymbolType > >::parse ( std::istream & ) {
	throw exception::CommonException("Parsing of symbol or epsilon from string not implemented.");
}

template < class SymbolType >
bool stringApi < common::symbol_or_epsilon < SymbolType > >::first ( std::istream & ) {
	return false;
}

template < class SymbolType >
void stringApi < common::symbol_or_epsilon < SymbolType > >::compose ( std::ostream & output, const common::symbol_or_epsilon < SymbolType > & symbol ) {
	if ( symbol.is_epsilon ( ) )
		output << "#E";
	else
		core::stringApi < SymbolType >::compose ( output, symbol.getSymbol ( ) );
}

} /* namespace core */

#endif /* _STRING_SYMBOL_OR_EPSILON_H_ */
