/*
 * Bool.cpp
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#include "Bool.h"
#include <object/Object.h>

#include <registration/StringRegistration.hpp>

namespace core {

bool stringApi < bool >::parse ( std::istream & ) {
	throw exception::CommonException("parsing bool from string not implemented");
}

bool stringApi < bool >::first ( std::istream & ) {
	return false;
}

void stringApi < bool >::compose ( std::ostream & output, bool primitive ) {
	output << ( primitive ? "true" : "false" );
}

} /* namespace core */

namespace {

auto stringWrite = registration::StringWriterRegister < bool > ( );

auto stringWriteGroup = registration::StringWriterRegisterTypeInGroup < object::Object, bool > ( );

} /* namespace */
