/*
 * String.h
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#ifndef _STRING_STRING_H_
#define _STRING_STRING_H_

#include <core/stringApi.hpp>

#include <primitive/PrimitiveFromStringLexer.h>

namespace core {

template < >
struct stringApi < std::string > {
	static std::string parse ( std::istream & input );
	static bool first ( std::istream & input );
	static void compose ( std::ostream & output, const std::string & primitive );
};

} /* namespace core */

#endif /* _STRING_STRING_H_ */
