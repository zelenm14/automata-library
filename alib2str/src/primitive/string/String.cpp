/*
 * String.cpp
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#include "String.h"
#include <object/Object.h>

#include <registration/StringRegistration.hpp>

namespace core {

std::string stringApi < std::string >::parse ( std::istream & input ) {
	primitive::PrimitiveFromStringLexer::Token token = primitive::PrimitiveFromStringLexer::next ( input );
	if(token.type != primitive::PrimitiveFromStringLexer::TokenType::STRING)
		throw exception::CommonException("Unrecognised STRING token.");

	return token.value;
}

bool stringApi < std::string >::first ( std::istream & input ) {
	primitive::PrimitiveFromStringLexer::Token token = primitive::PrimitiveFromStringLexer::next ( input );
	bool res = token.type == primitive::PrimitiveFromStringLexer::TokenType::STRING;
	primitive::PrimitiveFromStringLexer::putback ( input, token );
	return res;
}

void stringApi < std::string >::compose ( std::ostream & output, const std::string & primitive ) {
	output << primitive;
}

} /* namespace core */

namespace {

auto stringWrite = registration::StringWriterRegister < std::string > ( );
auto stringReader = registration::StringReaderRegister < object::Object, std::string > ( );

auto stringReaderGroup = registration::StringReaderRegisterTypeInGroup < object::Object, std::string > ( );
auto stringWriteGroup = registration::StringWriterRegisterTypeInGroup < object::Object, std::string > ( );

} /* namespace */
