/*
 * UnrankedPattern.cpp
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#include "UnrankedPattern.h"
#include <tree/Tree.h>

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister < tree::UnrankedPattern < > > ( );
auto stringReader = registration::StringReaderRegister < tree::Tree, tree::UnrankedPattern < > > ( );

} /* namespace */
