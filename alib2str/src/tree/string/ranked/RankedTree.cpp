/*
 * RankedTree.cpp
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#include "RankedTree.h"
#include <tree/Tree.h>

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister < tree::RankedTree < > > ( );
auto stringReader = registration::StringReaderRegister < tree::Tree, tree::RankedTree < > > ( );

} /* namespace */
