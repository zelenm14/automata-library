/*
 * RankedPattern.cpp
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#include "RankedPattern.h"
#include <tree/Tree.h>

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister < tree::RankedPattern < > > ( );
auto stringReader = registration::StringReaderRegister < tree::Tree, tree::RankedPattern < > > ( );

} /* namespace */
