/*
 * AutomatonToStringComposerCommon.h
 *
 * Created on: Sep 26, 2017
 * Author: Jan Travnicek
 */

#ifndef AUTOMATON_TO_STRING_COMPOSER_COMMON_H_
#define AUTOMATON_TO_STRING_COMPOSER_COMMON_H_

#include <ostream>
#include <alib/vector>

#include <core/stringApi.hpp>

namespace automaton {

struct AutomatonToStringComposerCommon {
	template < class Type >
	static void composeList ( std::ostream & output, const ext::vector < Type > & list );
};

template < class Type >
void AutomatonToStringComposerCommon::composeList ( std::ostream & output, const ext::vector < Type > & list ) {
	output << '[';
	bool first = true;
	for ( const Type & value : list ) {
		if ( ! first )
			output << ", ";
		first = false;

		core::stringApi < Type >::compose ( output, value );
	}
	output << ']';
}

} /* namespace automaton */

#endif /* AUTOMATON_TO_STRING_COMPOSER_COMMON_H_ */
