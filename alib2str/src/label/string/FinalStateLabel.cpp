/*
 * FinalStateLabel.cpp
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#include "FinalStateLabel.h"
#include <label/FinalStateLabel.h>
#include <primitive/string/String.h>
#include <object/Object.h>

#include <registration/StringRegistration.hpp>

namespace core {

label::FinalStateLabel stringApi < label::FinalStateLabel >::parse ( std::istream & ) {
	throw exception::CommonException("parsing FinalStateLabel from string not implemented");
}

bool stringApi < label::FinalStateLabel >::first ( std::istream & ) {
	return false;
}

void stringApi < label::FinalStateLabel >::compose ( std::ostream & output, const label::FinalStateLabel & label ) {
	stringApi < std::string >::compose ( output, (std::string) label );
}

} /* namespace core */

namespace {

auto stringWrite = registration::StringWriterRegister < label::FinalStateLabel > ( );

auto stringWriteGroup = registration::StringWriterRegisterTypeInGroup < object::Object, label::FinalStateLabel > ( );

} /* namespace */
