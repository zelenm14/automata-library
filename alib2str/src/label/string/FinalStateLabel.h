/*
 * FinalStateLabel.h
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#ifndef _STRING_FINAL_STATE_LABEL_H_
#define _STRING_FINAL_STATE_LABEL_H_

#include <label/FinalStateLabel.h>
#include <core/stringApi.hpp>

namespace core {

template < >
struct stringApi < label::FinalStateLabel > {
	static label::FinalStateLabel parse ( std::istream & input );
	static bool first ( std::istream & input );
	static void compose ( std::ostream & output, const label::FinalStateLabel & label );
};

} /* namespace core */

#endif /* _STRING_FINAL_STATE_LABEL_H_ */
