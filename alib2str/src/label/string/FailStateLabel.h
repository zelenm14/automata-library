/*
 * FailStateLabel.h
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#ifndef _STRING_FAIL_STATE_LABEL_H_
#define _STRING_FAIL_STATE_LABEL_H_

#include <label/FailStateLabel.h>
#include <core/stringApi.hpp>

namespace core {

template < >
struct stringApi < label::FailStateLabel > {
	static label::FailStateLabel parse ( std::istream & input );
	static bool first ( std::istream & input );
	static void compose ( std::ostream & output, const label::FailStateLabel & label );
};

} /* namespace core */

#endif /* _STRING_FAIL_STATE_LABEL_H_ */
