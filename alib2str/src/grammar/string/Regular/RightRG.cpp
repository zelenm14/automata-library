/*
 * RightRG.cpp
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#include "RightRG.h"
#include <grammar/Grammar.h>

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister < grammar::RightRG < > > ( );
auto stringReader = registration::StringReaderRegister < grammar::Grammar, grammar::RightRG < > > ( );

} /* namespace */
