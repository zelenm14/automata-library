/*
 * LeftLG.cpp
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#include "LeftLG.h"
#include <grammar/Grammar.h>

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister < grammar::LeftLG < > > ( );
auto stringReader = registration::StringReaderRegister < grammar::Grammar, grammar::LeftLG < > > ( );

} /* namespace */
