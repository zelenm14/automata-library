/*
 * CFG.cpp
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#include "CFG.h"
#include <grammar/Grammar.h>

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister < grammar::CFG < > > ( );
auto stringReader = registration::StringReaderRegister < grammar::Grammar, grammar::CFG < > > ( );

} /* namespace */
