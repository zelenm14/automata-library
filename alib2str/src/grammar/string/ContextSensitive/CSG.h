/*
 * CSG.h
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#ifndef _STRING_CSG_H_
#define _STRING_CSG_H_

#include <grammar/ContextSensitive/CSG.h>
#include <core/stringApi.hpp>

#include <grammar/GrammarFromStringLexer.h>

#include <grammar/string/common/GrammarFromStringParserCommon.h>
#include <grammar/string/common/GrammarToStringComposerCommon.h>

namespace core {

template<class SymbolType >
struct stringApi < grammar::CSG < SymbolType > > {
	static grammar::CSG < SymbolType > parse ( std::istream & input );
	static bool first ( std::istream & input );
	static void compose ( std::ostream & output, const grammar::CSG < SymbolType > & grammar );
};

template<class SymbolType >
grammar::CSG < SymbolType > stringApi < grammar::CSG < SymbolType > >::parse ( std::istream & input ) {
	grammar::GrammarFromStringLexer::Token token = grammar::GrammarFromStringLexer::next(input);
	if(token.type != grammar::GrammarFromStringLexer::TokenType::CSG)
		throw exception::CommonException("Unrecognised CSG token.");

	return grammar::GrammarFromStringParserCommon::parsePreservingCSLikeGrammar < grammar::CSG < SymbolType > > ( input );
}

template<class SymbolType >
bool stringApi < grammar::CSG < SymbolType > >::first ( std::istream & input ) {
	grammar::GrammarFromStringLexer::Token token = grammar::GrammarFromStringLexer::next ( input );
	bool res = token.type == grammar::GrammarFromStringLexer::TokenType::CSG;
	grammar::GrammarFromStringLexer::putback ( input, token );
	return res;
}

template<class SymbolType >
void stringApi < grammar::CSG < SymbolType > >::compose ( std::ostream & output, const grammar::CSG < SymbolType > & grammar ) {
	output << "CSG";
	grammar::GrammarToStringComposerCommon::composePreservingCSLikeGrammar ( output, grammar );
}

} /* namespace core */

#endif /* _STRING_CSG_H_ */
