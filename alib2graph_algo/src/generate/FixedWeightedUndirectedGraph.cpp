// FixedWeightedUndirectedGraph.cpp
//
//     Created on: 07. 03. 2018
//         Author: Jan Uhlik
//    Modified by:
//
// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.
// Git repository: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library

#include "FixedWeightedUndirectedGraph.hpp"
#include <registration/AlgoRegistration.hpp>

graph::WeightedUndirectedGraph<int,
                               edge::WeightedEdge<int,
                                                  double> > graph::generate::FixedWeightedUndirectedGraph::weighted_undirected() {
  graph::WeightedUndirectedGraph<int, edge::WeightedEdge<int, double> > graph;

  graph.addNode(1);
  graph.addNode(2);
  graph.addNode(3);
  graph.addNode(4);
  graph.addNode(5);
  graph.addNode(6);

  graph.addEdge(1, 2, 1);
  graph.addEdge(2, 3, 1.5);
  graph.addEdge(3, 4, 5);
  graph.addEdge(5, 1, 6);

  return graph;
}


// ---------------------------------------------------------------------------------------------------------------------

namespace {

auto FixedWeightedUndirectedGraph =
    registration::AbstractRegister<graph::generate::FixedWeightedUndirectedGraph,
                                   graph::WeightedUndirectedGraph<int, edge::WeightedEdge<int, double> > >
        (graph::generate::FixedWeightedUndirectedGraph::weighted_undirected);
}

// ---------------------------------------------------------------------------------------------------------------------
