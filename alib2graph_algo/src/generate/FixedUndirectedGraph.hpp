// FixedUndirectedGraph.cpp
//
//     Created on: 07. 03. 2018
//         Author: Jan Uhlik
//    Modified by:
//
// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.
// Git repository: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library

#ifndef _FIXED_UNDIRECTED_GRAPH_H_
#define _FIXED_UNDIRECTED_GRAPH_H_

#include <graph/GraphClasses.hpp>

namespace graph {

namespace generate {

class FixedUndirectedGraph {
// ---------------------------------------------------------------------------------------------------------------------

 public:
  static graph::UndirectedGraph<int, ext::pair<int, int> > undirected();

// ---------------------------------------------------------------------------------------------------------------------

};

// =====================================================================================================================


// ---------------------------------------------------------------------------------------------------------------------

} // namespace generate

} // namespace graph

#endif /* _FIXED_UNDIRECTED_GRAPH_H_ */
