// SquareGridHeuristics.hpp
//
//     Created on: 15. 03. 2018
//         Author: Jan Uhlik
//    Modified by:
//
// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.
// Git repository: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library

#ifndef ALIB2_SQUAREGRIDHEURISTICS_HPP
#define ALIB2_SQUAREGRIDHEURISTICS_HPP

#include "ChebyshevDistance.hpp"
#include "DiagonalDistance.hpp"
#include "EuclideanDistance.hpp"
#include "EuclideanDistanceNoSQRT.hpp"
#include "ManhattenDistance.hpp"

#endif //ALIB2_SQUAREGRIDHEURISTICS_HPP
