// FloydWarshall.cpp
//
//     Created on: 08. 03. 2018
//         Author: Jan Uhlik
//    Modified by:
//
// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.
// Git repository: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library

#include "FloydWarshall.hpp"

#include <registration/AlgoRegistration.hpp>

namespace {

// ---------------------------------------------------------------------------------------------------------------------

auto FloydWarshall1 = registration::AbstractRegister<graph::shortest_path::FloydWarshall,
                                                     ext::map<DefaultNodeType,
                                                              ext::map<DefaultNodeType, DefaultWeightType>>,
                                                     const graph::WeightedUndirectedGraph<> &>
    (graph::shortest_path::FloydWarshall::run);

auto FloydWarshall2 = registration::AbstractRegister<graph::shortest_path::FloydWarshall,
                                                     ext::map<DefaultNodeType,
                                                              ext::map<DefaultNodeType, DefaultWeightType>>,
                                                     const graph::WeightedUndirectedMultiGraph<> &>
    (graph::shortest_path::FloydWarshall::run);

auto FloydWarshall3 = registration::AbstractRegister<graph::shortest_path::FloydWarshall,
                                                     ext::map<DefaultNodeType,
                                                              ext::map<DefaultNodeType, DefaultWeightType>>,
                                                     const graph::WeightedDirectedGraph<> &>
    (graph::shortest_path::FloydWarshall::run);

auto FloydWarshall4 = registration::AbstractRegister<graph::shortest_path::FloydWarshall,
                                                     ext::map<DefaultNodeType,
                                                              ext::map<DefaultNodeType, DefaultWeightType>>,
                                                     const graph::WeightedDirectedMultiGraph<> &>
    (graph::shortest_path::FloydWarshall::run);

auto FloydWarshall5 = registration::AbstractRegister<graph::shortest_path::FloydWarshall,
                                                     ext::map<DefaultNodeType,
                                                              ext::map<DefaultNodeType, DefaultWeightType>>,
                                                     const graph::WeightedMixedGraph<> &>
    (graph::shortest_path::FloydWarshall::run);

auto FloydWarshall6 = registration::AbstractRegister<graph::shortest_path::FloydWarshall,
                                                     ext::map<DefaultNodeType,
                                                              ext::map<DefaultNodeType, DefaultWeightType>>,
                                                     const graph::WeightedMixedMultiGraph<> &>
    (graph::shortest_path::FloydWarshall::run);

auto FloydWarshallGrid1 = registration::AbstractRegister<graph::shortest_path::FloydWarshall,
                                                         ext::map<DefaultSquareGridNodeType,
                                                                  ext::map<DefaultSquareGridNodeType,
                                                                           DefaultWeightType>>,
                                                         const grid::WeightedSquareGrid4<> &>
    (graph::shortest_path::FloydWarshall::run);

auto FloydWarshallGrid2 = registration::AbstractRegister<graph::shortest_path::FloydWarshall,
                                                         ext::map<DefaultSquareGridNodeType,
                                                                  ext::map<DefaultSquareGridNodeType,
                                                                           DefaultWeightType>>,
                                                         const grid::WeightedSquareGrid8<> &>
    (graph::shortest_path::FloydWarshall::run);
}
