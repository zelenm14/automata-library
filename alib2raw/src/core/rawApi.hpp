/*
 * RawApi.hpp
 *
 * Created on: Apr 1, 2013
 * Author: Jan Travnicek
 */

#ifndef RAW_API_HPP_
#define RAW_API_HPP_

namespace core {

template < typename T >
struct rawApi;

} /* namespace core */

#endif /* RAW_API_HPP_ */
