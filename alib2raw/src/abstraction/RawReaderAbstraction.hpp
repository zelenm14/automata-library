/*
 * RawReaderAbstraction.hpp
 *
 *  Created on: 11. 7. 2017
 *	  Author: Jan Travnicek
 */

#ifndef _RAW_READER_ABSTRACTION_HPP_
#define _RAW_READER_ABSTRACTION_HPP_

#include <abstraction/NaryOperationAbstraction.hpp>
#include <abstraction/ValueOperationAbstraction.hpp>

#include <factory/RawDataFactory.hpp>

namespace abstraction {

template < class ReturnType >
class RawReaderAbstraction : virtual public NaryOperationAbstraction < std::string && >, virtual public ValueOperationAbstraction < ReturnType > {
public:
	std::shared_ptr < abstraction::Value > run ( ) override {
		std::shared_ptr < abstraction::Value > & param = std::get < 0 > ( this->getParams ( ) );
		return std::make_shared < abstraction::ValueHolder < ReturnType > > ( factory::RawDataFactory::fromString ( abstraction::retrieveValue < std::string && > ( param ) ), true );
	}

};

} /* namespace abstraction */

#endif /* _RAW_READER_ABSTRACTION_HPP_ */
