/*
 * UnrankedTree.cpp
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#include "UnrankedTree.h"

#include <registration/RawRegistration.hpp>

namespace {

auto treeWrite = registration::RawWriterRegister < tree::UnrankedTree < > > ( );
auto treeReader = registration::RawReaderRegister < tree::UnrankedTree < > > ( );

} /* namespace */
