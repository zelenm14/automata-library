/*
 * LinearString.cpp
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#include "LinearString.h"

#include <registration/RawRegistration.hpp>

namespace {

auto stringWrite = registration::RawWriterRegister < string::LinearString < > > ( );
auto stringReader = registration::RawReaderRegister < string::LinearString < > > ( );

} /* namespace */
