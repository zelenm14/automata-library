/*
 * ReadlineLineInterface.h
 *
 *  Created on: 20. 3. 2017
 *	  Author: Jan Travnicek
 */

#ifndef _READLINE_LINE_INTERFACE_H_
#define _READLINE_LINE_INTERFACE_H_

#include <string>

#include <readline/LineInterface.h>

class ReadlineLineInterface final : public cli::LineInterface {
	bool m_allowHistory;

	bool readline ( std::string & line, bool first ) override;

	void lineCallback ( const std::string & line ) const override;

public:
	ReadlineLineInterface ( bool allowHistory ) : m_allowHistory ( allowHistory ) {
	}
};

#endif /* _READLINE_LINE_INTERFACE_H_ */
