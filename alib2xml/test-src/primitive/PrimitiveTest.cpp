#include <catch2/catch.hpp>

#include <alib/list>

#include <primitive/xml/String.h>
#include <sax/SaxParseInterface.h>
#include <sax/SaxComposeInterface.h>
#include <factory/XmlDataFactory.hpp>

TEST_CASE ( "XML Primitives Parser", "[unit][xml][primitive]" ) {
	const std::string primitive = "aaa";

	{
		ext::deque<sax::Token> tokens = factory::XmlDataFactory::toTokens(primitive);
		std::string tmp = sax::SaxComposeInterface::composeMemory ( tokens );

		ext::deque<sax::Token> tokens2 = sax::SaxParseInterface::parseMemory ( tmp );
		std::string primitive2 = factory::XmlDataFactory::fromTokens (std::move(tokens2));

		CHECK ( primitive == primitive2 );
	}
	{
		std::string tmp = factory::XmlDataFactory::toString(primitive);
		std::string primitive2 = factory::XmlDataFactory::fromString (tmp);

		CHECK ( primitive == primitive2 );
	}
}

