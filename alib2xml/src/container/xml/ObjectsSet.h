/*
 * Set.h
 *
 * Created on: Apr 1, 2013
 * Author: Jan Travnicek
 */

#ifndef _XML_OBJECTS_SET_H_
#define _XML_OBJECTS_SET_H_

#include <alib/set>
#include <core/xmlApi.hpp>

namespace core {

template < typename T >
struct xmlApi < ext::set < T > > {
	static ext::set < T > parse ( ext::deque < sax::Token >::iterator & input );
	static bool first ( const ext::deque < sax::Token >::const_iterator & input );
	static std::string xmlTagName ( );
	static void compose ( ext::deque < sax::Token > & output, const ext::set < T > & input );
};

template < typename T >
ext::set < T > xmlApi < ext::set < T > >::parse ( ext::deque < sax::Token >::iterator & input ) {
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );

	ext::set < T > set;

	while ( sax::FromXMLParserHelper::isTokenType ( input, sax::Token::TokenType::START_ELEMENT ) )
		set.insert ( core::xmlApi < T >::parse ( input ) );

	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::END_ELEMENT, xmlTagName ( ) );
	return set;
}

template < typename T >
bool xmlApi < ext::set < T > >::first ( const ext::deque < sax::Token >::const_iterator & input ) {
	return sax::FromXMLParserHelper::isToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
}

template < typename T >
std::string xmlApi < ext::set < T > >::xmlTagName ( ) {
	return "Set";
}

template < typename T >
void xmlApi < ext::set < T > >::compose ( ext::deque < sax::Token > & output, const ext::set < T > & input ) {
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::START_ELEMENT );

	for ( const T & item : input )
		core::xmlApi < T >::compose ( output, item );

	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::END_ELEMENT );
}

} /* namespace core */

#endif /* _XML_OBJECTS_SET_H_ */
