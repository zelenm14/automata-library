/*
 * UnsignedLong.h
 *
 *  Created on: Mar 26, 2013
 *      Author: Jan Travnicek
 */

#ifndef _XML_PRIMITIVE_UNSIGNED_LONG_H_
#define _XML_PRIMITIVE_UNSIGNED_LONG_H_

#include <core/xmlApi.hpp>

namespace core {

template < >
struct xmlApi < unsigned long > {
	static unsigned long parse ( ext::deque < sax::Token >::iterator & input );
	static bool first ( const ext::deque < sax::Token >::const_iterator & input );
	static std::string xmlTagName ( );
	static void compose ( ext::deque < sax::Token > & output, unsigned long data );
};

} /* namespace core */

#endif /* _XML_PRIMITIVE_UNSIGNED_LONG_H_ */
