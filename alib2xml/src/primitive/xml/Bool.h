/*
 * Bool.h
 *
 *  Created on: Mar 26, 2013
 *      Author: Jan Travnicek
 */

#ifndef _XML_PRIMITIVE_BOOL_H_
#define _XML_PRIMITIVE_BOOL_H_

#include <core/xmlApi.hpp>

namespace core {

template < >
struct xmlApi < bool > {
	static bool parse ( ext::deque < sax::Token >::iterator & input );
	static bool first ( const ext::deque < sax::Token >::const_iterator & input );
	static std::string xmlTagName ( );
	static void compose ( ext::deque < sax::Token > & output, bool data );
};

} /* namespace core */

#endif /* _XML_PRIMITIVE_BOOL_H_ */
