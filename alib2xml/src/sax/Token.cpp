/*
 * ParseToken.cpp
 *
 *  Created on: 9.8.2012
 *      Author: Martin Zak
 */

#include "Token.h"
#include <sstream>
#include <alib/iostream>

namespace sax {

Token::Token ( std::string tokenData, Token::TokenType tokenType ) : data ( std::move ( tokenData ) ), type ( tokenType ) {
}

const std::string & Token::getData ( ) const & {
	return data;
}

std::string && Token::getData ( ) && {
	return std::move(data);
}

Token::TokenType Token::getType() const {
	return type;
}

bool Token::operator==(const Token& other) const {
	return data == other.data && type == other.type;
}

std::ostream& operator<<(std::ostream& os, const Token& token) {
	os << "(Token ";
	switch(token.type) {
		case Token::TokenType::START_ELEMENT:
			os << "START_ELEMENT ";
			break;
		case Token::TokenType::END_ELEMENT:
			os << "END_ELEMENT ";
			break;
		case Token::TokenType::START_ATTRIBUTE:
			os << "START_ATTRIBUTE ";
			break;
		case Token::TokenType::END_ATTRIBUTE:
			os << "END_ATTRIBUTE ";
			break;
		case Token::TokenType::CHARACTER:
			os << "CHARACTER ";
			break;
	}
	os << token.data;
	os << ")";
	return os;
}

Token::operator std::string() const {
	std::stringstream ss;
	ss << *this;
	return ss.str();
}

} /* namespace sax */
