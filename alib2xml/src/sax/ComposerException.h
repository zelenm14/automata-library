/*
 * ComposerException.h
 *
 *  Created on: Apr 16, 2013
 *      Author: Jan Travnicek
 */

#ifndef COMPOSER_EXCEPTION_H_
#define COMPOSER_EXCEPTION_H_

#include <exception/CommonException.h>
#include "Token.h"

namespace sax {

/**
 * Exception thrown by XML parser when is expected different tag than the one which is read.
 */
class ComposerException: public exception::CommonException {
	Token m_expected;
	Token m_read;
public:
	ComposerException(const Token& expected, const Token& read);
};

} /* namespace sax */

#endif /* COMPOSER_EXCEPTION_H_ */

